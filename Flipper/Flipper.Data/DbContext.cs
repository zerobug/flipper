﻿using Flipper.Model.Integracion.Merino;
using Repository.Pattern.Ef6;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data
{
    public class DbContext : DataContext
    {
        public DbContext() : base("db")
        {

        }
        static DbContext()
        {
            //Database.SetInitializer<DbContext>(new CreateDatabaseIfNotExists<DbContext>());
            Database.SetInitializer<DbContext>(null);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !String.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }

            modelBuilder.Entity<PedidoEvento>()
                .HasMany(e => e.Pedidos)
                .WithOptional(e => e.Estado)
                .HasForeignKey(e => e.EstadoId);

            modelBuilder.Entity<Pedido>()
                .HasMany(e => e.Eventos)
                .WithRequired(e => e.Pedido)
                .HasForeignKey(e => e.PedidoId)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
