﻿using Flipper.Model.Objetivos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping.Objetivos
{
    public class ObjetivoVentaMap : EntityTypeConfiguration<ObjetivoVenta>
    {
        public ObjetivoVentaMap()
        {
            ToTable("ObjetivoVenta");
        }
    }
}
