﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping
{
    public class ProductoGrupoMap : EntityTypeConfiguration<ProductoGrupo>
    {
        public ProductoGrupoMap()
        {
            ToTable("ProductoGrupo");
        }
    }
}
