﻿using Flipper.Model.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping.Common
{
    public class UsuarioPermisoMap : EntityTypeConfiguration<UsuarioPermiso>
    {
        public UsuarioPermisoMap()
        {
            ToTable("UsuarioPermiso");
        }
    }
}
