﻿using Flipper.Model.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping.Common
{
    public class ModeloMap : EntityTypeConfiguration<Modelo>
    {
        public ModeloMap()
        {
            ToTable("Modelo");
            HasKey(i => i.Id);
            Property(i => i.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
        }
    }
}
