﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping
{
    public class PedidoEventoTipoMap : EntityTypeConfiguration<PedidoEventoTipo>
    {
        public PedidoEventoTipoMap()
        {
            ToTable("PedidoEventoTipo");
            Property(i => i.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);

        }
    }
}
