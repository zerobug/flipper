﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping
{
    public class RemitoConFacturaMap : EntityTypeConfiguration<RemitoConFactura>
    {
        public RemitoConFacturaMap()
        {
            ToTable("RemitoConFactura");
        }
    }
}
