﻿using Flipper.Model.RRHH.Comision;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping.RRHH.Comision
{
    public class LiquidacionMap : EntityTypeConfiguration<Liquidacion>
    {
        public LiquidacionMap()
        {
            ToTable("Liquidacion");
        }
    }
}
