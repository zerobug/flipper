﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data.Mapping
{
    public class PedidoDetalleMap: EntityTypeConfiguration<PedidoDetalle>
    {
        public PedidoDetalleMap()
        {
            ToTable("PedidoDetalle");
            Property(i => i.Precio).HasPrecision(18, 4);
            Property(i => i.PrecioBruto).HasPrecision(18, 4);
        }
    }
}
