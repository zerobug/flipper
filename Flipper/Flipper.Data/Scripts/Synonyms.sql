﻿DROP SYNONYM CLIVA;
DROP SYNONYM LCLIDIST;
DROP SYNONYM maevta;
DROP SYNONYM dempaque;
DROP SYNONYM dflete;
DROP SYNONYM lispre;
DROP SYNONYM maeven;
DROP SYNONYM LisDto;
DROP SYNONYM lgruart;
DROP SYNONYM ldtocant;
DROP SYNONYM maesto;
DROP SYNONYM maecli;
DROP SYNONYM lgrucli;
DROP SYNONYM HPEDREV;
DROP SYNONYM IPEDREV;

DECLARE @db varchar(50)
--SET @db='ib_binka'
SET @db='ib_TesBinka'
--SET @db='ib_konig_ok'
--SET @db='ib_binka'

-- Condicion de iva
EXEC ('CREATE SYNONYM CLIVA FOR '+@db+'.dbo.CLIVA')
-- Sucursales de clientes
EXEC ('CREATE SYNONYM LCLIDIST FOR '+@db+'.dbo.LCLIDIST')
-- Condicion de venta
EXEC ('CREATE SYNONYM maevta FOR '+@db+'.dbo.maevta')
-- Empaques
EXEC ('CREATE SYNONYM dempaque FOR '+@db+'.dbo.dempaque')
-- Fletes
EXEC ('CREATE SYNONYM dflete FOR '+@db+'.dbo.dflete')
-- Lista de precios
EXEC ('CREATE SYNONYM lispre FOR '+@db+'.dbo.lispre')
-- Vendedores
EXEC ('CREATE SYNONYM maeven FOR '+@db+'.dbo.maeven')
-- Descuentos
EXEC ('CREATE SYNONYM LisDto FOR '+@db+'.dbo.LisDto')
-- Grupos de productos
EXEC ('CREATE SYNONYM lgruart FOR '+@db+'.dbo.lgruart')
-- Bonificaciones
EXEC ('CREATE SYNONYM ldtocant FOR '+@db+'.dbo.ldtocant')
-- Productos
EXEC ('CREATE SYNONYM maesto FOR '+@db+'.dbo.maesto')
-- Clientes
EXEC ('CREATE SYNONYM maecli FOR '+@db+'.dbo.maecli')
-- Grupos de clientes
EXEC ('CREATE SYNONYM lgrucli FOR '+@db+'.dbo.lgrucli')
-- Cabecera pedidos
EXEC ('CREATE SYNONYM HPEDREV FOR '+@db+'.dbo.HPEDREV')
-- Detalle pedidos
EXEC ('CREATE SYNONYM IPEDREV FOR '+@db+'.dbo.IPEDREV')

SELECT * FROM sys.synonyms