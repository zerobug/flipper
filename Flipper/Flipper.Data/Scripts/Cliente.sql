﻿/****** Object:  View [dbo].[Cliente]    Script Date: 07/06/2016 12:54:03 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Cliente] AS
SELECT 
a.id, cod_cli codigo, nombre, cuit, cod_ven vendedorCodigo, cod_lis listaCodigo, cod_zona zonaCodigo, COD_FLETE FleteCodigo, POR_DTO1+POR_DTO2+POR_DTO3+POR_DTO4+POR_DTO5 Descuento, POR_BON1+POR_BON2+POR_BON3 Bonificacion, b.ALI_IVA AlicuotaIVA, b.DISCRI DiscriminaIVA, a.COD_CLASE ClaseCodigo, b.DES_CAT CategoriaIVA
FROM ib_TestKonig.dbo.maecli a 
INNER JOIN ib_TestKonig.dbo.CLIVA b ON a.COD_IVA = b.COD_CAT
WHERE situacion='A'
--FROM ib_local.dbo.maecli
--INNER JOIN ib_local.dbo.CLIVA b ON a.COD_IVA = b.COD_CAT

GO


