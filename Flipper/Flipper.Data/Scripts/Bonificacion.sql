﻿/****** Object:  View [dbo].[Cliente]    Script Date: 07/06/2016 12:54:03 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Bonificacion] AS
SELECT 
id, cod_cli clientecodigo, COD_CLASE claseCodigo, COD_ZONA zonaCodigo, COD_ART productoCodigo, COD_RUB rubroCodigo, COD_MAR marcaCodigo, por_bonif Valor
--FROM ib_konig_ok.dbo.ldtocant
FROM ib_local.dbo.ldtocant
WHERE por_bonif>0
GO


