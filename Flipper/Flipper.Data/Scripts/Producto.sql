﻿/****** Object:  View [dbo].[Producto]    Script Date: 07/06/2016 12:54:39 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Producto] AS
SELECT 
id, cod_art codigo, descrip nombre, 0.0 ContenedorCantidad, CAST(0 as decimal) Precio, CAST(0 as decimal) Bonificacion, COD_MAR MarcaCodigo, COD_RUB RubroCodigo
--FROM ib_konig_ok.dbo.maesto
FROM ib_local.dbo.maesto
--WHERE COD_ART IN (SELECT COD_ART FROM ib_konig_ok.dbo.LGRUART WHERE COD_GRUART='COLEC ')
WHERE COD_ART IN (SELECT COD_ART FROM ib_local.dbo.LGRUART WHERE COD_GRUART='COLEC ')

GO


