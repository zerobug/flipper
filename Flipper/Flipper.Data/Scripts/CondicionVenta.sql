﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[CondicionVenta] AS
SELECT 
id, cod_vta Codigo, descrip nombre, por_dtorec descuento
FROM ib_konig_ok.dbo.maevta
GO


