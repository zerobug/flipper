﻿/****** Object:  View [dbo].[Cliente]    Script Date: 07/06/2016 12:54:03 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ClienteSucursal] AS
SELECT 
id, cod_cli clientecodigo, NOM_DIST nombre, DIREC_DIST Direccion, COD_DIST Codigo, COD_FLETE FleteCodigo
--FROM ib_konig_ok.dbo.LCLIDIST
FROM ib_local.dbo.LCLIDIST

GO


