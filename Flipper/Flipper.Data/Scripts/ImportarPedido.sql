﻿USE [FSKonigDB]
GO
/****** Object:  StoredProcedure [dbo].[ImportarPedido]    Script Date: 09/10/2016 14:15:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[ImportarPedido] @pedidoId int AS

DECLARE @deposito CHAR(6), @total decimal
SELECT @deposito = 'DA    '
BEGIN TRAN

SELECT @total = SUM(pd.precio*pd.PedidoCantidad)
FROM PedidoDetalle pd
WHERE pd.PedidoId=@pedidoid


INSERT ib_testKonig..HPEDREV ([TIP_COM],[INI_COM],[NRO_COM],[FEC_COM],[FEC_VTO],[FEC_ORIG],[FEC_PROG],[FEC_CUMP],[COD_CLI],[COD_CLASE],[COD_ZONA],[COD_IVA],[COD_VTA],[COD_VEN],[COD_LIS],[COD_MONEDA],[IMP_COTIZA],[POR_BON1],[POR_BON2],[POR_BON3],[POR_DTOREC],[IMP_BON],[DTO_REC],[IMP_DTOREC],[IMPORTE],[A_PAGAR],[IMP_PAG],[IMP_IVA],[ALI_IVA],[IMP_AJU],[AJU_ALI],[IMP_INTERN],[IMP_EXE],[DISCRI],[IMP_PERIVA],[POR_PERIVA],[TOL_MIN],[TOL_MAX],[NRO_REPART],[OCP_CLI],[ABIERTO],[PRIORIDAD],[ESTADO],[DISTRIBUC],[STK_COMPRO],[LEYENDA],[IMP_PERBRU],[POR_PERBRU],[SAL_APROB],[COD_DIST],[A_ENTREGAR],[NRO_PEDCOT],[COD_MOTREV],[OBS_REV],[FEC_REV],[KEY_USER],[NRO_REV],[COD_RECH],[TITULO],[COD_FLETE],[OBS_CRED],[DES_CREDIT],[COD_OPE],[COT_MONEDA],[IMP_LOCAL],[COD_PROBVT],[COT_REFER],[MONE_REFER],[IMP_BONLOC],[COD_EXPORT],[GEST_COB],[COD_INCOT],[COD_VIA],[PESO_NETO],[PESO_BRUTO],[VOLUMEN],[COD_MEDPAG],[ID_REV],[ESTADO_REV],[NO_COMER],[LUG_REQUER],[FEC_REQUER],[ETD],[ETA],[EN_PLANTA],[NROCOM_REF],[COD_PUEEMB],[COD_PAIS],[COD_PUEARR],[NROCONTROL],[BIEN_USO],[COD_SUC],[COD_LUGINC],[COD_CLIFAC],[COD_FLERM],[COD_PCIA],[PED_COMP],[CIRCUITO],[VTA_GRANOS],[TIPO_COMP],[CONTAC_CLI],[ACARGO_FLE],[TIP_ORIGEN],[HOJADERUTA],[FEC_FINHR],[VTA_ANTOFI],[COD_EXPRE],[COD_COND],[SUC_EXPRE]) 
SELECT ' 5',RIGHT('0000'+ CAST(Sucursal AS VARCHAR(4)),4),RIGHT('        '+ CAST(Numero AS VARCHAR(8)),8),Fecha,Fecha,'19000101','19000101',Fecha,c.codigo,'','','8','    02',v.codigo,c.listaCodigo,'     1',1,0,0,0,6.00,0,'D',0,0,@total,.00,0,21,0,0,0,0,0,0,0,0,0,'','',0,'  ','A',' ',1,'',0,0,0,ISNULL(cs.codigo,'   '),1,'','','','19000101','','','','',RIGHT('        '+ CAST(TransporteId AS VARCHAR(6)),6),'','','',1,@total,'',1,'     1',0,'',0,'','',0,0,0,'',0,'A',0,'  ','19000101','19000101','19000101','19000101','','','','','',0,'','','','',' 2',0,' ',0,' ','','',' ',0,'19000101',0,'','',''
FROM pedido p
INNER JOIN Cliente c ON p.ClienteId = c.id
INNER JOIN Vendedor v ON p.VendedorId = v.id
LEFT JOIN ClienteSucursal cs ON p.LugarEntrega = cs.id
WHERE p.id=@pedidoid

INSERT ib_TesBinka..IPEDREV 
([TIP_COM],[INI_COM],[NRO_COM],[NRO_ITEM],[COD_ART],[COD_MAR],[CAN_ART],[CAN_MIN],[CAN_MAX],[CAN_REM],[CAN_FAC],[CAN_BUL],[COD_LIS],[COD_BUL],[PRE_ART],[DTO_ART1],[DTO_ART2],[DTO_ART3],[SUB_ART],[SUB_DOLAR],[FEC_VTO],[FEC_PROG],[COD_IDIST],[COD_DEP],[ESTADO],[POR_INTERN],[IMP_INTERN],[DES_ART],[OBS_ITEM],[ITEM_PADRE],[SUB_LOCAL],[ALI_IVA],[COD_UNIDAD],[CAN_UNIVTA],[COD_UNIVTA],[COEFUNIVTA],[ID_REV],[ESTADO_REV],[CAN_USOEST],[EVENTUAL],[CAN_BONORI],[CAN_BONART],[PED_COMP],[DTO_ART4],[DTO_ART5]) 
SELECT ' 5',RIGHT('0000'+ CAST(Sucursal AS VARCHAR(4)),4),
RIGHT('        '+ CAST(Numero AS VARCHAR(4)),8),
RIGHT('   '+ CAST((row_number() over (order by pd.Id))*2-1 AS VARCHAR(3)),3),
p.codigo,MarcaCodigo,pd.PedidoCantidad,1,1,0,0,0,c.listaCodigo,'      ',pd.Precio,0,0,0,pd.Precio*pd.PedidoCantidad,pd.Precio*pd.PedidoCantidad,Fecha,Fecha,'   ',@deposito,' ',0,0,p.nombre,'','   ',pd.Precio*pd.PedidoCantidad,21,'UN    ',pd.PedidoCantidad,'UN    ',1,0,'A',0,0,0,0,0,0,0
FROM PedidoDetalle pd
INNER JOIN Pedido pe ON pd.PedidoId=pe.Id
INNER JOIN Cliente c ON pe.ClienteId = c.id
INNER JOIN Producto p ON pd.ProductoId = p.id
WHERE pd.PedidoId=@pedidoid

INSERT ib_TesBinka..IPEDREV 
([TIP_COM],[INI_COM],[NRO_COM],[NRO_ITEM],[COD_ART],[COD_MAR],[CAN_ART],[CAN_MIN],[CAN_MAX],[CAN_REM],[CAN_FAC],[CAN_BUL],[COD_LIS],[COD_BUL],[PRE_ART],[DTO_ART1],[DTO_ART2],[DTO_ART3],[SUB_ART],[SUB_DOLAR],[FEC_VTO],[FEC_PROG],[COD_IDIST],[COD_DEP],[ESTADO],[POR_INTERN],[IMP_INTERN],[DES_ART],[OBS_ITEM],[ITEM_PADRE],[SUB_LOCAL],[ALI_IVA],[COD_UNIDAD],[CAN_UNIVTA],[COD_UNIVTA],[COEFUNIVTA],[ID_REV],[ESTADO_REV],[CAN_USOEST],[EVENTUAL],[CAN_BONORI],[CAN_BONART],[PED_COMP],[DTO_ART4],[DTO_ART5]) 
SELECT ' 5',RIGHT('0000'+ CAST(Sucursal AS VARCHAR(4)),4),
RIGHT('        '+ CAST(Numero AS VARCHAR(4)),8),
RIGHT('   '+ CAST((row_number() over (order by pd.Id))*2 AS VARCHAR(3)),3),
p.codigo,MarcaCodigo,pd.BonificacionCantidad,1,1,0,0,0,c.listaCodigo,'',0,0,0,0,0,0,Fecha,Fecha,'   ',@deposito,' ',0,0,p.nombre,'','   ',0,21,'UN    ',pd.Cantidad,'UN    ',1,0,'A',0,0,0,0,0,0,0
FROM PedidoDetalle pd
INNER JOIN Pedido pe ON pd.PedidoId=pe.Id
INNER JOIN Cliente c ON pe.ClienteId = c.id
INNER JOIN Producto p ON pd.ProductoId = p.id
WHERE pd.PedidoId=@pedidoid



COMMIT