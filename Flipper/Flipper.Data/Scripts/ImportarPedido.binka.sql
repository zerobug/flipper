﻿USE [FSKonigDB]
GO
/****** Object:  StoredProcedure [dbo].[ImportarPedido]    Script Date: 01/14/2022 13:35:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[ImportarPedido] @pedidoId int AS

DECLARE @tipcom CHAR(2),
@inicom CHAR(6), 
@nrocom CHAR(8), 
@deposito CHAR(6), @total decimal(10,2),@codvta CHAR(6), @codiva CHAR(1), @codpcia CHAR(2), @descuento decimal(10,2), @descuentoImporte decimal(10,2), @subtotal decimal(10,2)
BEGIN TRAN

SELECT @deposito = LEFT(b.Deposito + '      ',6)
FROM Pedido a 
INNER JOIN Usuario b ON a.Sucursal = b.PuntoVenta
WHERE a.ID=@pedidoid

SELECT @total = SUM(pd.precio*pd.PedidoCantidad)
FROM PedidoDetalle pd
WHERE pd.PedidoId=@pedidoid

SELECT @tipcom=' 5', @inicom=RIGHT('0000'+ CAST(Sucursal AS VARCHAR(4)),4), @nrocom=RIGHT('        '+ CAST(Numero AS VARCHAR(8)),8) FROM Pedido WHERE ID=@pedidoid


SELECT @codvta= COD_VTA, @codiva=COD_IVA, @codpcia=COD_PCIA FROM Ib_Binka..MAECLI WHERE ID = (SELECT ClienteId FROM Pedido WHERE ID=@pedidoid)
SELECT @codpcia=ISNULL(COD_PROV,@codpcia) FROM Pedido p LEFT JOIN Ib_Binka.dbo.LCLIDIST cs ON p.LugarEntrega = cs.id WHERE p.Id=@pedidoid
SELECT @descuento= POR_DTOREC FROM Ib_Binka..MAEVTA WHERE COD_VTA = @codvta
SELECT @descuentoImporte=@total*@descuento/100
SELECT @total = @total-@descuentoImporte
SELECT @subtotal = @total
IF @codiva='1'
	SELECT @total = @subtotal*1.21


-- Cabecera del pedido
INSERT Ib_Binka..HPEDREV ([TIP_COM],[INI_COM],[NRO_COM],[FEC_COM],[FEC_VTO],[FEC_ORIG],[FEC_PROG],[FEC_CUMP],[COD_CLI],[COD_CLASE],[COD_ZONA],[COD_IVA],[COD_VTA],[COD_VEN],[COD_LIS],
[COD_MONEDA],[IMP_COTIZA],[POR_BON1],[POR_BON2],[POR_BON3],[POR_DTOREC],[IMP_BON],[DTO_REC],[IMP_DTOREC],[IMPORTE],[A_PAGAR],[IMP_PAG],[IMP_IVA],[ALI_IVA],[IMP_AJU],[AJU_ALI],[IMP_INTERN],
[IMP_EXE],[DISCRI],[IMP_PERIVA],[POR_PERIVA],[TOL_MIN],[TOL_MAX],[NRO_REPART],[OCP_CLI],[ABIERTO],[PRIORIDAD],[ESTADO],[DISTRIBUC],[STK_COMPRO],[LEYENDA],[IMP_PERBRU],[POR_PERBRU],[SAL_APROB],
[COD_DIST],[A_ENTREGAR],[NRO_PEDCOT],[COD_MOTREV],[OBS_REV],[FEC_REV],[KEY_USER],[NRO_REV],[COD_RECH],[TITULO],[COD_FLETE],[OBS_CRED],[DES_CREDIT],[COD_OPE],[COT_MONEDA],[IMP_LOCAL],[COD_PROBVT],
[COT_REFER],[MONE_REFER],[IMP_BONLOC],[COD_EXPORT],[GEST_COB],[COD_INCOT],[COD_VIA],[PESO_NETO],[PESO_BRUTO],[VOLUMEN],[COD_MEDPAG],[ID_REV],[ESTADO_REV],[NO_COMER],[LUG_REQUER],[FEC_REQUER],[ETD],
[ETA],[EN_PLANTA],[NROCOM_REF],[COD_PUEEMB],[COD_PAIS],[COD_PUEARR],[NROCONTROL],[BIEN_USO],[COD_SUC],[COD_LUGINC],[COD_CLIFAC],[COD_FLERM],[COD_PCIA],[PED_COMP],[CIRCUITO],[VTA_GRANOS],[TIPO_COMP],
[CONTAC_CLI],[ACARGO_FLE],[TIP_ORIGEN],[HOJADERUTA],[FEC_FINHR],[VTA_ANTOFI],[COD_EXPRE],[COD_COND],[SUC_EXPRE]) 
SELECT @tipcom,@inicom,@nrocom,
Fecha [FEC_COM],
Fecha [FEC_VTO],
'19000101' [FEC_ORIG],
'19000101' [FEC_PROG],
Fecha [FEC_CUMP],
c.codigo [COD_CLI],
'' [COD_CLASE],
'' [COD_ZONA],
@codiva, 
@codvta,
v.codigo,
c.listaCodigo,
'     1' [COD_MONEDA],
1 [IMP_COTIZA],
0 [POR_BON1],
0 [POR_BON2],
0 [POR_BON3],
@descuento,
0 [IMP_BON],
'D' [DTO_REC],
@descuentoImporte [IMP_DTOREC],
@subtotal [IMPORTE],
@total [A_PAGAR],
0 [IMP_PAG],
@total -@subtotal [IMP_IVA],
21 [ALI_IVA],
0 [IMP_AJU],0 [AJU_ALI],0 [IMP_INTERN],0 [IMP_EXE], 
CASE WHEN @codiva=1 THEN 1 ELSE 0 END [DISCRI],0,0,0,0,'','',0,'  ','A',' ',1,'',0,0,0
,ISNULL(cs.codigo,'   '),1,'','','','19000101','','','','',f.codigo,'','','',1,@total,'',1,'     1',0,'',0,'','',0,0,0,'',0,'A',0,'  ',
'19000101','19000101','19000101','19000101','','','','','',0,'','','','',@codpcia,0,' ',0,' ','','',' ',0,'19000101',0,'','',''
FROM pedido p
INNER JOIN Cliente c ON p.ClienteId = c.id
INNER JOIN Vendedor v ON p.VendedorId = v.id
LEFT JOIN ClienteSucursal cs ON p.LugarEntrega = cs.id
LEFT JOIN Flete f ON p.TransporteId = f.id
WHERE p.id=@pedidoid

UPDATE Ib_Binka..HPEDREV 
SET Leyenda = (SELECT ISNULL(Leyenda,'') FROM Pedido WHERE ID=@pedidoid)
WHERE [TIP_COM]=@tipcom
AND [INI_COM]=@inicom
AND [NRO_COM]=@nrocom

-- Productos con valor
INSERT Ib_Binka..IPEDREV 
([TIP_COM],[INI_COM],[NRO_COM],[NRO_ITEM],
[COD_ART],[COD_MAR],[CAN_ART],[CAN_MIN],[CAN_MAX],[CAN_REM],[CAN_FAC],[CAN_BUL],[COD_LIS],[COD_BUL],[PRE_ART],[DTO_ART1],
[DTO_ART2],[DTO_ART3],[SUB_ART],[SUB_DOLAR],[FEC_VTO],[FEC_PROG],[COD_IDIST],[COD_DEP],[ESTADO],[POR_INTERN],[IMP_INTERN],[DES_ART],[OBS_ITEM],
[ITEM_PADRE],[SUB_LOCAL],[ALI_IVA],[COD_UNIDAD],[CAN_UNIVTA],[COD_UNIVTA],[COEFUNIVTA],[ID_REV],[ESTADO_REV],[CAN_USOEST],[EVENTUAL],[CAN_BONORI],[CAN_BONART],
[PED_COMP],[DTO_ART4],[DTO_ART5]) 
SELECT [TIP_COM],[INI_COM],[NRO_COM], RIGHT('   '+ CAST((row_number() over (order by [COD_ART], [PRE_ART] desc)) AS VARCHAR(3)),3) [NRO_ITEM],
[COD_ART],[COD_MAR],[CAN_ART],[CAN_MIN],[CAN_MAX],[CAN_REM],[CAN_FAC],[CAN_BUL],[COD_LIS],[COD_BUL],[PRE_ART],[DTO_ART1],
[DTO_ART2],[DTO_ART3],[SUB_ART],[SUB_DOLAR],[FEC_VTO],[FEC_PROG],[COD_IDIST],[COD_DEP],[ESTADO],[POR_INTERN],[IMP_INTERN],[DES_ART],[OBS_ITEM],
[ITEM_PADRE],[SUB_LOCAL],[ALI_IVA],[COD_UNIDAD],[CAN_UNIVTA],[COD_UNIVTA],[COEFUNIVTA],[ID_REV],[ESTADO_REV],[CAN_USOEST],[EVENTUAL],[CAN_BONORI],[CAN_BONART],
[PED_COMP],[DTO_ART4],[DTO_ART5] FROM (
SELECT @tipcom [TIP_COM],@inicom [INI_COM],@nrocom [NRO_COM],
p.codigo [COD_ART],MarcaCodigo [COD_MAR],pd.PedidoCantidad [CAN_ART],pd.PedidoCantidad [CAN_MIN],pd.PedidoCantidad [CAN_MAX],0 [CAN_REM],0 [CAN_FAC],0 [CAN_BUL],c.listaCodigo [COD_LIS],'      ' [COD_BUL],pd.PrecioBruto [PRE_ART],pd.Descuento [DTO_ART1],
0 [DTO_ART2],0 [DTO_ART3],pd.Precio*pd.PedidoCantidad [SUB_ART],pd.Precio*pd.PedidoCantidad [SUB_DOLAR],Fecha [FEC_VTO],Fecha [FEC_PROG],'   ' [COD_IDIST],@deposito [COD_DEP],' ' [ESTADO],0 [POR_INTERN],0 [IMP_INTERN],p.nombre [DES_ART],'' [OBS_ITEM],
'   ' [ITEM_PADRE],pd.Precio*pd.PedidoCantidad [SUB_LOCAL],21 [ALI_IVA],'UN    ' [COD_UNIDAD],pd.PedidoCantidad [CAN_UNIVTA],'UN    ' [COD_UNIVTA],1 [COEFUNIVTA],0 [ID_REV],'A' [ESTADO_REV],0 [CAN_USOEST],0 [EVENTUAL],0 [CAN_BONORI],0 [CAN_BONART],
0 [PED_COMP],0 [DTO_ART4],0 [DTO_ART5]
FROM PedidoDetalle pd
INNER JOIN Pedido pe ON pd.PedidoId=pe.Id
INNER JOIN Cliente c ON pe.ClienteId = c.id
INNER JOIN Producto p ON pd.ProductoId = p.id
WHERE pd.PedidoId=@pedidoid AND p.Kit=0
UNION ALL
-- Productos bonificacios
SELECT @tipcom,@inicom,@nrocom,p.codigo,MarcaCodigo,pd.BonificacionCantidad,pd.BonificacionCantidad,pd.BonificacionCantidad,0,0,0,c.listaCodigo,'',0,0,0,0,0,0,Fecha,Fecha,'   ',@deposito,' ',0,0,p.nombre,'','   ',0,21,'UN    ',pd.BonificacionCantidad,'UN    ',1,0,'A',0,0,0,0,0,0,0
FROM PedidoDetalle pd
INNER JOIN Pedido pe ON pd.PedidoId=pe.Id
INNER JOIN Cliente c ON pe.ClienteId = c.id
INNER JOIN Producto p ON pd.ProductoId = p.id
WHERE pd.PedidoId=@pedidoid  AND p.Kit=0 AND pd.BonificacionCantidad>0
) as lineas
ORDER BY [NRO_ITEM]


-- Productos en  kit
/*
DECLARE @rowcount int
SELECT @rowcount = COUNT(*)
FROM ib_Konig_Ok..IPEDREV 
WHERE [TIP_COM]=@tipcom
AND [INI_COM]=@inicom
AND [NRO_COM]=@nrocom

INSERT ib_Konig_Ok..IPEDREV 
([TIP_COM],[INI_COM],[NRO_COM],[NRO_ITEM],[COD_ART],[COD_MAR],[CAN_ART],[CAN_MIN],[CAN_MAX],[CAN_REM],[CAN_FAC],[CAN_BUL],[COD_LIS],[COD_BUL],[PRE_ART],[DTO_ART1],[DTO_ART2],[DTO_ART3],[SUB_ART],[SUB_DOLAR],[FEC_VTO],[FEC_PROG],[COD_IDIST],[COD_DEP],[ESTADO],[POR_INTERN],[IMP_INTERN],[DES_ART],[OBS_ITEM],[ITEM_PADRE],[SUB_LOCAL],[ALI_IVA],[COD_UNIDAD],[CAN_UNIVTA],[COD_UNIVTA],[COEFUNIVTA],[ID_REV],[ESTADO_REV],[CAN_USOEST],[EVENTUAL],[CAN_BONORI],[CAN_BONART],[PED_COMP],[DTO_ART4],[DTO_ART5]) 
SELECT @tipcom,@inicom,@nrocom,
RIGHT('   '+ CAST((row_number() over (order by pd.Id))+@rowcount AS VARCHAR(3)),3),
p.codigo,p.MarcaCodigo,pd.PedidoCantidad,pd.PedidoCantidad,pd.PedidoCantidad,0,0,0,c.listaCodigo,'      ',pd.PrecioBruto,pd.Descuento,0,0,
(pd.Precio-(pd.Precio*pv.Descuento/100))*pd.PedidoCantidad,
(pd.Precio-(pd.Precio*pv.Descuento/100))*pd.PedidoCantidad,
Fecha,Fecha,'   ',@deposito,' ',0,0,p.nombre,'','   ',
(pd.Precio-(pd.Precio*pv.Descuento/100))*pd.PedidoCantidad,
21,'UN    ',pd.PedidoCantidad,'UN    ',1,0,'A',0,0,0,0,0,0,0
FROM PedidoDetalle pd
INNER JOIN Pedido pe ON pd.PedidoId=pe.Id
INNER JOIN Cliente c ON pe.ClienteId = c.id
INNER JOIN Producto pr ON pd.ProductoId = pr.id
INNER JOIN productovirtualdetalle pv ON pd.ProductoId = pv.ProductoVirtualId
INNER JOIN Producto p ON pv.ProductoId = p.id
WHERE pd.PedidoId=@pedidoid AND pr.Kit=1
*/

DELETE FROM Ib_Binka..WEB_HPEDREV
WHERE [TIP_COM]=@tipcom
AND [INI_COM]=@inicom
AND [NRO_COM]=@nrocom
DELETE FROM Ib_Binka..WEB_IPEDREV
WHERE [TIP_COM]=@tipcom
AND [INI_COM]=@inicom
AND [NRO_COM]=@nrocom

INSERT Ib_Binka..WEB_IPEDREV 
SELECT [TIP_COM],[INI_COM],[NRO_COM],[NRO_ITEM],[COD_CLI],[COD_ART],[COD_RUB],[COD_MAR],[COD_CLASE],[COD_ZONA],[CAN_ART]
      ,[CAN_MIN],[CAN_MAX],[CAN_REM],[CAN_FAC],[CAN_BUL],[COD_LIS],[COD_BUL],[PRE_ART],[DTO_ART1],[DTO_ART2],[DTO_ART3],[SUB_ART]
      ,[SUB_DOLAR],[FEC_ORIG],[FEC_VTO],[FEC_PROG],[FEC_CUMP],[A_ENTREGAR],[COD_IDIST],[COD_DEP],[PRIORIDAD],[ESTADO],[POR_INTERN]
      ,[IMP_INTERN],[DES_ART],[OBS_ITEM],[NRO_REV],[RESERVADO],[NRO_ORDEN],[ITEM_PADRE],[ID],[SUB_LOCAL],[ALI_IVA],[A_EMBALAR],[EMBALADO]
      ,[COD_INCOT],[PESO_NETO],[PESO_BRUTO],[VOLUMEN],[COD_VIA],[COD_ORIGEN],[COD_UNIDAD],[DUE_DATE],[OBS_DDATE],[ETD],[ETA],[CAN_UNIVTA]
      ,[COD_UNIVTA],[COEFUNIVTA],[CAN_EXPORT],[ID_REV],[ESTADO_REV],[VOLUM_TOT],[FEC_REQUER],[LUG_REQUER],[EN_PLANTA],[ESTAD_PICK],[CAN_USOEST]
      ,[EVENTUAL],[CAN_BONORI],[CAN_BONART],[PED_COMP],[HILTON],[IMP_FOB],[DTO_ART4],[DTO_ART5],[ESTADOITEM],[COD_RECH],[CODARTSIM],[NRO_RUTA],[NRO_REVIS],[COD_TIPPRF]
FROM Ib_Binka..IPEDREV 
WHERE [TIP_COM]=@tipcom
AND [INI_COM]=@inicom
AND [NRO_COM]=@nrocom

INSERT Ib_Binka..WEB_HPEDREV
SELECT * FROM Ib_Binka..HPEDREV 
WHERE [TIP_COM]=@tipcom
AND [INI_COM]=@inicom
AND [NRO_COM]=@nrocom

COMMIT