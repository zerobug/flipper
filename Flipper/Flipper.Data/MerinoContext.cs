﻿using Flipper.Model.DTO;
using Repository.Pattern.Ef6;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Data
{
    public class MerinoContext:DataContext
    {
        public MerinoContext() : base("merino")
        {
        }
        static MerinoContext()
        {
            //Database.SetInitializer<DbContext>(new CreateDatabaseIfNotExists<DbContext>());
            Database.SetInitializer<MerinoContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReporteVentaDto>().Property(i => i.VendedorCodigo).HasColumnName("COD_VEN");
            base.OnModelCreating(modelBuilder);

        }
    }
}
