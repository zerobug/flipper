﻿using Flipper.Model.DTO.RRHH.Comision;
using Flipper.Service.RRHH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    public class RRHHComisionController : BaseApiController
    {
        public IHttpActionResult Get(int id)
        {
            return Ok(new ComisionService().GetById(id));
        }

        [HttpPost]
        public IHttpActionResult Actualizar(LiquidacionDto valor)
        {
            return Ok(new ComisionService().GetDetalle(valor));
        }

        [HttpPost]
        [Route("Api/RRHHComision/Search")]
        public IHttpActionResult Search(string query)
        {
            return Ok(new ComisionService().Search(query));
        }

        [HttpPost]
        public IHttpActionResult Grabar(LiquidacionDto value)
        {
            try
            {
                var item = new ComisionService().Grabar(value);
                return Ok(item);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return BadRequest("No se pudo grabar la liquidacion");
            }
        }

    }
}
