﻿using Flipper.Model.DTO.Cliente;
using Flipper.Model.Integracion.Merino;
using Flipper.Service.Cliente;
using Flipper.Service.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    [CustomAuthorizationFilterAttribute]
    public class PedidoController : BaseApiController
    {
        public IEnumerable<Pedido> Get()
        {
            return MockService.Pedidos as IEnumerable<Pedido>;
        }

        public IHttpActionResult Get(int id)
        {
            var xx = new PedidoService().GetById(id);
            return Ok(xx);
        }

        public IHttpActionResult Post(PedidoDto value)
        {
            try
            {
                var pedido = new PedidoService().Grabar(value, UsuarioId);
                return Ok(pedido);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        [Route("Api/Pedido/Search")]
        public IHttpActionResult Search(PedidoSearchDto query)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                    return Unauthorized();
                string vendedorCodigo = "";
                if (UsuarioId > 0)
                    vendedorCodigo = new SeguridadService().GetById(UsuarioId).Valor.Codigo;

                return Ok(new PedidoService().Search(query, UsuarioId));
            }
            catch (Exception ex)
            {

                return BadRequest(ex.ToString());
            }
        }

        [HttpPost]
        [Route("Api/Pedido/Autorizar")]
        public IHttpActionResult Autorizar(PedidoDto value)
        {
            if (!User.Identity.IsAuthenticated)
                    return Unauthorized();
            var pedido = new PedidoService().Autorizar(value, UsuarioId);
            return Ok(pedido);
    }

        [HttpPost]
        [Route("Api/Pedido/Rechazar")]
        public IHttpActionResult Rechazar(PedidoDto value)
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized();
            var pedido = new PedidoService().Rechazar(value, UsuarioId);
            return Ok();
        }

    }
}
