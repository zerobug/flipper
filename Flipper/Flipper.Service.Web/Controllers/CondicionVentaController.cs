﻿using Flipper.Service.Cliente;
using Flipper.Service.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    [CustomAuthorizationFilterAttribute]
    public class CondicionVentaController : BaseApiController
    {

        [HttpGet]
        public IHttpActionResult Get(int id, string query = "")
        {
            List<System.Web.Mvc.SelectListItem> resultado = new List<System.Web.Mvc.SelectListItem>();
            var search = new CondicionVentaService().GetAll();
            if (search.Exito)
            {
                resultado.AddRange(search.Valor.Select(i => new System.Web.Mvc.SelectListItem() { Value = i.Id.ToString(), Text = i.Nombre.TrimEnd() }));
            }
            return Ok(resultado);
        }

    }
}
