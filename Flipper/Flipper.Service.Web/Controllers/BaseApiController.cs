﻿using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        protected int UsuarioId
        {
            get
            {
                try
                {
                    return int.Parse(User.Identity.Name);
                }
                catch (Exception)
                {
                }
                return 0;
            }
        }
        protected string Usuario { get; set; }
        protected bool GetVendedor()
        {
            if (Request.Headers.Authorization!=null)
            {
                Usuario = Request.Headers.Authorization.Parameter;
            }
            return !string.IsNullOrEmpty(Usuario);
        }
        public BaseApiController()
        {
        }
    }
}
