﻿using Flipper.Model.DTO;
using Flipper.Service.Cliente.Reporte;
using Flipper.Service.Web.Filters;
using Syncfusion.EJ.ReportViewer;
using Syncfusion.Reports.EJ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    [CustomAuthorizationFilterAttribute]
    public class ReportsController : BaseApiController, IReportController
    {
        [ActionName("GetResource")]
        [AcceptVerbs("GET")]
        public object GetResource(string key, string resourcetype, bool isPrint)
        {
            return ReportHelper.GetResource(key, resourcetype, isPrint);
        }

        public void OnInitReportOptions(ReportViewerOptions reportOption)
        {
            var reportName = reportOption.ReportModel.ReportPath;
            reportOption.ReportModel.ReportPath = HostingEnvironment.MapPath("~/Reportes/")+ reportOption.ReportModel.ReportPath;
            IEnumerable<ReporteVentaDto> data = new HashSet<ReporteVentaDto>();
            //data = new VentaService().ByVendedor("20160101", "20160101");
            reportOption.ReportModel.DataSources.Add(new ReportDataSource { Name = "DataSet1", Value = data });
        }

        public void OnReportLoaded(ReportViewerOptions reportOption)
        {
            if (System.Web.HttpContext.Current.Items.Contains("parakey"))
            {
                reportOption.ReportModel.Parameters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Syncfusion.Reports.EJ.ReportParameter>>(System.Web.HttpContext.Current.Items["parakey"].ToString());
                System.Web.HttpContext.Current.Items.Remove("parakey");
                reportOption.ReportModel.DataSources.Clear();
                var parameters = reportOption.ReportModel.Parameters.ToDictionary(i => i.Name);
                IEnumerable<ReporteVentaDto> data = new HashSet<ReporteVentaDto>();
                var desde = parameters["desde"].Values[0];
                var hasta = parameters["hasta"].Values[0];
                var reporte = parameters["tipo"].Values[0];
                switch (reporte)
                {
                    case "porVendedor":
                        data = new VentaService().ByVendedor(desde,hasta);
                        break;
                    case "porProducto":
                        data = new VentaService().ByProducto(desde, hasta);
                        break;
                    case "porVendedorCliente":
                        data = new VentaService().ByClienteVendedor(desde, hasta);
                        break;
                    case "porVendedorClienteProducto":
                        data = new VentaService().ByProductoClienteVendedor(desde, hasta);
                        break;
                    default:
                        break;
                }
                string vendedorCodigo = "";
                //GetVendedor();
                if (UsuarioId > 0)
                    vendedorCodigo = new SeguridadService().GetById(UsuarioId).Valor.Codigo;
                if (!string.IsNullOrEmpty(vendedorCodigo))
                    data = data.Where(i => i.VendedorCodigo == vendedorCodigo);
                reportOption.ReportModel.DataSources.Add(new ReportDataSource { Name = "DataSet1", Value = data });
            }

        }

        [AcceptVerbs("POST")]
        public object PostReportAction(Dictionary<string, object> jsonResult)
        {
            if (jsonResult.ContainsValue("GetDataSourceCredential") && jsonResult.ContainsKey("params"))
            {
                System.Web.HttpContext.Current.Items.Add("parakey", jsonResult["params"]);
            }
            return ReportHelper.ProcessReport(jsonResult, this);
        }

        public IHttpActionResult GetData(string report, string desde, string hasta)
        {
            return Ok(new VentaService().ByVendedor(desde,hasta));
        }
    }
    public class Venta
    {
        public string Fecha { get; set; }
        public string Vendedor { get; set; }
        public string VendedorCodigo { get; set; }
        public string Producto { get; set; }
        public string Cliente { get; set; }
        public string Provincia { get; set; }
        public decimal Importe { get; set; }
        public decimal Cantidad { get; set; }
    }

}
