﻿using Flipper.Core.Helper;
using Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    public class ProductoController : BaseApiController
    {
        public IHttpActionResult Get(string query)
        {
            if (!GetVendedor())
                return Unauthorized();
            return Ok(new ProductoService().Search(query).Valor.Select(i => new System.Web.Mvc.SelectListItem() { Value = i.Id.ToString(), Text = string.Format("{0} ({1})", i.Nombre.TrimEnd(), i.Codigo.TrimEnd()) }));
        }

        public IHttpActionResult GetByID(string id)
        {
            int entityId;
            if (!int.TryParse(id, out entityId))
                return Ok();
            if (!GetVendedor())
                return Unauthorized();
            return Ok(new ProductoService().Get(entityId));
        }

        public IHttpActionResult GetByIDWithPrice(string id, string lista)
        {
            int entityId;
            if (!int.TryParse(id, out entityId))
                return Ok();
            if (!GetVendedor())
                return Unauthorized();
            return Ok(new ProductoService().Get(entityId, lista));
        }

        public IHttpActionResult GetByIDAndClient(string id, string cliente)
        {
            int entityId;
            int clienteId;
            if (!int.TryParse(id, out entityId)|| !int.TryParse(cliente, out clienteId))
                return Ok(CommonService.GetError("Producto o cliente invalido"));

            return Ok(new ProductoService().Get(entityId, clienteId));
        }
        //public IEnumerable<Producto> Get()
        //{
        //    var context = new HttpContextWrapper(HttpContext.Current);
        //    var filterCollection = KendoGridFilterCollection.BuildCollection(context.Request);
        //    if (filterCollection.Filters.Count > 0)
        //    {
        //        return MockService.Productos.Where(i=> i.Nombre.ToLower().Contains(filterCollection.Filters.First().Value.ToLower())) as IEnumerable<Producto>;
        //    }
        //    return MockService.Productos as IEnumerable<Producto>;
        //}

    }
}
