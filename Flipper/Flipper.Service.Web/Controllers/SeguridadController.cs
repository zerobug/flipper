﻿using Flipper.Model.DTO.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    public class SeguridadController : BaseApiController
    {
        [HttpGet]
        public IHttpActionResult Ingresar(string usuario, string clave)
        {
            var result = new SeguridadService().Autenticar(usuario, clave);
            if (result == 0)
                return Unauthorized();

            var user = new SeguridadService().GetById(result);


            return Ok(new UsuarioPermiso() { UsuarioId = result, Autorizar = string.IsNullOrEmpty(user.Valor.Codigo) });

        }

        public IHttpActionResult GetByID(string id)
        {
            int usuarioId;
            if (!int.TryParse(id, out usuarioId))
                return Ok();
            if (!GetVendedor())
                return Unauthorized();
            return Ok(new SeguridadService().GetById(usuarioId));
        }
    }
}
