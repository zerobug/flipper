﻿using Flipper.Service.Cliente.Reporte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    public class ReporteVentaDiariaController : BaseApiController
    {

        [HttpGet]
        public IHttpActionResult Get(string desde, string hasta, string tipo)
        {
            return Ok(new VentaService().Diaria(desde, hasta, tipo));
        }


    }
}
