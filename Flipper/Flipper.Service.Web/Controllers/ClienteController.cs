﻿using Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using Flipper.Service.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    [CustomAuthorizationFilterAttribute]
    public class ClienteController : BaseApiController
    {
        [HttpGet]
        public IHttpActionResult ComboSource(string query, string id="")
        {
            List<System.Web.Mvc.SelectListItem> resultado = new List<System.Web.Mvc.SelectListItem>();

            if (!GetVendedor())
                return Unauthorized();
            string vendedorCodigo = "";
            int clienteId;
            if(UsuarioId>0)
                vendedorCodigo = new SeguridadService().GetById(UsuarioId).Valor.Codigo;


            resultado.AddRange(new ClienteService()
                .Search(query, vendedorCodigo)
                .Valor
                .Select(i => 
                new System.Web.Mvc.SelectListItem() {
                    Value = i.Id.ToString(),
                    Text = string.Format("{1} - {0}", i.Nombre.TrimEnd(), i.Codigo.TrimEnd()) }));
            if (int.TryParse(id, out clienteId)&&clienteId>0)
            {
                var cliente = new ClienteService().Get(clienteId);
                if (cliente.Exito)
                    resultado.Add(new System.Web.Mvc.SelectListItem() { Value = cliente.Valor.Id.ToString(), Text = cliente.Valor.Nombre.TrimEnd() });
            }

            return Ok(resultado);
        }

        public IHttpActionResult GetByID(string id)
        {
            try
            {
                int clienteId;
                if (!int.TryParse(id, out clienteId))
                    return Ok();
                if (!GetVendedor())
                    return Unauthorized();
                return Ok(new ClienteService().Get(clienteId));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return BadRequest();
            }
        }

        public IHttpActionResult GetSucursales(string id)
        {
            int clienteId;
            if (!int.TryParse(id, out clienteId))
                return Ok(new ErrorResult("Cliente invalido"));
            return Ok(new ClienteService().GetSucursalList(clienteId));
        }
    }
}
