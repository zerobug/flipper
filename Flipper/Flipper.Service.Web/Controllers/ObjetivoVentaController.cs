﻿using Flipper.Model.DTO.Objetivos;
using Flipper.Model.Objetivos;
using Flipper.Service.Objetivos;
using Flipper.Service.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    [CustomAuthorizationFilterAttribute]
    public class ObjetivoVentaController : BaseApiController
    {
        [HttpPost]
        [Route("Api/ObjetivoVenta/Search")]
        public IHttpActionResult Search(ObjetivoVentaSearchDto query)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                    return Unauthorized();
                string vendedorCodigo = "";
                if (UsuarioId > 0)
                    vendedorCodigo = new SeguridadService().GetById(UsuarioId).Valor.Codigo;

                return Ok(new ObjetivoVentaService().Search(query, UsuarioId));
            }
            catch (Exception ex)
            {

                return BadRequest(ex.ToString());
            }
        }

        public IHttpActionResult Get(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized();
            var model = new ObjetivoVentaService().GetById(id);
            return Ok(model);
        }

        public IHttpActionResult Post(ObjetivoVenta value)
        {
            try
            {
                var pedido = new ObjetivoVentaService().Grabar(value, UsuarioId);
                return Ok(pedido);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        [Route("Api/ObjetivoVenta/Reporte")]
        public IHttpActionResult Reporte(ObjetivoVentaSearchDto query)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                    return Unauthorized();
                string vendedorCodigo = "";
                if (UsuarioId > 0)
                    vendedorCodigo = new SeguridadService().GetById(UsuarioId).Valor.Codigo;

                return Ok(new ObjetivoVentaService().Reporte(query, UsuarioId));
            }
            catch (Exception ex)
            {

                return BadRequest(ex.ToString());
            }
        }
    }
}
