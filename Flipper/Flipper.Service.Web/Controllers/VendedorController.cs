﻿using Flipper.Service;
using Flipper.Service.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    [CustomAuthorizationFilterAttribute]
    public class VendedorController : BaseApiController
    {
        [HttpGet]
        public IHttpActionResult ComboSource(string query, string id = "")
        {
            List<System.Web.Mvc.SelectListItem> resultado = new List<System.Web.Mvc.SelectListItem>();

            if (!GetVendedor())
                return Unauthorized();

            resultado.AddRange(new VendedorService()
                .Search(query??"", id)
                .Valor
                .Select(i =>
                new System.Web.Mvc.SelectListItem()
                {
                    Value = i.Id.ToString(),
                    Text = string.Format("{1} - {0}", i.Nombre.TrimEnd(), i.Codigo.TrimEnd())
                }));

            return Ok(resultado);
        }
    }
}
