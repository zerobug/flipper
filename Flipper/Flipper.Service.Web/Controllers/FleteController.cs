﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Flipper.Service.Web.Controllers
{
    public class FleteController : BaseApiController
    {
        public IHttpActionResult Get(int id, string query="")
        {
            List<System.Web.Mvc.SelectListItem> resultado = new List<System.Web.Mvc.SelectListItem>();
            var search = new FleteService().Search(query);
            if (search.Exito)
            {
                resultado.AddRange(search.Valor.Select(i => new System.Web.Mvc.SelectListItem() { Value = i.Id.ToString(), Text = i.Nombre.TrimEnd() }));
            }
            if (id > 0)
            {
                var flete = new FleteService().GetById(id);
                if (flete.Exito)
                    resultado.Add(new System.Web.Mvc.SelectListItem() { Value = flete.Valor.Id.ToString(), Text = flete.Valor.Nombre.TrimEnd() });
            }
            return Ok(resultado);
        }

    }
}
