﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Flipper.Core.Helper;

[assembly: OwinStartup(typeof(Flipper.Service.Web.Startup))]

namespace Flipper.Service.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            SettingHelper.Empresa = (int)SettingHelper.Empresas.Konig;
        }
    }
}
