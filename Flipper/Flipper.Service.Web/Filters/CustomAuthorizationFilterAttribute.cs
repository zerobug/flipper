﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Flipper.Service.Web.Filters
{
    public class CustomAuthorizationFilterAttribute: AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization != null && actionContext.Request.Headers.Authorization.Parameter!=null)
                Thread.CurrentPrincipal = HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(actionContext.Request.Headers.Authorization.Parameter),new[] {"" });
            base.OnAuthorization(actionContext);
        }
    }
}