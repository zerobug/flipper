﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.RRHH.Comision
{
    public class LiquidacionDetalleDto
    {
        public int Id { get; set; }
        public int LiquidacionId { get; set; }
        public string Grupo { get; set; }
        public string Detalle { get; set; }
        public decimal Venta { get; set; }
        public decimal VentaComision { get; set; }
        public decimal Cobranza { get; set; }
        public decimal CobranzaComision { get; set; }
    }
}
