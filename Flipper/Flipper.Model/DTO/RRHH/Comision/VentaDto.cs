﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.RRHH.Comision
{
    public class VentaDto
    {
        public string VendedorCodigo { get; set; }
        public string ClienteCodigo { get; set; }
        public string VendedorNombre { get; set; }
        public string ClienteNombre { get; set; }
        public int Cobranza { get; set; }
        public decimal Importe { get; set; }
    }
}
