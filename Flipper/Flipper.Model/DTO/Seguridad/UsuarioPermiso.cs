﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.Seguridad
{
    public class UsuarioPermiso
    {
        public int UsuarioId { get; set; }
        public bool Autorizar { get; set; }
    }
}
