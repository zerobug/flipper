﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.Objetivos
{
    public class ObjetivoVentaReporteDto
    {
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public int Periodo { get; set; }
        public decimal Objetivo { get; set; }
        public decimal Cargados { get; set; }
        public decimal Autorizados { get; set; }
        public decimal Facturados { get; set; }
        public decimal Rechazados { get; set; }
    }
}
