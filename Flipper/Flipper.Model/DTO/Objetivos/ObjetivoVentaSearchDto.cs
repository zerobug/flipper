﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.Objetivos
{
    public class ObjetivoVentaSearchDto
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public int Periodo { get; set; }
    }
}
