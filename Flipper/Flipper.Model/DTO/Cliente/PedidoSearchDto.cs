﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.Cliente
{
    public class PedidoSearchDto
    {
        public string Cliente { get; set; }
        public string Vendedor { get; set; }
        public string Estado { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
    }
}
