﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M = Flipper.Model.Integracion.Merino;

namespace Flipper.Model.DTO.Cliente
{
    public class ClienteDto
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Cuit { get; set; }
        public string VendedorCodigo { get; set; }
        public string ListaCodigo { get; set; }
        public string ZonaCodigo { get; set; }
        public bool VentaPorPack { get; set; }
        public int Entrega { get; set; }
        public Flete Flete { get; set; }
        public IEnumerable<ClienteSucursal> Sucursales { get; set; }
        public decimal AlicuotaIVA { get; set; }
        public bool DiscriminaIVA { get; set; }
        public string CategoriaIVA { get; set; }
        public string ClaseCodigo { get; set; }
        public string CondicionVenta { get; set; }

    }
}
