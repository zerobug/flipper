﻿using Flipper.Model.Common;
using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.Cliente
{
    public class PedidoEventoDto
    {
        public int Id { get; set; }
        public int PedidoId { get; set; }
        public DateTime Fecha { get; set; }
        public int PedidoEventoTipoId { get; set; }
        public PedidoEventoTipo PedidoEventoTipo { get; set; }
        public int EstadoId { get; set; }
        public Estado Estado { get; set; }
        public string Observacion { get; set; }
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        public string FechaYHora { get {
                return Fecha.ToString("dd/MM/yyyy HH:mm");
            } }
    }
}
