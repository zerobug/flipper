﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO.Cliente
{
    public class PedidoDetalleDto
    {
        public int Id { get; set; }
        public int PedidoId { get; set; }
        public int ProductoId { get; set; }
        public Producto Producto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Bonificacion { get; set; }
        public decimal BonificacionAdicional { get; set; }
        public decimal PedidoCantidad { get; set; }
        public decimal BonificacionCantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal PrecioBruto { get; set; }
        public decimal Descuento { get; set; }
    }
}
