﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M = Flipper.Model.Integracion.Merino;

namespace Flipper.Model.DTO.Cliente
{
    public class PedidoDto
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public ClienteDto Cliente { get; set; }
        public int VendedorId { get; set; }
        public M.Vendedor Vendedor { get; set; }
        public int CondicionVentaId { get; set; }
        public M.CondicionVenta CondicionVenta { get; set; }
        public int Sucursal { get; set; }
        public int Numero { get; set; }
        public DateTime FechaEntrega { get; set; }
        public DateTime Fecha { get; set; }
        public int LugarEntrega { get; set; }
        public int TransporteId { get; set; }
        public HashSet<PedidoDetalleDto> Detalle { get; set; }
        public HashSet<PedidoEventoDto> Eventos { get; set; }
        public int? EstadoId { get; set; }
        public PedidoEventoDto Estado { get; set; }
        public string Observacion { get; set; }
        public string ObservacionCobranza { get; set; }
        public HashSet<M.PedidoObservacion> Observaciones { get; set; }
        public IEnumerable<M.ClienteSucursal> Sucursales { get; set; }
        public bool PuedeAutorizar { get; set; }
        public bool PuedeEditar { get; set; }
        public bool PuedeVer { get; set; }
        public bool TieneObservacion { get { return !string.IsNullOrEmpty(Estado?.Observacion); } }
        public string Leyenda { get; set; }
        public string OrdenDeCompra { get; set; }
        public string Factura { get; set; }
        public string Remito { get; set; }
    }
}
