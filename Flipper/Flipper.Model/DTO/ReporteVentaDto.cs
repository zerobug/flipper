﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO
{
    public class ReporteVentaDto
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Vendedor { get; set; }
        public string VendedorCodigo { get; set; }
        public string Producto { get; set; }
        public string Cliente { get; set; }
        public string Provincia { get; set; }
        public decimal Importe { get; set; }
        public decimal Cantidad { get; set; }
    }
}
