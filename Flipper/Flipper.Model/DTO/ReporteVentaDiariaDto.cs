﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.DTO
{
    public class ReporteVentaDiariaDto
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Importe { get; set; }
        [NotMapped]
        public decimal Cantidad { get; set; }
        [NotMapped]
        public int Dia
        {
            get
            {
                return Fecha.Day;
            }
        }
        [NotMapped]
        public decimal Acumulado { get; set; }
        [NotMapped]
        public decimal Objetivo { get; set; }

    }
}
