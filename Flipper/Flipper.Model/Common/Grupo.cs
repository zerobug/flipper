﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class Grupo : Repository.Pattern.Ef6.Entity
    {
        public enum Comision
        {
            Vendedores=1,
            Exclusivos=2,
            Supermercados=3,
            Gerentes=4
        }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
        public HashSet<GrupoDetalle> Detalle { get; set; }
    }
}
