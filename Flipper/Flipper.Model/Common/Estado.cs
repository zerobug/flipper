﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class Estado : Repository.Pattern.Ef6.Entity
    {
        public enum Pedido
        {
            Cargado = 1,
            Autorizado = 3,
            Rechazado = 2,
            PendienteAdministracion = 4,
            PendienteConfirmacion = 5,
        }

        public int Id { get; set; }
        public Modelo Modelo { get; set; }
        public int ModeloId { get; set; }
        public string Nombre { get; set; }
        public int Orden { get; set; }
        public bool Activo { get; set; }
    }
}
