﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class UsuarioPermiso : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public Usuario Usuario { get; set; }
        public int UsuarioId { get; set; }
        public int PermisoId { get; set; }
        public Permiso Permiso { get; set; }

    }
}
