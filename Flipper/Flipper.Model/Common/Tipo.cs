﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class Tipo : Repository.Pattern.Ef6.Entity
    {

        public int Id { get; set; }
        public int ModeloId { get; set; }
        public Modelo Modelo { get; set; }
        public string Nombre { get; set; }
        public int Orden { get; set; }
        public bool Activo { get; set; }
    }
}
