﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class Workflow : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int EstadoId { get; set; }
        public string Regla { get; set; }
    }
}
