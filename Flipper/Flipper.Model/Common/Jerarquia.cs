﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class Jerarquia : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
        public HashSet<JerarquiaDetalle> Detalle { get; set; }
    }
}
