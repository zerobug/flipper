﻿namespace Flipper.Model.Common
{
    public class Modelo : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
    }
}