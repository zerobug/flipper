﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Common
{
    public class JerarquiaDetalle : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int JerarquiaId { get; set; }
        public Jerarquia Jerarquia { get; set; }
        public string Padre { get; set; }
        public string Hijo { get; set; }
    }
}
