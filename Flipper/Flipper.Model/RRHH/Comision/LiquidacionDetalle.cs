﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.RRHH.Comision
{
    public class LiquidacionDetalle : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int LiquidacionId { get; set; }
        public Liquidacion Liquidacion { get; set; }
        public string Grupo { get; set; }
        public string Detalle { get; set; }
        public decimal Venta { get; set; }
        public decimal Cobranza { get; set; }
    }
}
