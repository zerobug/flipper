﻿using Flipper.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.RRHH.Comision
{
    public class Liquidacion : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int Periodo { get; set; }
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }
        public string Observacion { get; set; }
        public int EstadoId { get; set; }
        public Estado Estado { get; set; }
        public decimal ComisionVentaVendedor { get; set; }
        public decimal ComisionCobranzaVendedor { get; set; }
        public decimal ComisionVentaGerente { get; set; }
        public decimal ComisionCobranzaGerente { get; set; }
    }
}
