﻿using Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Objetivos
{
    public class ObjetivoVenta : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public int Periodo { get; set; }
        public decimal Importe { get; set; }

    }
}
