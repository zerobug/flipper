﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class PedidoObservacionTipo : Repository.Pattern.Ef6.Entity
    {
        public enum Valores
        {
            Venta = 1,
            Cobranza = 2,
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
