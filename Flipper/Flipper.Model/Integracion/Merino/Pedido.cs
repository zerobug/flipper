﻿using System;
using System.Collections.Generic;

namespace Flipper.Model.Integracion.Merino
{
    public class Pedido : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public int CondicionVentaId { get; set; }
        public CondicionVenta CondicionVenta { get; set; }
        public int Sucursal { get; set; }
        public int Numero { get; set; }
        public DateTime FechaEntrega { get; set; }
        public int LugarEntrega { get; set; }
        public int TransporteId { get; set; }
        public HashSet<PedidoDetalle> Detalle { get; set; }
        public HashSet<PedidoEvento> Eventos { get; set; }
        public HashSet<PedidoObservacion> Observaciones { get; set; }
        public int? EstadoId { get; set; }
        public PedidoEvento Estado { get; set; }
        public DateTime Fecha { get; set; }
        public string Leyenda { get; set; }
        public string OrdenDeCompra { get; set; }
    }
}
