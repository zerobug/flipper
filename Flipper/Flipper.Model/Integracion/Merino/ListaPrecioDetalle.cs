﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class ListaPrecioDetalle: Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string ListaCodigo { get; set; }
        public string ProductoCodigo { get; set; }
        public decimal Precio { get; set; }

    }
}
