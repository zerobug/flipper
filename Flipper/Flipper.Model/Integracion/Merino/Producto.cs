﻿namespace Flipper.Model.Integracion.Merino
{
    public class Producto : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public decimal ContenedorCantidad { get; set; }
        public decimal Bonificacion { get; set; }
        public decimal Precio
        {
            get
            {
                if(Descuento>0)
                {
                    return PrecioBruto * (100-Descuento) / 100;
                }
                else
                {
                    return PrecioBruto;
                }

            }
        }
        public string Moneda { get; set; }
        public string MarcaCodigo { get; set; }
        public decimal Descuento { get; set; }
        public string RubroCodigo { get; set; }
        public decimal PrecioBruto { get; set; }
        public bool RemiteSeparado { get; set; }
    }
}
