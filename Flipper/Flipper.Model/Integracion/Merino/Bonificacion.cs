﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class Bonificacion : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string ProductoCodigo { get; set; }
        public string ClaseCodigo { get; set; }
        public string ZonaCodigo { get; set; }
        public string ClienteCodigo { get; set; }
        public string RubroCodigo { get; set; }
        public string MarcaCodigo { get; set; }
        public decimal Valor { get; set; }
    }
}
