﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class RemitoConFactura : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Punto { get; set; }
        public string Numero { get; set; }
        public string PedidoPunto { get; set; }
        public string PedidoNumero { get; set; }
        public string FacturaPunto { get; set; }
        public string FacturaNumero { get; set; }
    }
}
