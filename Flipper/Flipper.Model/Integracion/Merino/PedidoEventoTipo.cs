﻿namespace Flipper.Model.Integracion.Merino
{
    public class PedidoEventoTipo : Repository.Pattern.Ef6.Entity
    {
        public enum Valores
        {
            Carga=1,
            Control=2,
        }
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
