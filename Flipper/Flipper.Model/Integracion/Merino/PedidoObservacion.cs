﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class PedidoObservacion : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int PedidoId { get; set; }
        public Pedido Pedido { get; set; }
        public DateTime Fecha { get; set; }
        public int TipoId { get; set; }
        public PedidoObservacion Tipo { get; set; }
        public string Observacion { get; set; }

        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

    }
}
