﻿using Flipper.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class Usuario : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Login { get; set; }
        public string Clave { get; set; }
        public bool Activo { get; set; }
        public int PuntoVenta { get; set; }
        public HashSet<UsuarioPermiso> Permisos { get; set; }
    }
}
