﻿namespace Flipper.Model.Integracion.Merino
{
    public class Vendedor : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
    }
}
