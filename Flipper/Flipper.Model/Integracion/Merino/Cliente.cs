﻿namespace Flipper.Model.Integracion.Merino
{
    public class Cliente: Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Cuit { get; set; }
        public string VendedorCodigo { get; set; }
        public string ListaCodigo { get; set; }
        public string ZonaCodigo { get; set; }
        public string FleteCodigo { get; set; }
        public decimal Descuento { get; set; }
        public decimal Bonificacion { get; set; }
        public decimal AlicuotaIVA { get; set; }
        public bool DiscriminaIVA { get; set; }
        public string CategoriaIVA { get; set; }
        public string ClaseCodigo { get; set; }
        public string CondicionVenta { get; set; }

    }
}
