﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class Empaque : Repository.Pattern.Ef6.Entity
    {
        public const string Pack = "6     ";
        
        public int Id { get; set; }
        public string BultoCodigo { get; set; }
        public string ProductoCodigo { get; set; }
        public decimal Cantidad { get; set; }
    }
}
