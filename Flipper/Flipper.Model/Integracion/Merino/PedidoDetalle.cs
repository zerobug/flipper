﻿using System.Runtime.Serialization;

namespace Flipper.Model.Integracion.Merino
{
    public class PedidoDetalle : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int PedidoId { get; set; }
        public Pedido Pedido { get; set; }
        public int ProductoId { get; set; }
        public Producto Producto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Bonificacion { get; set; }
        public decimal BonificacionAdicional { get; set; }
        public decimal PedidoCantidad { get; set; }
        public decimal BonificacionCantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Descuento { get; set; }
        public decimal PrecioBruto { get; set; }
    }
}