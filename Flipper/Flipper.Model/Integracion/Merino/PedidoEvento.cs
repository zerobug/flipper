﻿using Flipper.Model.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Flipper.Model.Integracion.Merino
{
    public class PedidoEvento : Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public int PedidoId { get; set; }
        public Pedido Pedido { get; set; }
        public DateTime Fecha { get; set; }
        public int PedidoEventoTipoId { get; set; }
        public PedidoEventoTipo PedidoEventoTipo { get; set; }
        public int EstadoId { get; set; }
        public Estado Estado { get; set; }
        public string Observacion { get; set; }
        public HashSet<Pedido> Pedidos { get; set; }
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

    }
}
