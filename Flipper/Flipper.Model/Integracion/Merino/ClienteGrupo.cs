﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Model.Integracion.Merino
{
    public class ClienteGrupo: Repository.Pattern.Ef6.Entity
    {
        public int Id { get; set; }
        public string ClienteCodigo { get; set; }
        public string GrupoCodigo { get; set; }
    }
}
