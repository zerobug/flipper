﻿using Flipper.Model.Objetivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flipper.Model.DTO.Objetivos;
using Flipper.Service.Result;
using System.Data.Entity;
using Flipper.Model.Integracion.Merino;
using Flipper.Model.Common;

namespace Flipper.Service.Objetivos
{
    public class ObjetivoVentaService:BaseService
    {
        public int Grabar(ObjetivoVenta valor, int usuarioId)
        {
            try
            {
                if (ValidarDuplicado(valor))
                    throw new Exception();
                var padre = usuarioId.ToString();
                var hijo = valor.VendedorId.ToString();
                var jerarquia = Queryable<JerarquiaDetalle>().Where(i => i.JerarquiaId == 1 && i.Padre == padre && i.Hijo== hijo).Any();
                if (!jerarquia)
                {
                    throw new Exception("No posee el permiso para el vendedor seleccionado");
                }
                if (valor.Id == 0)
                {
                    UOW.Repository<ObjetivoVenta>().Insert(valor);
                }else
                {
                    UOW.Repository<ObjetivoVenta>().Update(valor);
                }
                UOW.SaveChanges();
                return valor.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool ValidarDuplicado(ObjetivoVenta valor)
        {
            return Queryable<ObjetivoVenta>().Where(i => i.Periodo == valor.Periodo && i.VendedorId == valor.VendedorId && i.Id != valor.Id).Any();
        }

        public GenericResult<ObjetivoVenta> GetById(int id)
        {
            if (id == 0)
                return new GenericResult<ObjetivoVenta>() { Valor = new ObjetivoVenta() };
                var entity = Query<ObjetivoVenta>(i => i.Id == id)
                    .Include(i => i.Vendedor)
                    .Select().First();
                return new GenericResult<ObjetivoVenta>() { Valor = entity };
        }

        public List<ObjetivoVentaReporteDto> Reporte(ObjetivoVentaSearchDto valor, int usuarioId)
        {
            var perfil = new SeguridadService().GetById(usuarioId).Valor;
            var vendedor = perfil.Codigo;
            var resultado = new List<ObjetivoVentaReporteDto>();
            var padre = usuarioId.ToString();
            var jerarquia = Queryable<JerarquiaDetalle>().Where(i => i.JerarquiaId == 1 && i.Padre == padre).Select(i => i.Hijo).ToList().Select(i => int.Parse(i));

            var objetivoquery = Queryable<ObjetivoVenta>().Include(i => i.Vendedor);
            if (!string.IsNullOrWhiteSpace(vendedor))
            {
                objetivoquery = objetivoquery.Where(i => i.Vendedor.Codigo == vendedor);
            }
            else
            {
                objetivoquery = objetivoquery.Where(i => jerarquia.Contains(i.VendedorId));

            }
            var lista = objetivoquery.Select(i => new ObjetivoVentaReporteDto { Vendedor = i.Vendedor, Periodo = i.Periodo, VendedorId = i.VendedorId, Objetivo = i.Importe }).ToList();

            var fechadesde = DateTime.ParseExact($"{valor.Periodo}01", "yyyyMMdd", null);
            var fechahasta = fechadesde.AddMonths(1).AddDays(-1);
            foreach (var item in lista)
            {
                var query = Queryable<Pedido>().Include(i => i.Detalle).Include(i => i.Estado)
                    .Where(i => i.VendedorId == item.VendedorId && i.Fecha >= fechadesde && i.Fecha <= fechahasta)
                    .ToList();
                foreach (var pedido in query)
                {
                    if (pedido.Estado.EstadoId == (int)Estado.Pedido.Autorizado)
                    {
                        var sucursal = pedido.Sucursal.ToString().PadLeft(4, '0');
                        var numero = pedido.Numero.ToString().PadLeft(8,' ');
                        var remito = Queryable<RemitoConFactura>().Where(i => i.PedidoPunto == sucursal && i.PedidoNumero == numero).Any();
                        if (remito)
                        {
                            item.Facturados += pedido.Detalle.Sum(i => i.Precio * i.Cantidad);
                        }
                        else
                        {
                            item.Autorizados += pedido.Detalle.Sum(i => i.Precio * i.Cantidad);
                        }

                    }
                    else if (pedido.Estado.EstadoId == (int)Estado.Pedido.Rechazado)
                        item.Rechazados += pedido.Detalle.Sum(i => i.Precio * i.Cantidad);
                    else
                        item.Cargados += pedido.Detalle.Sum(i => i.Precio * i.Cantidad);
                }
                resultado.Add(item);
            }
            return resultado.ToList();
        }

        public List<ObjetivoVenta> Search(ObjetivoVentaSearchDto valor, int usuarioId)
        {
            var query = Queryable<ObjetivoVenta>().Include(i => i.Vendedor);
            var padre = usuarioId.ToString();
            var jerarquia = Queryable<JerarquiaDetalle>().Where(i => i.JerarquiaId == 1 && i.Padre == padre).Select(i => i.Hijo).ToList().Select(i => int.Parse(i));

            if (valor.VendedorId > 0)
                query = query.Where(i => i.VendedorId == valor.VendedorId);

            if (valor.Periodo > 0)
                query = query.Where(i => i.Periodo == valor.Periodo);

            query = query.Where(i => jerarquia.Contains(i.VendedorId));

            return query.ToList();
        }
    }
}
