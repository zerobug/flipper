﻿using Flipper.Core.Helper;
using Flipper.Data;
using Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service
{
    public class ProductoService: BaseService
    {
        public GenericResult<HashSet<Producto>> GetAll()
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                return new GenericResult<HashSet<Producto>>() { Valor = new HashSet<Producto>(UOW.Repository<Producto>().Query().Select().ToList()) };
            }
        }
        public GenericResult<Producto> Get(int id)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var producto = UOW.Repository<Producto>().Query(i => i.Id == id).Select().Single();
                var pack = UOW.Repository<Empaque>().Query(i => i.ProductoCodigo == producto.Codigo && i.BultoCodigo == Empaque.Pack).Select();

                producto.RemiteSeparado= UOW.Repository<ProductoGrupo>().Query(i => i.ProductoCodigo == producto.Codigo && i.GrupoCodigo == "RXS").Select().Any();

                if (pack.Any())
                    producto.ContenedorCantidad = pack.First().Cantidad;

                if (producto.ContenedorCantidad==0)
                    producto.ContenedorCantidad = 1;

                return new GenericResult<Producto>() { Valor = producto };
            }
        }
        public decimal GetBonificacion(string producto, string clase)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var bonificacion = UOW.Repository<Bonificacion>().Query(i => i.ProductoCodigo == producto && i.ClaseCodigo==clase).Select();
                if (bonificacion.Any())
                    return bonificacion.First().Valor;
                return 0;
            }
        }
        public decimal GetBonificacion(Producto producto, Model.DTO.Cliente.ClienteDto cliente)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var bonificacion = UOW.Repository<Descuento>().Query(i =>
                (i.ProductoCodigo == producto.Codigo && (i.ClienteCodigo == cliente.Codigo ||
                (cliente.ZonaCodigo.StartsWith(i.ZonaCodigo) && i.ZonaCodigo != "") ||
                (cliente.ClaseCodigo.StartsWith(i.ClaseCodigo) && i.ClaseCodigo != ""))) ||
                ((producto.RubroCodigo.Contains(i.RubroCodigo) && i.RubroCodigo != "") &&
                (i.ClienteCodigo == cliente.Codigo ||
                (cliente.ZonaCodigo.StartsWith(i.ZonaCodigo) && i.ZonaCodigo != "") ||
                (cliente.ClaseCodigo.StartsWith(i.ClaseCodigo) && i.ClaseCodigo != ""))) ||
                ((producto.MarcaCodigo.Contains(i.MarcaCodigo) && i.MarcaCodigo != "") &&
                (i.ClienteCodigo == cliente.Codigo ||
                (cliente.ZonaCodigo.StartsWith(i.ZonaCodigo) && i.ZonaCodigo != "") ||
                (cliente.ClaseCodigo.StartsWith(i.ClaseCodigo) && i.ClaseCodigo != "")))
                ).Select();
                if (bonificacion.Any())
                    return bonificacion.First().Valor;
                return 0;
            }
        }

        public GenericResult<Producto> Get(int id,string lista )
        {
            var resultado = Get(id);
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var precio = UOW.Repository<ListaPrecioDetalle>().Query(i => i.ProductoCodigo == resultado.Valor.Codigo && i.ListaCodigo == lista).Select();
                if (precio.Any())
                {
                    resultado.Valor.PrecioBruto = precio.First().Precio;
                }

                var bonificacion = UOW.Repository<Bonificacion>().Query(i => i.ProductoCodigo == resultado.Valor.Codigo && i.ClienteCodigo == string.Empty).Select();
                if (bonificacion.Any())
                    resultado.Valor.Bonificacion = bonificacion.First().Valor;
                return resultado;
            }
        }

        public GenericResult<Producto> Get(int id, int cliente)
        {
            var clienteResultado = new ClienteService().Get(cliente);
            var claseCodigo = clienteResultado.Valor.ClaseCodigo;
            var clienteCodigo = clienteResultado.Valor.Codigo;
            var resultado = Get(id, clienteResultado.Valor.ListaCodigo);
            if(!clienteResultado.Valor.DiscriminaIVA)
            {
                resultado.Valor.PrecioBruto = resultado.Valor.PrecioBruto * (100 + clienteResultado.Valor.AlicuotaIVA) / 100;
            }
            if (SettingHelper.Empresa == (int)SettingHelper.Empresas.Konig)
            {
                resultado.Valor.Bonificacion = GetBonificacion(resultado.Valor.Codigo, claseCodigo);
            }
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                IEnumerable<Descuento> descuento = null;
                if (SettingHelper.Empresa == (int)SettingHelper.Empresas.Konig)
                {
                    descuento = UOW.Repository<Descuento>().Query(i => i.ClaseCodigo == claseCodigo && i.ProductoCodigo == resultado.Valor.Codigo).Select();
                    if (descuento.Any())
                    {
                        resultado.Valor.Descuento = descuento.First().Valor;
                    }
                }else
                {
                    descuento = UOW.Repository<Descuento>().Query(i => i.ClienteCodigo == clienteCodigo && i.MarcaCodigo == resultado.Valor.MarcaCodigo).Select();
                    if (descuento.Any())
                    {
                        resultado.Valor.Descuento = descuento.First().Valor;
                    }
                    descuento = UOW.Repository<Descuento>().Query(i => i.ClienteCodigo == clienteCodigo && i.ProductoCodigo == resultado.Valor.Codigo).Select();
                    if (descuento.Any())
                    {
                        resultado.Valor.Descuento = descuento.First().Valor;
                    }

                }
                return resultado;
            }
        }

        public GenericResult<HashSet<Producto>> Search(string valor)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var valorSinCodigo = valor;
                if (valor != null)
                { 
                    valorSinCodigo = valor.Split('(').FirstOrDefault() ?? valor;
                }
                return new GenericResult<HashSet<Producto>>() { Valor = new HashSet<Producto>(UOW.Repository<Producto>().Query(i => i.Nombre.Contains(valorSinCodigo) || i.Codigo == valorSinCodigo).Select().ToList()) };
            }
        }
    }
}
