﻿using Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service
{
    public class SeguridadService:BaseService
    {
        public int Autenticar(string login, string clave)
        {
            var check = UOW.Repository<Usuario>().Queryable().Where(i => i.Login == login && i.Clave == clave).Take(1).ToList();
            if (check.Count==1)
                return check.Single().Id;
            return 0;
        }

        public GenericResult<Usuario> GetById(int id)
        {
            return new GenericResult<Usuario>() { Valor = Query<Usuario>(i => i.Id == id).Include(i=>i.Permisos).Select().Single() };
        }
        public GenericResult<Usuario> GetByCodigo(string codigo)
        {
            return new GenericResult<Usuario>() { Valor = Query<Usuario>(i => i.Codigo == codigo).Include(i => i.Permisos).Select().Single() };
        }
    }
}
