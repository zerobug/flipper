﻿using Flipper.Data;
using NLog;
using Repository.Pattern.Ef6;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Flipper.Service
{
    public class BaseService
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();
        protected Repository.Pattern.Ef6.UnitOfWork UOW;
        protected DbContext Context;

        public BaseService()
        {
            Context = new DbContext();
            UOW = new Repository.Pattern.Ef6.UnitOfWork(Context);
        }

        protected QueryFluent<T> Query<T>() where T : Entity
        {
            return new QueryFluent<T>((Repository<T>)UOW.Repository<T>());
        }

        protected QueryFluent<T> Query<T>(Expression<Func<T, bool>> expression) where T : Entity
        {
            return new QueryFluent<T>((Repository<T>)UOW.Repository<T>(), expression);
        }

        protected IQueryable<T> Queryable<T>() where T : Entity
        {
            return UOW.Repository<T>().Queryable();
        }
        protected Repository.Pattern.Ef6.UnitOfWork NewUnitOfWork()
        {
            return new Repository.Pattern.Ef6.UnitOfWork(new DbContext());
        }
    }
}
