﻿using Flipper.Data;
using Flipper.Model.Common;
using Flipper.Model.DTO.RRHH.Comision;
using Flipper.Model.RRHH.Comision;
using Flipper.Service.Result;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service.RRHH
{
    public class ComisionService:BaseService
    {
        public LiquidacionDto Grabar(LiquidacionDto valor)
        {
                Liquidacion liquidacion = new Liquidacion();
                var liquidacionQuery = Query<Liquidacion>(i => i.Id == valor.Id).Select();
                if (liquidacionQuery.Count() == 0)
                {
                    liquidacion = new Liquidacion();
                    UOW.Repository<Liquidacion>().Insert(liquidacion);
                }
                else
                {
                    liquidacion = liquidacionQuery.First();
                liquidacion.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                }
                liquidacion = valor.Adapt(liquidacion);

                if (liquidacion.Id == 0)
                {
                    liquidacion.EstadoId = 1;
                }

                UOW.SaveChanges();
                return liquidacion.Adapt<LiquidacionDto>();

        }

        public LiquidacionDto GetDetalle(string desde, string hasta)
        {

            return GetDetalle(new Liquidacion() { Desde=DateTime.ParseExact(desde, "dd/MM/yyyy", null), Hasta =DateTime.ParseExact(hasta, "dd/MM/yyyy", null) });
        }

        public LiquidacionDto GetDetalle(int id)
        {
            var liquidacion = GetById(id);
            return GetDetalle(liquidacion.Adapt<Liquidacion>());
        }

        public LiquidacionDto GetDetalle(LiquidacionDto valor)
        {
            return GetDetalle(valor.Adapt<Liquidacion>());
        }

        public LiquidacionDto GetDetalle(Liquidacion valor)
        {

            var resultado = new LiquidacionDto();
            List<VentaDto> venta = null;
            var desde = valor.Desde.ToString("yyyyMMdd");
            var hasta = valor.Hasta.ToString("yyyyMMdd");
            using (var db = new MerinoContext())
            {
                venta = db.Database.SqlQuery<VentaDto>("getventaParaComisionesV2  @p0, @p1", desde, hasta).ToList();
            }
            var detalles = CreateDetalle(venta);
            resultado.Vendedores = new HashSet<LiquidacionDetalleDto>(detalles.Where(i => i.Grupo == Grupo.Comision.Vendedores.ToString()).Select(i=>i.Adapt<LiquidacionDetalleDto>()));
            foreach (var item in resultado.Vendedores)
            {
                item.VentaComision = item.Venta * valor.ComisionVentaVendedor / 100;
                item.CobranzaComision = item.Cobranza * valor.ComisionCobranzaVendedor/ 100;
            }
            resultado.Exclusivos = new HashSet<LiquidacionDetalleDto>(detalles.Where(i => i.Grupo == Grupo.Comision.Exclusivos.ToString()).Select(i => i.Adapt<LiquidacionDetalleDto>()));
            resultado.Supermercados= new HashSet<LiquidacionDetalleDto>(detalles.Where(i => i.Grupo == Grupo.Comision.Supermercados.ToString()).Select(i => i.Adapt<LiquidacionDetalleDto>()));
            resultado.Gerentes = new HashSet<LiquidacionDetalleDto>(detalles.Where(i => i.Grupo == Grupo.Comision.Gerentes.ToString()).Select(i => i.Adapt<LiquidacionDetalleDto>()));
            foreach (var item in resultado.Gerentes)
            {
                item.VentaComision = item.Venta * valor.ComisionVentaGerente / 100;
                item.CobranzaComision = item.Cobranza * valor.ComisionCobranzaGerente/ 100;
            }
            resultado.Totales = new Dictionary<int, decimal[]>();
            resultado.Totales.Add(0, new decimal[] { resultado.Vendedores.Sum(i => i.Venta), resultado.Vendedores.Sum(i => i.VentaComision), resultado.Vendedores.Sum(i => i.Cobranza), resultado.Vendedores.Sum(i => i.CobranzaComision) });
            resultado.Totales.Add(1, new decimal[] { resultado.Exclusivos.Sum(i => i.Venta), resultado.Exclusivos.Sum(i => i.VentaComision), resultado.Exclusivos.Sum(i => i.Cobranza), resultado.Exclusivos.Sum(i => i.CobranzaComision) });
            resultado.Totales.Add(2, new decimal[] { resultado.Supermercados.Sum(i => i.Venta), resultado.Supermercados.Sum(i => i.VentaComision), resultado.Supermercados.Sum(i => i.Cobranza), resultado.Supermercados.Sum(i => i.CobranzaComision) });
            resultado.Totales.Add(3, new decimal[] { resultado.Gerentes.Sum(i => i.Venta), resultado.Gerentes.Sum(i => i.VentaComision), resultado.Gerentes.Sum(i => i.Cobranza), resultado.Gerentes.Sum(i => i.CobranzaComision) });
            return resultado;
        }

        private IEnumerable<LiquidacionDetalle> CreateDetalle(List<VentaDto> ventas)
        {
            decimal ventaGeneral = 0;
            decimal cobranzaGeneral = 0;
            var resultado = new List<LiquidacionDetalle>();
            var grupoIds = new int[] { (int)Grupo.Comision.Exclusivos, (int)Grupo.Comision.Gerentes, (int)Grupo.Comision.Vendedores, (int)Grupo.Comision.Supermercados};
            var grupos = Query<Grupo>(i => grupoIds.Contains(i.Id)).Include(i => i.Detalle).Select().ToList();

            var vendedores = grupos.Where(i => i.Id == (int)Grupo.Comision.Vendedores).Single().Detalle.Select(i => i.Valor);
            var ventasVendedores = ventas.Where(i => vendedores.Contains(i.VendedorCodigo.Trim())).GroupBy(i => new { i.VendedorCodigo, i.VendedorNombre });
            foreach (var item in ventasVendedores)
            {
                resultado.Add(new LiquidacionDetalle() {
                    Grupo = Grupo.Comision.Vendedores.ToString(),
                    Detalle = $"{item.Key.VendedorCodigo} - {item.Key.VendedorNombre}",
                    Venta = item.Where(i=> i.Cobranza == 0).Sum(i => i.Importe),
                    Cobranza = item.Where(i => i.Cobranza == 1).Sum(i => i.Importe) });
                ventaGeneral += item.Where(i => i.Cobranza == 0).Sum(i => i.Importe);
                cobranzaGeneral += item.Where(i => i.Cobranza == 1).Sum(i => i.Importe);
            }
            var exclusivos = grupos.Where(i => i.Id == (int)Grupo.Comision.Exclusivos).Single().Detalle.Select(i => i.Valor);
            var ventasExclusivos = ventas.Where(i => i.Cobranza == 0 && exclusivos.Contains(i.ClienteCodigo.Trim())).GroupBy(i => new { i.ClienteCodigo, i.ClienteNombre });
            foreach (var item in ventasExclusivos)
            {
                resultado.Add(new LiquidacionDetalle() {
                    Grupo = Grupo.Comision.Exclusivos.ToString(),
                    Detalle = $"{item.Key.ClienteCodigo} - {item.Key.ClienteNombre}",
                    Venta = item.Where(i => i.Cobranza == 0).Sum(i => i.Importe),
                    Cobranza = item.Where(i => i.Cobranza == 1).Sum(i => i.Importe) });
                ventaGeneral += item.Where(i => i.Cobranza == 0).Sum(i => i.Importe);
                cobranzaGeneral += item.Where(i => i.Cobranza == 1).Sum(i => i.Importe);
            }
            var supermercados = grupos.Where(i => i.Id == (int)Grupo.Comision.Supermercados).Single().Detalle.Select(i => i.Valor);
            var ventasSupermercados = ventas.Where(i => supermercados.Contains(i.VendedorCodigo.Trim())).GroupBy(i => new { i.VendedorCodigo, i.VendedorNombre });
            foreach (var item in ventasSupermercados)
            {
                resultado.Add(new LiquidacionDetalle() {
                    Grupo = Grupo.Comision.Supermercados.ToString(),
                    Detalle = $"{item.Key.VendedorCodigo} - {item.Key.VendedorNombre}",
                    Venta = item.Where(i => i.Cobranza == 0).Sum(i => i.Importe),
                    Cobranza = item.Where(i => i.Cobranza == 1).Sum(i => i.Importe) });
                ventaGeneral += item.Where(i => i.Cobranza == 0).Sum(i => i.Importe);
                cobranzaGeneral += item.Where(i => i.Cobranza == 1).Sum(i => i.Importe);
            }
            var gerentes = grupos.Where(i => i.Id == (int)Grupo.Comision.Gerentes).Single().Detalle.Select(i => i.Valor);
            foreach (var item in gerentes)
            {
                resultado.Add(new LiquidacionDetalle() { Grupo = Grupo.Comision.Gerentes.ToString(), Detalle = item, Venta = ventaGeneral, Cobranza=cobranzaGeneral });
            }

            return resultado;
        }

        public LiquidacionDto GetById(int id)
        {
            var liquidacion = Query<Liquidacion>(i => i.Id == id).Select().Single();
            return liquidacion.Adapt<LiquidacionDto>();
        }

        public IEnumerable<LiquidacionDto> Search(string valor)
        {


            return Query<Liquidacion>().Select().Adapt<IEnumerable<LiquidacionDto>>();
        }
    }
}
