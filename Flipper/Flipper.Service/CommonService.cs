﻿using Flipper.Service.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service
{
    public class CommonService
    {
        public static ErrorResult GetError(string mensaje)
        {
            return new ErrorResult(mensaje);
        }
    }
}
