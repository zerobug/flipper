﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service.Result
{
    public class GenericResult<T>:BaseResult
    {
        public T Valor { get; set; }
    }
}
