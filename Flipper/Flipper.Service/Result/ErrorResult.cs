﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service.Result
{
    public class ErrorResult: BaseResult
    {
        public ErrorResult(string mensaje):base()
        {
            Exito = false;
            Errores = new HashSet<Error>() { new Error() { Descripcion = mensaje } };
        }
    }
}
