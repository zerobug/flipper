﻿namespace Flipper.Service.Result
{
    public class Error
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}