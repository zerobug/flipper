﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service.Result
{
    public class BaseResult
    {
        public bool Exito { get; set; }
        public HashSet<Error> Errores { get; set; }
        public string ResumenError
        {
            get
            {
                if (Errores == null || Errores.Count() == 0)
                    return "";
                return string.Join(Environment.NewLine, Errores.Select(i => i.Descripcion));
            }
        }

        public void AddError(string mensaje)
        {
            Exito = false;
            if (Errores == null)
                Errores = new HashSet<Error>();

            Errores.Add(new Error() { Descripcion = mensaje });
        }

        public BaseResult()
        {
            Exito = true;
        }
    }
}
