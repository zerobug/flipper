﻿using M=Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service
{
    public static class MockService
    {
        public static HashSet<M.Cliente> Clientes { get; set; }
        public static HashSet<M.Producto> Productos { get; set; }
        public static HashSet<M.Pedido> Pedidos { get; set; }
        static MockService()
        {
            Clientes = new HashSet<M.Cliente>();
            Productos = new HashSet<M.Producto>();
            Pedidos = new HashSet<M.Pedido>();
            Clientes.Add(new M.Cliente() { Id = 3380, Codigo = "D16676", Nombre = "DISTRIBUIDORA BALLESTER DE JORGE ASIS", ZonaCodigo = "2" });
            //Clientes.Add(new Cliente() { Id = 3415, Codigo = "D16677", Nombre = "DISTRIBUIDORA MONTEMAYOR S.A", Zona = 2 });
            //Clientes.Add(new Cliente() { Id = 3464, Codigo = "D16678", Nombre = "DISTRIBUIDORA MORCHON", Zona = 2 });
            //Clientes.Add(new Cliente() { Id = 3565, Codigo = "D16680", Nombre = "INSUNORTE", Zona = 2 });
            //Clientes.Add(new Cliente() { Id = 53, Codigo = "D2020", Nombre = "ORGANIZACION VET.PAMPA SUR", Zona = 1 });
            //Clientes.Add(new Cliente() { Id = 11, Codigo = "D2057", Nombre = "DROVET  S.A.", Zona = 1 });
            //Clientes.Add(new Cliente() { Id = 55, Codigo = "D2059", Nombre = "DISTRIB. AVENIDA DE GRACIUTTI", Zona = 1 });

            //Productos.Add(new M.Producto() { Id = 1, Codigo = "1-9019", Nombre = "BACTROVET PRATA x 500 mL", Bonificacion = 10, ContenedorCantidad = 6, Precio = 6 });
            //Productos.Add(new M.Producto() { Id = 2, Codigo = "1-9021", Nombre = "BACTROVET PRATA SEMI x 312,5 Kg", Bonificacion = 15, ContenedorCantidad = 12, Precio = 12 });
            //Productos.Add(new M.Producto() { Id = 3, Codigo = "1-9061", Nombre = "BASKEN PLUS x 4 comprimidos", Bonificacion = 20, ContenedorCantidad = 15, Precio = 15 });
            //Productos.Add(new M.Producto() { Id = 4, Codigo = "1-9062", Nombre = "BASKEN PLUS 40 x 4 comprimidos", Bonificacion = 25, ContenedorCantidad = 18, Precio = 18 });
            //Productos.Add(new M.Producto() { Id = 5, Codigo = "1-9063", Nombre = "BASKEN PLUS 20 x 4 comprimidos", Bonificacion = 30, ContenedorCantidad = 20, Precio = 20 });

            var pedido = new M.Pedido();
            pedido.Id = 1;
            pedido.Cliente = Clientes.ToArray()[0];
            pedido.Vendedor = new M.Vendedor() { Id = 1, Codigo = "", Nombre = "Gabriel" };
            pedido.FechaEntrega = new DateTime(2016, 5, 2);
            pedido.Sucursal = 50;
            pedido.Numero = 234;
            pedido.Detalle = new HashSet<M.PedidoDetalle>();
            //pedido.Detalle.Add(new M.PedidoDetalle() { Id=1, Cantidad = 10, Bonificacion = 15, CantidadBonificada = 1, CantidadCargo = 14, Precio = Productos.ToArray()[0].Precio, Producto = Productos.ToArray()[0] });
            //pedido.Detalle.Add(new M.PedidoDetalle() { Id = 2, Cantidad = 10, Bonificacion = 15, CantidadBonificada = 1, CantidadCargo = 14, Precio = Productos.ToArray()[1].Precio, Producto = Productos.ToArray()[1] });
            //pedido.Detalle.Add(new M.PedidoDetalle() { Id = 3, Cantidad = 10, Bonificacion = 15, CantidadBonificada = 1, CantidadCargo = 14, Precio = Productos.ToArray()[2].Precio, Producto = Productos.ToArray()[2] });
            Pedidos.Add(pedido);
            pedido = new M.Pedido();
            pedido.Id = 3;
            pedido.Cliente = Clientes.ToArray()[1];
            pedido.Vendedor = new M.Vendedor() { Id = 1, Codigo = "", Nombre = "Gabriel" };
            pedido.FechaEntrega = new DateTime(2016, 5, 3);
            pedido.Sucursal = 50;
            pedido.Numero = 235;
            pedido.Detalle = new HashSet<M.PedidoDetalle>();
            //pedido.Detalle.Add(new M.PedidoDetalle() { Id = 4, Cantidad = 10, Bonificacion = 15, CantidadBonificada = 1, CantidadCargo = 14, Precio = Productos.ToArray()[0].Precio, Producto = Productos.ToArray()[0] });
            //pedido.Detalle.Add(new M.PedidoDetalle() { Id = 5, Cantidad = 10, Bonificacion = 15, CantidadBonificada = 1, CantidadCargo = 14, Precio = Productos.ToArray()[1].Precio, Producto = Productos.ToArray()[1] });
            //pedido.Detalle.Add(new M.PedidoDetalle() { Id = 6, Cantidad = 10, Bonificacion = 15, CantidadBonificada = 1, CantidadCargo = 14, Precio = Productos.ToArray()[2].Precio, Producto = Productos.ToArray()[2] });
            Pedidos.Add(pedido);
        }
    }
}
