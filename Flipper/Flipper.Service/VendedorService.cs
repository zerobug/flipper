﻿using Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service
{
    public class VendedorService:BaseService
    {
        public GenericResult<Vendedor> Get(string codigo)
        {
            return new GenericResult<Vendedor>() { Valor = UOW.Repository<Vendedor>().Query(i => i.Codigo == codigo).Select().Single() };
        }

        public GenericResult<Vendedor> GetById(int id)
        {
            return new GenericResult<Vendedor>() { Valor = UOW.Repository<Vendedor>().Query(i => i.Id == id).Select().Single() };
        }

        public GenericResult<HashSet<Vendedor>> Search(string valor, string id)
        {
            var query = UOW.Repository<Vendedor>().Queryable();
            int vendedorId = 0;
            int.TryParse(id, out vendedorId);
            query = query.Where(i => (i.Nombre.Contains(valor) || i.Id == vendedorId));

            return new GenericResult<HashSet<Vendedor>>() { Valor = new HashSet<Vendedor>(query.ToList()) };
        }
    }
}
