﻿using Flipper.Data;
using Flipper.Service.Result;
using System.Collections.Generic;
using System.Linq;
using M = Flipper.Model.Integracion.Merino;

namespace Flipper.Service
{
    public class FleteService:BaseService
    {
        public GenericResult<HashSet<M.Flete>> Search(string valor)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var query = UOW.Repository<M.Flete>().Queryable();
                query = query.Where(i => (i.Nombre.Contains(valor) || i.Codigo == valor));

                return new GenericResult<HashSet<M.Flete>>() { Valor = new HashSet<M.Flete>(query.ToList()) };
            }
        }

        public GenericResult<M.Flete> GetById(int valor)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var query = UOW.Repository<M.Flete>().Query(i=>i.Id==valor).Select();

                return new GenericResult<M.Flete>() { Valor = query.First() };
            }
        }

        public GenericResult<M.Flete> Get(string valor)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var query = UOW.Repository<M.Flete>().Queryable();
                query = query.Where(i => (i.Nombre.Contains(valor) || i.Codigo == valor));

                return new GenericResult<M.Flete>() { Valor = query.First() };
            }
        }
    }
}
