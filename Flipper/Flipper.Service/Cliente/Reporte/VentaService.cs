﻿using Flipper.Data;
using Flipper.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flipper.Service.Cliente.Reporte
{
    public class VentaService:BaseService
    {
        public IEnumerable<ReporteVentaDto> ByVendedor(string desde, string hasta)
        {
            var resultado = new List<ReporteVentaDto>();
            using(var db = new MerinoContext())
            {
                resultado = db.Database.SqlQuery<ReporteVentaDto>("getReporteVentaV2 @p0, @p1", desde, hasta).ToList();
            }
            return resultado;
        }

        public IEnumerable<ReporteVentaDto> ByProducto(string desde, string hasta)
        {
            var resultado = new List<ReporteVentaDto>();
            using (var db = new MerinoContext())
            {
                resultado = db.Database.SqlQuery<ReporteVentaDto>("getReporteVentaPorProductoV2  @p0, @p1", desde, hasta).ToList();
            }
            return resultado;
        }

        public IEnumerable<ReporteVentaDto> ByClienteVendedor(string desde, string hasta)
        {
            var resultado = new List<ReporteVentaDto>();
            using (var db = new MerinoContext())
            {
                resultado = db.Database.SqlQuery<ReporteVentaDto>("getreporteventaclienteV2  @p0, @p1", desde, hasta).ToList();
            }
            return resultado;
        }

        public IEnumerable<ReporteVentaDto> ByProductoClienteVendedor(string desde, string hasta)
        {
            var resultado = new List<ReporteVentaDto>();
            using (var db = new MerinoContext())
            {
                resultado = db.Database.SqlQuery<ReporteVentaDto>("getreporteventaporproductoclientevendedorV2  @p0, @p1", desde, hasta).ToList();
            }
            return resultado;
        }

        public IEnumerable<ReporteVentaDiariaDto> Diaria(string desde, string hasta, string tipo)
        {
            var resultado = new List<ReporteVentaDiariaDto>();
            using (var db = new MerinoContext())
            {
                string filtro = "101%";
                if (tipo == "2")
                    filtro = "2%";
                resultado = db.Database.SqlQuery<ReporteVentaDiariaDto>("getReporteVentaDiaria @p0, @p1, @p2", desde, hasta, filtro).ToList();
            }
            return resultado;
        }
    }
}
