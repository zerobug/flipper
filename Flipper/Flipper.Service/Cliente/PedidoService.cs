﻿using AutoMapper;
using Flipper.Data;
using Flipper.Model.Common;
using Flipper.Model.DTO.Cliente;
using Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Configuration;

namespace Flipper.Service.Cliente
{
    public class PedidoService: BaseService
    {
        public PedidoDto Grabar(PedidoDto valor, int usuario)
        {
            bool withTr = Context.Database.CurrentTransaction == null;
            DbContextTransaction tr = null;
            if (withTr)
                tr = Context.Database.BeginTransaction();
            try
            {
                //Mapper.Initialize(cfg =>{
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PedidoDto, Pedido>();
                    cfg.CreateMap<PedidoDetalleDto, PedidoDetalle>();
                    cfg.CreateMap<PedidoEventoDto, PedidoEvento>();
                    cfg.CreateMap<ClienteDto, Model.Integracion.Merino.Cliente>();
                });
                var mapper = config.CreateMapper();
                var pedido = mapper.Map<Pedido>(valor);
                if (pedido.Id == 0)
                {
                    if (valor.TransporteId == 0)
                    {
                        throw new InvalidOperationException("Debe cargar el flete");
                    }

                    var cliente = new ClienteService().Get(pedido.ClienteId);
                    if (string.IsNullOrWhiteSpace(cliente.Valor.VendedorCodigo))
                        throw new InvalidOperationException("El cliente no tiene vendedor asignado");

                    var vendedor = new VendedorService().Get(cliente.Valor.VendedorCodigo).Valor;
                    pedido.VendedorId = vendedor.Id;
                    pedido.Sucursal = new SeguridadService().GetByCodigo(vendedor.Codigo).Valor.PuntoVenta;
                    pedido.Numero = 1;
                    if (string.IsNullOrEmpty(pedido.Leyenda))
                        pedido.Leyenda = "";
                    var proximo = UOW.Repository<Pedido>().Queryable().Where(i => i.Sucursal == pedido.Sucursal).OrderByDescending(i => i.Numero).Take(1).ToList();
                    if (proximo != null && proximo.Count == 1)
                    {
                        pedido.Numero = proximo.Single().Numero + 1;
                    }
                    pedido.Fecha = DateTime.Today;
                    pedido.Detalle.ToList().ForEach(i =>
                    {
                        //if (i.Producto.PrecioBruto == 0)
                        //{
                        //    throw new InvalidOperationException($"El producto {i.Producto.Codigo} tiene el precio en 0");
                        //}

                        i.Pedido = pedido;
                        i.PrecioBruto = i.Producto.PrecioBruto;
                        i.Descuento = i.Producto.Descuento;
                        i.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added;
                    });
                    pedido.Eventos = new HashSet<PedidoEvento>();
                    pedido.Eventos.Add(new PedidoEvento() { Pedido = pedido, EstadoId = (int)Estado.Pedido.Cargado, Fecha = DateTime.Now, PedidoEventoTipoId = (int)PedidoEventoTipo.Valores.Carga, Observacion = valor.Observacion, ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added, UsuarioId = usuario });
                    pedido.Observaciones = new HashSet<PedidoObservacion>();
                    if (!string.IsNullOrEmpty(valor.Observacion))
                    {
                        pedido.Observaciones.Add(new PedidoObservacion() { TipoId = (int)PedidoObservacionTipo.Valores.Venta, Fecha = DateTime.Now, Observacion = valor.Observacion, ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added, UsuarioId = usuario });
                    }
                    if (!string.IsNullOrEmpty(valor.ObservacionCobranza))
                    {
                        pedido.Observaciones.Add(new PedidoObservacion() { TipoId = (int)PedidoObservacionTipo.Valores.Cobranza, Fecha = DateTime.Now, Observacion = valor.ObservacionCobranza, ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added, UsuarioId = usuario });
                    }


                    pedido.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added;
                    UOW.Repository<Pedido>().InsertGraphRange(new HashSet<Pedido>() { pedido });
                    UOW.SaveChanges();
                    pedido.EstadoId = pedido.Eventos.Single().Id;
                    pedido.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                }
                else
                {
                    var eventid = Queryable<Pedido>().AsNoTracking().Where(i => i.Id == pedido.Id).Select(i => i.EstadoId ?? 0).Single();
                    if (eventid > 0)
                        pedido.EstadoId = eventid;
                    foreach (var item in pedido.Eventos)
                    {
                        item.Usuario = null;
                        item.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                    }
                    foreach (var item in pedido.Detalle)
                    {
                        item.Pedido = null;
                        item.Producto = null;
                        item.PedidoId = pedido.Id;
                        if (item.Id == 0)
                        {
                            item.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added;
                        }
                        else
                        {
                            item.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                        }
                    }
                    pedido.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                    UOW.Repository<Pedido>().InsertOrUpdateGraph(pedido);
                }
                Boolean isEnabled;
                var autorizacionAutomatica = ConfigurationManager.AppSettings["autorizacionAutomatica"].ToString();
                Boolean.TryParse(autorizacionAutomatica, out isEnabled);

                if (!pedido.Detalle.Any(i=>i.BonificacionAdicional!=0) && isEnabled)
                {
                    var evento = new PedidoEvento() { Pedido = pedido, EstadoId = (int)Estado.Pedido.Autorizado, Fecha = DateTime.Now, PedidoEventoTipoId = (int)PedidoEventoTipo.Valores.Control, Observacion = valor.Observacion, UsuarioId = usuario, ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added };
                    pedido.Eventos.Add(evento);
                    Context.Database.ExecuteSqlCommand("ImportarPedido @p0", valor.Id);
                }
                UOW.SaveChanges();
                if (withTr)
                    tr.Commit();
                return valor;
            }
            catch (Exception)
            {
                if (withTr)
                    tr.Rollback();
                throw;
            }
        }

        public PedidoDto Rechazar(PedidoDto valor, int usuario)
        {
            var tr = Context.Database.BeginTransaction();
            try
            {
                Grabar(valor,0);
                var pedido = Query<Pedido>(i => i.Id == valor.Id).Include(i => i.Eventos).Select().First();
                var evento = new PedidoEvento() {
                    Pedido = pedido,
                    EstadoId = (int)Estado.Pedido.Rechazado,
                    Fecha = DateTime.Now,
                    PedidoEventoTipoId = (int)PedidoEventoTipo.Valores.Control,
                    Observacion = valor.Observacion,
                    UsuarioId = usuario,
                    ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added
                };
                pedido.Eventos.Add(evento);
                UOW.SaveChanges();
                pedido.EstadoId = evento.Id;
                pedido.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                UOW.SaveChanges();
                tr.Commit();
                return null;
            }
            catch (Exception)
            {
                tr.Rollback();
                throw;
            }
        }

        public PedidoDto Autorizar(PedidoDto valor, int usuario)
        {
            bool requiereAutorizacion = false;
            var tr = Context.Database.BeginTransaction();
            try
            {
                Grabar(valor,0);
                var pedido = Query<Pedido>(i => i.Id == valor.Id)
                    .Include(i => i.Detalle.Select(d=>d.Producto))
                    .Include(i => i.Eventos)
                    .Include(i => i.Estado)
                    .Include(i => i.Cliente)
                    .Select()
                    .First();

                if (pedido.Estado.EstadoId == (int)Estado.Pedido.Cargado)
                {
                    var productos = pedido.Detalle.Select(i => i.Producto.Codigo);

                    try
                    {
                        if (pedido.Cliente.ZonaCodigo.StartsWith("1"))
                        {
                            var topes = Query<BonificacionTope>(i => productos.Contains(i.ProductoCodigo)).Select().ToList().ToDictionary(i => i.ProductoCodigo);
                            if (pedido.Detalle.Any(i => topes.ContainsKey(i.Producto.Codigo.Trim()) && (i.Bonificacion + i.BonificacionAdicional) > topes[i.Producto.Codigo.Trim()].Valor))
                                requiereAutorizacion = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }

                int estado;
                if (requiereAutorizacion)
                {
                    estado = (int)Estado.Pedido.PendienteAdministracion;
                }
                else
                {
                    if (pedido.Estado.EstadoId == (int)Estado.Pedido.PendienteAdministracion)
                    {
                        estado = (int)Estado.Pedido.PendienteConfirmacion;
                    }else
                    {
                        var primera = Query<PedidoEvento>(i => i.PedidoId == pedido.Id && i.UsuarioId == usuario && i.EstadoId == (int)Estado.Pedido.PendienteConfirmacion).Select().Any();
                        if (!primera)
                            estado = (int)Estado.Pedido.Autorizado;
                        else
                            estado = (int)Estado.Pedido.PendienteConfirmacion;
                    }
                }
                var evento = new PedidoEvento() { Pedido = pedido, EstadoId = estado, Fecha = DateTime.Now, PedidoEventoTipoId = (int)PedidoEventoTipo.Valores.Control, Observacion = valor.Observacion, UsuarioId = usuario, ObjectState = Repository.Pattern.Infrastructure.ObjectState.Added };
                pedido.Eventos.Add(evento);
                UOW.SaveChanges();
                pedido.EstadoId = evento.Id;
                pedido.ObjectState = Repository.Pattern.Infrastructure.ObjectState.Modified;
                if (estado == (int)Estado.Pedido.Autorizado)
                    Context.Database.ExecuteSqlCommand("ImportarPedido @p0", valor.Id);
                UOW.SaveChanges();
                //tr.Rollback();
                tr.Commit();
                //var enviaMail = MailingPedidoAprobado(valor, usuario);
                return null;
            }
            catch (Exception)
            {
                tr.Rollback(); 
                throw;
            }
        }

        public bool MailingPedidoAprobado(PedidoDto valor, int usuario)
        {
            Boolean isEnabled;
            var mailingEnabled = ConfigurationManager.AppSettings["enabledMailing"].ToString();
            Boolean.TryParse(mailingEnabled, out isEnabled);

            if (!isEnabled)
                return false;

            var cliente = Query<Model.Integracion.Merino.Cliente>(i => i.Id == valor.ClienteId)
                    .Select()
                    .First();
            var vendedor = Query<Model.Integracion.Merino.Vendedor>(i => i.Id == valor.VendedorId)
                .Select()
                .First();
            var mailRecipient = ConfigurationManager.AppSettings["mailRecipient"].ToString();
            var mailMessage = new MimeMessage();

            mailMessage.From.Add(new MailboxAddress("Konig Mailing", "weworks.test@gmail.com"));

            mailMessage.To.Add(new MailboxAddress("Konig", mailRecipient));

            mailMessage.Subject = "Autorización Pedido";

            mailMessage.Body = new TextPart("plain")

            {

                Text = "Hola, Administrador: " + Environment.NewLine +
                "Se le notifica que se acaba de autorizar un pedido." + Environment.NewLine +
                "El pedido autorizado es:" + Environment.NewLine +
                "" + Environment.NewLine +
                "Id: " + valor.Id + Environment.NewLine +
                "Cliente: " + valor.ClienteId + " - " + cliente.Nombre + Environment.NewLine +
                "Vendedor: " + valor.VendedorId + " - " + vendedor.Nombre + Environment.NewLine +
                "Número: " + valor.Numero + Environment.NewLine +
                "Saludos"

            };

            using (var smtpClient = new SmtpClient())

            {

                smtpClient.Connect("smtp.gmail.com", 465, true);

                smtpClient.Authenticate("weworks.test@gmail.com", "WeWorks001");

                smtpClient.Send(mailMessage);

                smtpClient.Disconnect(true);

            }
            return true;
        }

        public GenericResult<PedidoDto> GetById(int id)
        {
            if (id == 0)
                return new GenericResult<PedidoDto>(){Valor = new PedidoDto()};
            using (UOW = NewUnitOfWork())
            {
                var config = new MapperConfiguration(cfg => { 
                    cfg.CreateMap<Pedido, PedidoDto>();
                    cfg.CreateMap<PedidoEvento, PedidoEventoDto>();
                    cfg.CreateMap<Model.Integracion.Merino.Cliente, ClienteDto>();
                    cfg.CreateMap<PedidoDetalle, PedidoDetalleDto>();
                });

                    var mapper = config.CreateMapper();

                var pedido = Query<Pedido>(i => i.Id == id)
                    .Include(i => i.Detalle.Select(d => d.Producto))
                    .Include(i => i.Eventos.Select(d => d.Usuario))
                    .Include(i => i.Vendedor)
                    .Include(i => i.Observaciones)
                    .Select().First();
                var clienteInfo = new ClienteService().Get(pedido.ClienteId).Valor;

                var resultado = mapper.Map<PedidoDto>(pedido);
                if (pedido.Observaciones != null && pedido.Observaciones.Any(i => i.TipoId == (int)PedidoObservacionTipo.Valores.Venta))
                    resultado.Observacion = pedido.Observaciones.First(i => i.TipoId == (int)PedidoObservacionTipo.Valores.Venta).Observacion;
                if (pedido.Observaciones != null && pedido.Observaciones.Any(i => i.TipoId == (int)PedidoObservacionTipo.Valores.Cobranza))
                    resultado.ObservacionCobranza = pedido.Observaciones.First(i => i.TipoId == (int)PedidoObservacionTipo.Valores.Cobranza).Observacion;

                resultado.CondicionVenta = new CondicionVenta { Id=resultado.CondicionVentaId };
                resultado.Cliente = clienteInfo;
                resultado.Sucursales = new ClienteService().GetSucursalList(pedido.ClienteId).Valor;
                return new GenericResult<PedidoDto>() { Valor =  resultado};
            }
        }

        public GenericResult<HashSet<Pedido>> Search(string valor, string vendedor)
        {
            using (UOW = NewUnitOfWork())
            {
                var query = Queryable<Pedido>().Include(i => i.Cliente).Include(i => i.Vendedor).Include(i=>i.Estado.Estado);
                if (!string.IsNullOrWhiteSpace(vendedor))
                {
                    query = query.Where(i => i.Vendedor.Codigo == vendedor);
                }

                return new GenericResult<HashSet<Pedido>>() { Valor = new HashSet<Pedido>(query.OrderByDescending(i=>i.Id).ToList()) };
            }
        }

        public GenericResult<HashSet<PedidoDto>> Search(PedidoSearchDto valor, int usuario)
        {
            using (UOW = NewUnitOfWork())
            {
                var perfil= new SeguridadService().GetById(usuario).Valor;
                var vendedor = perfil.Codigo;
                var padre = usuario.ToString();
                var jerarquia = Queryable<JerarquiaDetalle>().Where(i=>i.JerarquiaId==1 && i.Padre ==padre).Select(i => i.Hijo).ToList().Select(i=>int.Parse(i));

                var query = Queryable<Pedido>().Include(i => i.Cliente).Include(i => i.Vendedor).Include(i => i.Estado.Estado);
                if (!string.IsNullOrWhiteSpace(vendedor))
                {
                    query = query.Where(i => i.Vendedor.Codigo == vendedor);
                }
                else
                {
                    query = query.Where(i => jerarquia.Contains( i.VendedorId));

                }

                if (!string.IsNullOrWhiteSpace(valor.Vendedor))
                {
                    int vendedorId = 0;
                    if(int.TryParse(valor.Vendedor, out vendedorId))
                    {
                        query = query.Where(i => i.VendedorId == vendedorId);
                    }
                }

                if (!string.IsNullOrEmpty(valor.Estado))
                {
                    var estado = int.Parse(valor.Estado);
                    query = query.Where(i => i.Estado.EstadoId == estado);
                }
                

                int clientId;
                int.TryParse(valor.Cliente, out clientId);
                if (!string.IsNullOrEmpty(valor.Cliente))
                    query = query.Where(i => i.ClienteId == clientId);

                var desde = DateTime.ParseExact(valor.FechaDesde, "dd/MM/yyyy", null);
                var hasta = DateTime.ParseExact(valor.FechaHasta, "dd/MM/yyyy", null);
                if ((usuario == 36 || usuario == 33) && desde < new DateTime(2018, 1, 17))
                    desde = new DateTime(2018,1,18);

                query = query.Where(i => i.Fecha >= desde && i.Fecha <= hasta);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Pedido, PedidoDto>();
                    cfg.CreateMap<PedidoEvento, PedidoEventoDto>();
                    cfg.CreateMap<Model.Integracion.Merino.Cliente, ClienteDto>();
                    cfg.CreateMap<PedidoDetalle, PedidoDetalleDto>();
                });
                var mapper = config.CreateMapper();
                var lista = query.OrderByDescending(i => i.Id).ToList().Select(i => mapper.Map<PedidoDto>(i)).ToList();
                foreach (var item in lista)
                {
                    if (item.Estado.EstadoId == (int)Estado.Pedido.Cargado && item.Estado.UsuarioId==usuario)
                        item.PuedeEditar = true;

                    if (item.Estado.EstadoId==(int)Estado.Pedido.Cargado && perfil.Permisos.Select(i=>i.PermisoId).Contains(1))
                        item.PuedeAutorizar = true;
                    if (item.Estado.EstadoId == (int)Estado.Pedido.PendienteAdministracion && perfil.Permisos.Select(i => i.PermisoId).Contains(2))
                        item.PuedeAutorizar = true;
                    if (item.Estado.EstadoId == (int)Estado.Pedido.PendienteConfirmacion && perfil.Permisos.Select(i => i.PermisoId).Contains(2))
                        item.PuedeAutorizar = true;
                    item.PuedeVer = true;
                    string sucursal = item.Sucursal.ToString().PadLeft(4, '0');
                    var numero = item.Numero.ToString().PadLeft(8);
                    var remito = Queryable<RemitoConFactura>().Where(i => i.PedidoPunto == sucursal && i.PedidoNumero == numero).FirstOrDefault();
                    if (remito != null)
                    {
                        item.Remito = $"{remito.Punto}-{remito.Numero.Trim()}";
                        item.Factura = $"{remito.FacturaPunto}-{remito.FacturaNumero.Trim()}";
                    }
                }

                return new GenericResult<HashSet<PedidoDto>>() { Valor = new HashSet<PedidoDto>(lista) };
            }
        }
    }
}
