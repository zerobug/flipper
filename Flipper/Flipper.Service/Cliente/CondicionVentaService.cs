﻿using M = Flipper.Model.Integracion.Merino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flipper.Service.Result;
using Flipper.Data;

namespace Flipper.Service.Cliente
{
    public class CondicionVentaService : BaseService
    {
        public GenericResult<HashSet<M.CondicionVenta>> GetAll()
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                return new GenericResult<HashSet<M.CondicionVenta>>() { Valor = new HashSet<M.CondicionVenta>(UOW.Repository<M.CondicionVenta>().Query().Select().ToList()) };
            }
        }

    }
}
