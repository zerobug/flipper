﻿using Flipper.Data;
using M = Flipper.Model.Integracion.Merino;
using Flipper.Service.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flipper.Model.DTO.Cliente;
using AutoMapper;

namespace Flipper.Service
{
    public class ClienteService: BaseService
    {
        public GenericResult<HashSet<M.Cliente>> GetAll()
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                return new GenericResult<HashSet<M.Cliente>>() { Valor = new HashSet<M.Cliente>(UOW.Repository<M.Cliente>().Query().Select().ToList()) };
            }
        }

        public GenericResult<ClienteDto> Get(int id)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                Mapper.Initialize(cfg => cfg.CreateMap<M.Cliente, ClienteDto>());
                var cliente = UOW.Repository<M.Cliente>().Query(i => i.Id == id).Select().Single();
                var dto = Mapper.Map<ClienteDto>(cliente);
                dto.VentaPorPack = UOW.Repository<M.ClienteGrupo>().Query(i => i.ClienteCodigo == cliente.Codigo && i.GrupoCodigo=="98    ").Select().Any();
                //dto.VentaPorPack = !(cliente.Codigo.StartsWith( "V")|| cliente.Codigo.StartsWith("P"));
                if (DateTime.Now.Hour > 12)
                    dto.Entrega = 72;
                else
                    dto.Entrega = 48;

                var flete = new FleteService().Search(cliente.FleteCodigo);
                if (flete.Exito)
                    dto.Flete = flete.Valor.First();
                var sucursales = GetSucursalList(cliente.Id);
                if (sucursales.Exito)
                    dto.Sucursales = sucursales.Valor;

                return new GenericResult<ClienteDto>() { Valor = dto };
            }
        }

        public GenericResult<IEnumerable< M.ClienteSucursal>> GetSucursalList(int id)
        {
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var cliente = UOW.Repository<M.Cliente>().Query(i => i.Id == id).Select().Single();
                var sucursales = UOW.Repository<M.ClienteSucursal>().Query(i => i.ClienteCodigo == cliente.Codigo).Select().ToList();
                sucursales.ForEach(i => {
                    i.Nombre = i.Nombre.Trim();
                    if(string.IsNullOrEmpty(i.FleteCodigo))
                    {
                        var flete = new FleteService().Search(i.FleteCodigo);
                        if (flete.Exito)
                            i.Flete = flete.Valor.First();
                    }
                });
                return new GenericResult<IEnumerable< M.ClienteSucursal>>() { Valor = sucursales };
            }
        }

        public GenericResult<HashSet<M.Cliente>> Search(string valor, string vendedor)
        {
            vendedor = vendedor.PadLeft(6, ' ');
            using (UOW = new Repository.Pattern.Ef6.UnitOfWork(new DbContext()))
            {
                var query = UOW.Repository<M.Cliente>().Queryable();

                if (!string.IsNullOrWhiteSpace(vendedor))
                {
                    query = query.Where(i => i.VendedorCodigo == vendedor);
                }
                query = query.Where(i => (i.Nombre.Contains(valor) || i.Codigo == valor));

                return new GenericResult<HashSet<M.Cliente>>() { Valor = new HashSet<M.Cliente>(query.ToList()) };
            }
        }
    }
}
