(function (angular) {
    'use strict';
    function detalleController(appConfig,sharedService,$http,comisionService) {
        var ctrl = this;
        kendo.culture("es-AR");
        ctrl.controls={};
        ctrl.controls.comisionOptions={
            format:'#.##',
            culture: 'en-US',
            decimals:2
        }
        this.$routerOnActivate = function(next, previous) {
            if(next.params.id=='null' || next.params.id=='0'){
                ctrl.item={};
                ctrl.item.Desde=moment().startOf('month').format('DD/MM/YYYY');
                ctrl.item.Hasta=moment().endOf('month').format('DD/MM/YYYY');
                console.log(ctrl.item);
            }else{
                cargarLiquidacion(next.params.id);
            }
        }
        ctrl.actualizar=function () {
            comisionService.actualizar(ctrl.item)
            .then(function (response) {
                ctrl.totales = response.data.Totales;
                ctrl.vendedores = response.data.Vendedores;
                ctrl.exclusivos = response.data.Exclusivos;
                ctrl.supermercados = response.data.Supermercados;
                ctrl.gerentes = response.data.Gerentes;
            },function (error) {
                sharedService.showError(error);
            });
        }
        ctrl.grabar=function () {
            comisionService.grabar(ctrl.item)
            .then(function (response) {
                ctrl.item =response.data; 
            },function (error) {
                sharedService.showError(error);
            });
        }
        function cargarLiquidacion(id){
            comisionService.getById(id)
            .then(function (response) {
                ctrl.item =response.data; 
            },function (error) {
                sharedService.showError(error);
            });
        }
    }
    angular.module('app').component('rrhhComisionDetalle', {
        templateUrl: '/App/RRHH/Comision/detalle.tpl.html',
        controller: detalleController,
        bindings: { $router: '<' },
    });
})(window.angular);