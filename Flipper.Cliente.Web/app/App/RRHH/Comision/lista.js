(function (angular) {
    'use strict';
    function listaController(appConfig,sharedService,$http,comisionService) {
        var ctrl = this;

        this.$routerOnActivate = function(next, previous) {
            comisionService.search('')
            .then(function (response) {
                ctrl.pedidos = response.data;
            },function (error) {
                sharedService.showError(error);
            });
        }

        ctrl.ver = function (id) {
            this.$router.navigate(['RRHHComisionDetalle', { id: id }]);
        }

        ctrl.nuevo = function () {
            this.$router.navigate(['RRHHComisionDetalle', { id: 0 }]);
        }
    }
    angular.module('app').component('rrhhComisionLista', {
        templateUrl: '/App/RRHH/Comision/lista.tpl.html',
        controller: listaController,
        bindings: { $router: '<' },
    });
})(window.angular);