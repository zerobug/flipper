(function (angular) {
    'use strict';
    function consultaController(appConfig,sharedService,$http,pedidoService,productoService,clienteService) {
        var ctrl = this;
        ctrl.title = 'Cuenta corriente de clientes';
        ctrl.clienteOpt = {
				placeholder: "Elija el cliente...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: {
					type: "json",
					serverFiltering: true,
					transport: {
						read: {
                            url:appConfig.apiUrl + "Api/Cliente/ComboSource",
                            beforeSend: function(req) {
                                req.setRequestHeader('Authorization', sharedService.getToken());
                            },                            
                            data: function() {
                                return {
                                query: ctrl.cliente.text(),
                                id: ctrl.clienteId
                                };
                            }
                        }
					},
				},
			};
        ctrl.estadoOpt = {
				placeholder: "Elija el estado...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: [{Value:1, Text:'Pendiente'},{Value:2, Text:'Todos'}]
			};
    }
    angular.module('app').component('clienteCuentaCorrienteConsulta', {
        templateUrl: '/App/Cliente/CuentaCorriente/consulta.tpl.html',
        controller: consultaController,
        bindings: { $router: '<' },
    });
})(window.angular);