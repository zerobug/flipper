(function (angular) {
    'use strict';
    function ventaDiariaController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().subtract(1, 'months').startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.model = {ventas:[]};
        console.log('diaria');
        //ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        //ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        //ctrl.reportPath= 'VentaDiaria.rdl';
        ctrl.consultar = function () {
            $http.get(appConfig.apiUrl + "Api/ReporteVentaDiaria/Get/?desde="+moment(ctrl.desde).format('YYYYMMDD')+"&hasta="+moment(ctrl.hasta).format('YYYYMMDD')+"&tipo=1").then(function (response) {
                ctrl.model.ventas=response.data;
                var data = [];
                ctrl.data2 = [{
                    key: "Venta",
                    values: []
                    }]
                ctrl.model.ventas.forEach(function(el) {
                    ctrl.data2[0].values.push({ x: el.Dia , y : el.Importe });
                });
                console.log(ctrl.data2);
                console.log(data);

            }, function (error) {
                sharedService.showError(error.data.ExceptionMessage);
            });
            // var proxy = $('#container').data('ejReportViewer');
            // proxy._refresh = true;
            // $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            // _params = [];
            // _params.push({ Name: 'tipo', Values: ['porProducto'] });
            // _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            // _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            // proxy._refreshReport();
        }

        // ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
        //     var proxy = $('#container').data('ejReportViewer');
        //     var inVokemethod = onSuccess;

        //     $.ajax({
        //         type: type,
        //         url: url,
        //         crossDomain: true,
        //         contentType: 'application/json; charset=utf-8',
        //         dataType: 'json',
        //         data: jsondata,
        //         beforeSend: function (req) {
        //             if (inVokemethod == "_getDataSourceCredential") {
        //                 var _json = jQuery.parseJSON(this.data);
        //                 if (_params != null) {
        //                     _json["params"] = _params;
        //                 }
        //                 this.data = JSON.stringify(_json);
        //             }

        //             if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
        //                 if (!proxy._isToolbarClick) {
        //                     proxy._showloadingIndicator(true);
        //                     proxy._updateDatasource = true;
        //                 } else {
        //                     proxy._showNavigationIndicator(true);
        //                 }
        //             }
        //             req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
        //             req.setRequestHeader('Authorization', sharedService.getToken());
        //         },
        //         success: function (data) {
        //             if (data && typeof (data.Data) != "undefined") {
        //                 data = data.Data;
        //             }
        //             if (typeof (data) == "string") {
        //                 if (data.indexOf("Sf_Exception") != -1) {
        //                     proxy._renderExcpetion(inVokemethod + ":" + data);
        //                     return;
        //                 }
        //             }
        //             proxy[inVokemethod](data);
        //         },
        //     });
        //}
        ctrl.options2 = {
            chart: {
                type: 'discreteBarChart',
                height: 350,
                margin : {
                    top: 10,
                    right: 20,
                    bottom: 40,
                    left: 70
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                useInteractiveGuideline: true,
                dispatch: {
                    stateChange: function(e){ console.log("stateChange"); },
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Dias'
                },
                yAxis: {
                    axisLabel: 'Ventas',
                    tickFormat: function(d){
                        return d3.format('$08.02g')(d);
                    },
                    axisLabelDistance: -10
                },
            },
            title: {
                enable: true,
                text: 'Venta por dia'
            },
        };

        ctrl.options = {
            chart: {
                type: 'lineChart',
                height: 350,
                margin : {
                    top: 10,
                    right: 20,
                    bottom: 40,
                    left: 55
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                useInteractiveGuideline: true,
                dispatch: {
                    stateChange: function(e){ console.log("stateChange"); },
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Dias'
                },
                yAxis: {
                    axisLabel: 'Ventas',
                    tickFormat: function(d){
                        return d3.format('$.02g')(d);
                    },
                    axisLabelDistance: -10
                },
            },
            title: {
                enable: true,
                text: 'Venta Acumulada vs objetivo'
            },
        };
                
        ctrl.data2 = [];

        ctrl.data = [{
            key: "Venta",
            values: [
                { x: "1" , y : 0 },
                { x : "2" , y : 0 },
                { x : "3" , y : 12 },
                { x : "4" , y : 19 },
                { x: "5" , y : 30 },
                { x : "6" ,y : 33 },
                { x : "7" ,y : 35 },
                { x : "8" , y: 50 }
                ]
            },{
            key: "Proyeccion",
            values: [
                { x: "1" , y : 0 },
                { x : "2" , y : 10 },
                { x : "3" , y : 20 },
                { x : "4" , y : 30 },
                { x: "5" , y : 40 },
                { x : "6" ,y : 50 },
                { x : "7" ,y : 60 },
                { x : "8" , y: 70 }
                ]
            }]
        }
    angular.module('app').component('clienteReporteVentaDiaria', {
        templateUrl: '/App/Cliente/Reporte/Venta/diaria.tpl.html',
        controller: ventaDiariaController,
        bindings: { $router: '<' },
    });
})(window.angular);