(function (angular) {
    'use strict';
    function ventaPorVendedorClienteProductoController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        ctrl.reportPath= 'Ventas.rdl';
        ctrl.consultar = function () {
            var proxy = $('#container').data('ejReportViewer');
            proxy._refresh = true;
            $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            _params = [];
            _params.push({ Name: 'tipo', Values: ['porVendedorClienteProducto'] });
            _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            proxy._refreshReport();
        }

            ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
                var proxy = $('#container').data('ejReportViewer');
                var inVokemethod = onSuccess;

                $.ajax({
                    type: type,
                    url: url,
                    crossDomain: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: jsondata,
                    beforeSend: function (req) {
                        if (inVokemethod == "_getDataSourceCredential") {
                            var _json = jQuery.parseJSON(this.data);
                            if (_params != null) {
                                _json["params"] = _params;
                            }
                            this.data = JSON.stringify(_json);
                        }

                        if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
                            if (!proxy._isToolbarClick) {
                                proxy._showloadingIndicator(true);
                                proxy._updateDatasource = true;
                            } else {
                                proxy._showNavigationIndicator(true);
                            }
                        }
                        req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
                        req.setRequestHeader('Authorization', sharedService.getToken());
                    },
                    success: function (data) {
                        if (data && typeof (data.Data) != "undefined") {
                            data = data.Data;
                        }
                        if (typeof (data) == "string") {
                            if (data.indexOf("Sf_Exception") != -1) {
                                proxy._renderExcpetion(inVokemethod + ":" + data);
                                return;
                            }
                        }
                        proxy[inVokemethod](data);
                    },
                });
            }
    }
    angular.module('app').component('clienteReporteVentaPorVendedorClienteProducto', {
        templateUrl: '/App/Cliente/Reporte/Venta/porVendedorClienteProducto.tpl.html',
        controller: ventaPorVendedorClienteProductoController,
        bindings: { $router: '<' },
    });
})(window.angular);