(function (angular) {
  'use strict';
  function detalleController(appConfig,sharedService,objetivoVentaService) {
    var ctrl = this;
    kendo.culture("es-AR");
    ctrl.title = '';
    ctrl.modelo={};

    ctrl.vendedorOpt = {
        placeholder: "Elija el vendedor...",
        dataTextField: "Text",
        dataValueField: "Value",
        delay: 500,
        filter: "contains",
        autoBind: false,
        valuePrimitive: true,
        dataSource: {
          type: "json",
          serverFiltering: true,
          transport: {
            read: {
              url:appConfig.apiUrl + "Api/Vendedor/ComboSource",
              beforeSend: function(req) {
                req.setRequestHeader('Authorization', sharedService.getToken());
              },                            
              data: function() {
                return {
                    query: ctrl.vendedor.text(),
                    id: ctrl.vendedorId
                };
              }
            }
          },
        },
      };
  
    this.$routerOnActivate = function(next, previous) {
        if(next.params.id=='null' || next.params.id===0){
            ctrl.title ='Nuevo objetivo'; 
        }else{
            ctrl.title ='ver objetivo';
            if(next.params.operacion=='editar'){
                ctrl.title ='editar objetivo';
                ctrl.editable = true;
            }
            cargar(next.params.id);
        }
    };

    ctrl.cancelar = function () {
        ctrl.$router.navigate(['ObjetivoVentaLista', {  }]);
    };

    function cargar(id) {
        objetivoVentaService.getById(id).then(function name(response) {
            ctrl.modelo = response.data.Valor;
        }, function (error) {
            sharedService.showError(error);
        });
    };

    ctrl.grabar = function () {
      if(!ctrl.modelo.Periodo||!moment(ctrl.modelo.Periodo+'01',"YYYYMMDD").isValid())
      {
        sharedService.showError('Debe ingresar un periodo valido');
        return;
      }
      objetivoVentaService.grabar(ctrl.modelo).then(function name(response) {
        ctrl.$router.navigate(['ObjetivoVentaLista', {  }]);
      }, function (error) {
        console.log(error);
        sharedService.showError(error.data.ExceptionMessage);
      });
    };

    function isCargando(){
        if(ctrl.llamadasPendientes>0)
            return true;
        return false;
    }
  }
  angular.module('app').component('objetivoVentaDetalle', {
    templateUrl: '/App/Cliente/objetivoVenta/detalle.tpl.html',
    controller: detalleController,
    bindings: { $router: '<' },
  });
})(window.angular);