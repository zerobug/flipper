(function (angular) {
  'use strict';
  function reporteController(appConfig,sharedService,objetivoVentaService) {
    var ctrl = this;
    kendo.culture("es-AR");
    ctrl.title = '';
    ctrl.controls={};
    ctrl.editable = true;
    ctrl.llamadasPendientes=0;
    ctrl.vendedorOpt = {
      placeholder: "Elija el vendedor...",
      dataTextField: "Text",
      dataValueField: "Value",
      delay: 500,
      filter: "contains",
      autoBind: false,
      valuePrimitive: true,
      dataSource: {
        type: "json",
        serverFiltering: true,
        transport: {
          read: {
            url:appConfig.apiUrl + "Api/Vendedor/ComboSource",
            beforeSend: function(req) {
                req.setRequestHeader('Authorization', sharedService.getToken());
            },                            
            data: function() {
              return {
                query: ctrl.vendedor.text(),
                id: ctrl.vendedorId
              };
            }
          }
        },
      },
    };

    this.$routerOnActivate = function(next, previous) {
    };

    ctrl.cancelar = function () {
      ctrl.$router.navigate(['Home', {  }]);
    };

    ctrl.buscar = function(){
      refrescar();
    }

    function refrescar(){
      objetivoVentaService.reporte({vendedorId:ctrl.vendedorId, periodo:ctrl.periodo })
      .then(function (response) {
        ctrl.objetivos = response.data;
      }, function (error) {
        sharedService.showError(error|| 'Ha ocurrido un error contactese con sistemas');
      });
    }
  }
  angular.module('app').component('objetivoVentaReporte', {
      templateUrl: '/App/Cliente/objetivoVenta/reporte.tpl.html',
      controller: reporteController,
      bindings: { $router: '<' },
  });
})(window.angular);