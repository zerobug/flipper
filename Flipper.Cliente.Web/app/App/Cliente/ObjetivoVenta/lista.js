(function (angular) {
  'use strict';
  function listaController(appConfig,objetivoVentaService,sharedService) {
    var ctrl = this;
    this.$routerOnActivate = function() {

    }

    function refrescar(){
      objetivoVentaService.search({vendedorId:ctrl.vendedorId, periodo:ctrl.periodo })
      .then(function (response) {
        ctrl.objetivos = response.data;
        var i =0;
        for (i = 0; i < ctrl.objetivos.length; i++) {
          ctrl.objetivos[i].PuedeEditar=true;
          ctrl.objetivos[i].PuedeBorrar=true;
        }
      }, function (error) {
        sharedService.showError(error|| 'Ha ocurrido un error contactese con sistemas');
      });
    }

    ctrl.vendedorOpt = {
      placeholder: "Elija el vendedor...",
      dataTextField: "Text",
      dataValueField: "Value",
      delay: 500,
      filter: "contains",
      autoBind: false,
      valuePrimitive: true,
      dataSource: {
        type: "json",
        serverFiltering: true,
        transport: {
          read: {
            url:appConfig.apiUrl + "Api/Vendedor/ComboSource",
            beforeSend: function(req) {
                req.setRequestHeader('Authorization', sharedService.getToken());
            },                            
            data: function() {
              return {
                query: ctrl.vendedor.text(),
                id: ctrl.vendedorId
              };
            }
          }
        },
      },
    };

    ctrl.editar=function (id) {
      this.$router.navigate(['ObjetivoVentaEditar', { id: id, operacion:'editar' }]);
    }
    
    ctrl.buscar = function(){
      refrescar();
    }
  }
  angular.module('app').component('objetivoVentaLista', {
      templateUrl: '/App/Cliente/objetivoVenta/lista.tpl.html',
      controller: listaController,
      bindings: { $router: '<' },
  });
})(window.angular);