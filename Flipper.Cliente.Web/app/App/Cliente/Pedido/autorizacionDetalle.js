(function (angular) {
    'use strict';
    function autorizacionDetalleController($http, appConfig) {
        var ctrl = this;
        this.$routerOnActivate = function(next) {
            $http.get(appConfig.apiUrl+ 'api/pedido/'+next.params.id).then(function (response) {
            ctrl.pedido = response.data; 
            });
        };
    }
    angular.module('app').component('clientePedidoAutorizacionDetalle', {
        templateUrl: '/App/Cliente/Pedido/autorizacionDetalle.tpl.html',
        controller: autorizacionDetalleController,
        bindings: {
        }
    });
})(window.angular);