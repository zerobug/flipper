﻿(function (angular) {
    'use strict';
    function listaController(appConfig,pedidoService,sharedService,$window) {
        var ctrl = this;
        ctrl.puedeAutorizar =false;
        ctrl.fechaDesde =moment().startOf('month').format('DD/MM/YYYY');
        ctrl.fechaHasta =moment().endOf('month').format('DD/MM/YYYY');
        ctrl.filtro={};
        //ctrl.fechaDesde ='';
        ctrl.vendedorOpt = {
            placeholder: "Elija el vendedor...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
              type: "json",
              serverFiltering: true,
              transport: {
                read: {
                  url:appConfig.apiUrl + "Api/Vendedor/ComboSource",
                  beforeSend: function(req) {
                      req.setRequestHeader('Authorization', sharedService.getToken());
                  },                            
                  data: function() {
                    return {
                      query: ctrl.vendedor.text(),
                      id: ctrl.filtro.vendedorId
                    };
                  }
                }
              },
            },
            };

        this.$routerOnActivate = function() {
            ctrl.puedeAutorizar = sharedService.getPermisos().Autorizar;
            if(sharedService.filtroPedido){
                //ctrl.cliente.dataSource.read();
                //ctrl.vendedor.dataSource.read();
                console.log(sharedService.filtroPedido);
                ctrl.fechaDesde =sharedService.filtroPedido.fechaDesde;
                ctrl.fechaHasta =sharedService.filtroPedido.fechaHasta;
                ctrl.clienteId=sharedService.filtroPedido.clienteId;
                ctrl.vendedorId=sharedService.filtroPedido.vendedor;
                ctrl.vendedor.value(sharedService.filtroPedido.vendedor);
                ctrl.cliente.value(sharedService.filtroPedido.clienteId);
            }else{
                ctrl.fechaDesde =moment().toDate();//.format('DD/MM/YYYY');
                ctrl.fechaHasta =moment().toDate();//.format('DD/MM/YYYY');
            }
            //refrescar();
        }
        ctrl.autorizar=function (id) {
            this.$router.navigate(['ClientePedidoNuevo', { id: id, operacion:'autorizar' }]);
        }
        ctrl.ver=function (id) {
            this.$router.navigate(['ClientePedidoNuevo', { id: id, operacion:'ver' }]);
        }
        ctrl.editar=function (id) {
            this.$router.navigate(['ClientePedidoNuevo', { id: id, operacion:'editar' }]);
        }
        function refrescar(){
            //$window.sessionStorage.filtroPedido = JSON.stringify({fechaDesde:ctrl.fechaDesde, fechaHasta: ctrl.fechaHasta});
            sharedService.filtroPedido = {cliente:ctrl.clienteId, vendedor: ctrl.vendedorId, fechaDesde:ctrl.fechaDesde, fechaHasta: ctrl.fechaHasta};
            var desde=moment(ctrl.fechaDesde).format('DD/MM/YYYY');
            var hasta=moment(ctrl.fechaHasta).format('DD/MM/YYYY');
            pedidoService.search({cliente:ctrl.clienteId, vendedor: ctrl.vendedorId, estado:ctrl.estado, fechaDesde:desde, fechaHasta: hasta })
            .then(function (response) {
                ctrl.pedidos = response.data.Valor;
            }, function (error) {
                    sharedService.showError(error|| 'Ha ocurrido un error contactese con sistemas');
            });
        }
        ctrl.buscar = function(){
            refrescar();
        }
      
        ctrl.clienteOpt = {
				placeholder: "Elija el cliente...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: {
					type: "json",
					serverFiltering: true,
					transport: {
						read: {
                            url:appConfig.apiUrl + "Api/Cliente/ComboSource",
                            beforeSend: function(req) {
                                req.setRequestHeader('Authorization', sharedService.getToken());
                            },                            
                            data: function() {
                                return {
                                query: ctrl.cliente.text(),
                                id: ctrl.clienteId
                                };
                            }
                        }
					},
				},
			};
        ctrl.estadoOpt = {
				placeholder: "Elija el estado...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: [{Value:1, Text:'Pendiente'},{Value:2, Text:'Rechazado'},{Value:3, Text:'Autorizado'},{Value:4, Text:'Pendiente Administracion'},{Value:4, Text:'Pendiente Confirmacion'}]
            };
            //
    }
    angular.module('app').component('clientePedidoLista', {
        templateUrl: '/App/Cliente/Pedido/lista.tpl.html',
        controller: listaController,
        bindings: { $router: '<' },
    });
})(window.angular);