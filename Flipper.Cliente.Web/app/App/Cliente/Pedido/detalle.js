﻿(function (angular) {
    'use strict';
    function detalleController(appConfig,sharedService,$http,pedidoService,productoService,clienteService,condicionVentaService) {
        var ctrl = this;
        kendo.culture("es-AR");
        ctrl.title = '';
        ctrl.controls={};
        ctrl.editable = true;
        ctrl.llamadasPendientes=0;
        ctrl.puedeAutorizar =false;

        ctrl.input = {
            fleteId:0,
            clienteId:0,
            cliente:function(valor){
                if(arguments.length){
                    if(isCargando()){
                        ctrl.llamadasPendientes--;
                    }else{
                        if(valor!=null){
                            clienteService.getById(valor)
                            .then(function (response) {
                                ctrl.clienteData = response.data.Valor;
                                ctrl.input.fecha = moment().add(ctrl.clienteData.Entrega,'H').toDate();
                                ctrl.pedido.ClienteId =valor;
                                ctrl.entregaOpt.dataSource = {data:ctrl.clienteData.Sucursales };
                                ctrl.input.fleteId= ctrl.clienteData.Flete.Id;
                                ctrl.controls.flete.dataSource.read();
                                ctrl.input.Transporte = ctrl.clienteData.Flete.Id;
                                ctrl.input.ListaCodigo = ctrl.clienteData.ListaCodigo;
                                ctrl.input.CategoriaIVA = ctrl.clienteData.CategoriaIVA;
                                ctrl.input.CondicionVenta = ctrl.clienteData.CondicionVenta;
                                ctrl.pedido.TransporteId = ctrl.clienteData.Flete.Id;
                            }, function (error) {
                                sharedService.showError(error);
                            });
                        }
                    }
                    return ctrl.input.clienteId = valor;
                }else{
                    return ctrl.input.clienteId;
                }
            },
            productoId:0,
            producto:function (valor) {
                if(arguments.length){
                    if(valor!=null&&valor!=0){
                        productoService.getByIDAndClient(valor, ctrl.clienteData.Id)
                        .then(function (response) {
                            if(response.data.Exito){
                                if(!validarTrazable(response.data.Valor)){
                                    sharedService.showError('No se pueden mezclar productos trazables y no trazables');
                                    return;
                                }
                                ctrl.nuevoDetalle={};
                                ctrl.nuevoDetalle.Producto = response.data.Valor;
                                ctrl.nuevoDetalle.ProductoId=ctrl.nuevoDetalle.Producto.Id;
                                ctrl.nuevoDetalle.Bonificacion = ctrl.nuevoDetalle.Producto.Bonificacion;
                                ctrl.nuevoDetalle.BonificacionAdicional = 0;
                                ctrl.nuevoDetalle.Cantidad=0;
                                ctrl.nuevoDetalle.Precio=ctrl.nuevoDetalle.Producto.Precio;
                            }else{
                                sharedService.showError(response.data.ResumenError);
                            }
                        }, function (error) {
                            sharedService.showError(error);
                        });
                    }
                    return ctrl.input.productoId = valor;
                }else{
                    return ctrl.input.productoId;
                }
            },
            fecha: moment().toDate()
        };

        this.$routerOnActivate = function(next, previous) {
            ctrl.nuevo();
            var permisos = sharedService.getPermisos();
            condicionVentaService.search().then(function name(response) {
                ctrl.condicionVentaLista = response.data;
            }, function (error) {
                sharedService.showError(error);
            });

            if(next.params.id=='null' || next.params.id===0){
                ctrl.title ='Nuevo pedido'; 
            }else{
                ctrl.title ='ver pedido';
                ctrl.puedeAutorizar = false;
                ctrl.editable = false;
                if(next.params.operacion=='autorizar'){
                    ctrl.title ='Autorizar pedido';
                    if(!angular.isUndefined(permisos)){
                        ctrl.puedeAutorizar = permisos.Autorizar;
                        ctrl.editable = permisos.Autorizar;
                    }
                }
                if(next.params.operacion=='editar'){
                    ctrl.title ='editar pedido';
                    ctrl.editable = true;
                }
                cargarPedido(next.params.id);
            }
        };

        ctrl.Pedido = function (valor){
            if(arguments.length){
                if(valor<0){
                    return false;
                }
                ctrl.nuevoDetalle.Cantidad = valor;
                calcularBonificacion();
                return true;
            }else{
                if(angular.isUndefined(ctrl.nuevoDetalle)||ctrl.nuevoDetalle===null)
                    return 0;
                return ctrl.nuevoDetalle.Cantidad;
            }
        };

        ctrl.Bonificacion = function (valor) {
            if(arguments.length){
                if(valor<0){
                    return false;
                }
                ctrl.nuevoDetalle.BonificacionAdicional = valor;
                calcularBonificacion();
                return true;
            }else{
                if(angular.isUndefined(ctrl.nuevoDetalle)||ctrl.nuevoDetalle===null)
                    return 0;
                return ctrl.nuevoDetalle.BonificacionAdicional;
            }
        };

        ctrl.nuevo = function () {
            ctrl.pedido = {
                Detalle:[],
            };
            ctrl.nuevoDetalle={};
            ctrl.input.productoId=0;
            ctrl.input.productoCB=null;
            ctrl.input.fleteId=0;
            ctrl.input.clienteId=0;
            ctrl.input.clienteCB=null;
            ctrl.input.Transporte=null;
            ctrl.input.fecha=moment().toDate();
            ctrl.cliente.focus();
            ctrl.total=0;
        };

        function getFechaEntrega() {
            if(ctrl.input.fecha instanceof Date && !isNaN(ctrl.input.fecha.valueOf())){
                return moment(ctrl.input.fecha).format("YYYY-MM-DD");
            }else{
                return moment(ctrl.input.fecha,'DD/MM/YYYY').format("YYYY-MM-DD");
            }
        };

        function validarTrazable(producto){
            for(var i=0;i<ctrl.pedido.Detalle.length;i++){
                if(producto.RemiteSeparado != ctrl.pedido.Detalle[i].Producto.RemiteSeparado)
                    return false;
            }
            return true;
        };

        function validarTieneItems() {
            if(ctrl.pedido.Detalle.length>0)
                return true;
            sharedService.showError('El pedido debe tener al menos 1 producto');
            return false;
        };

        function validarMoneda(item) {            
            if(!ctrl.pedido.Detalle.length>0){
                return true;
            }else{
                if(ctrl.pedido.Detalle[0].Producto.Moneda == item.Producto.Moneda)
                    return true;
            }
            sharedService.showError('Todos los productos deben tener precio en la misma moneda.');
            return false;
        }

        ctrl.enviar = function () {
            ctrl.pedido.FechaEntrega=getFechaEntrega();
            if(!validarTieneItems())
                return;
            ctrl.pedido.CondicionVentaId=ctrl.input.CondicionVenta;
            console.log('grabar: ',ctrl.clienteData.Sucursales);


            if(!ctrl.pedido.LugarEntrega && ctrl.clienteData.Sucursales.length>0){
                sharedService.showError('Debe completar la sucursal.');
                return;
            }
            if(!ctrl.pedido.TransporteId){
                sharedService.showError('Debe completar el flete.');
                return;
            }
            pedidoService.grabar(ctrl.pedido).then(function (response) {
                sharedService.showSuccess('Pedido enviado');
                ctrl.nuevo();
            }, function (error) {
                sharedService.showError(error.data.ExceptionMessage);
            });
        };

        ctrl.cancelar = function () {
            ctrl.$router.navigate(['Home', {  }]);
        };

        ctrl.bonificar = function () {
            ctrl.nuevoDetalle.Total=0;
            ctrl.nuevoDetalle.Precio=0;
            ctrl.nuevoDetalle.Bonificacion=0;
            ctrl.nuevoDetalle.BonificacionCantidad=0;
            ctrl.nuevoDetalle.Producto.PrecioBruto=0;
            
            ctrl.agregar();
        };

        ctrl.agregar = function () {
            if(validarMoneda(ctrl.nuevoDetalle)){
                ctrl.total = ctrl.total + ctrl.nuevoDetalle.Total; 
                ctrl.pedido.Detalle.push(ctrl.nuevoDetalle);
                ctrl.nuevoDetalle={};
                ctrl.productoCB.search('');
                // ctrl.input.producto=null;
                ctrl.input.productoCB=null;
                // console.log(ctrl.productoCB.items())
            }            
        }

        ctrl.eliminar = function (item) {
            for(var i = ctrl.pedido.Detalle.length; i--;) {
                if(ctrl.pedido.Detalle[i].ProductoId == item.ProductoId) {
                    ctrl.total =ctrl.total - item.Total; 
                    ctrl.pedido.Detalle.splice(i, 1);
                }
            }
        }

        ctrl.autorizar = function () {
            ctrl.pedido.FechaEntrega=getFechaEntrega();
            if(!validarTieneItems())
                return;
            ctrl.pedido.CondicionVentaId=ctrl.input.CondicionVenta;
            pedidoService.autorizar(ctrl.pedido).then(function (response) {
                    sharedService.showSuccess('Pedido enviado');
                    ctrl.$router.navigate(['ClientePedidoConsulta', {  }]);
                }, function (error) {
                    sharedService.showError(error);
                });
        };

        ctrl.rechazar = function () {
            ctrl.pedido.FechaEntrega=getFechaEntrega();
            if(!validarTieneItems())
                return;
            pedidoService.rechazar(ctrl.pedido).then(function (response) {
                    sharedService.showSuccess('Pedido enviado');
                    ctrl.$router.navigate(['ClientePedidoConsulta', { }]);
                }, function (error) {
                    sharedService.showError(error);
                });
        };

        ctrl.clienteOpt = {
            placeholder: "Elija el cliente...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url:appConfig.apiUrl + "Api/Cliente/ComboSource",
                        beforeSend: function(req) {
                            req.setRequestHeader('Authorization', sharedService.getToken());
                        },                            
                        data: function() {
                            return {
                            query: ctrl.cliente.text(),
                            id: ctrl.input.clienteId
                            };
                        }
                    }
                },
            },
        };

        ctrl.fleteOpt = {
            placeholder: "Elija el flete...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url:appConfig.apiUrl + "Api/Flete/",
                        beforeSend: function(req) {
                            req.setRequestHeader('Authorization', sharedService.getToken());
                        },                            
                        data: function() {
                            return {
                            query: ctrl.controls.flete.text(),
                            id: ctrl.input.fleteId
                            };
                        }
                    }
                },
            },
        };

        ctrl.entregaOpt = {
            placeholder: "Elija la direccion...",
            dataTextField: "Nombre",
            dataValueField: "Id",
            delay: 500,
            filter: "contains",
            autoBind: false,
            suggest: true,
            valuePrimitive: true,
        };
            
        ctrl.entregaChange = function(){
            var flete={};

            for(var i=0;i<ctrl.entregaOpt.dataSource.data.length;i++){
                if(ctrl.entregaOpt.dataSource.data[i].Id==ctrl.pedido.LugarEntrega){
                    flete=ctrl.entregaOpt.dataSource.data[i].Flete;
                }
            }
            if(flete){
                ctrl.input.fleteId = flete.Id;
                ctrl.controls.flete.dataSource.read().then(function() {
                    ctrl.controls.flete.select(function(dataItem) {    
                        return dataItem.Value == flete.Id;
                    });
                    ctrl.controls.flete.trigger("change");
                });
            }
        }

        ctrl.productoPruebaSeleccion =  function() {
            console.log("Se ejecutó ng-change");
        }

        ctrl.productoOpt = {
            placeholder: "Elija el producto...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url:appConfig.apiUrl + "Api/Producto/",
                        beforeSend: function(req) {
                            req.setRequestHeader('Authorization', sharedService.getToken());
                        },                            
                        data: function() {
                            return {
                            query: ctrl.productoCB.text()
                            };
                        }
                    }
                },
            },
            change: function (e) {
                console.log("change");
                console.log(e);
            },
            select: function(e) {
                console.log("select item: ",e.item)
                console.log("select productoOpt:",ctrl.productoOpt)
                var item = e.item;
                var text = item.text();
            }
        };

        function calcularBonificacion() {
            var data = {};
            if(ctrl.nuevoDetalle.Producto){
                var bonificacion = (ctrl.nuevoDetalle.Producto.Bonificacion+ctrl.nuevoDetalle.BonificacionAdicional)/100;
                data.bonificadas = Math.floor(parseInt(ctrl.nuevoDetalle.Cantidad)*bonificacion);
                data.total = parseInt(ctrl.nuevoDetalle.Cantidad)+data.bonificadas;
                if(ctrl.clienteData.VentaPorPack){
                    data.totalCorregido = Math.ceil(data.total/ctrl.nuevoDetalle.Producto.ContenedorCantidad)*ctrl.nuevoDetalle.Producto.ContenedorCantidad
                    data.Cantidad = Math.ceil((data.totalCorregido/(bonificacion+1)).toFixed(2));
                    data.bonificadas = data.totalCorregido-data.Cantidad;
                }else{
                    data.Cantidad = ctrl.nuevoDetalle.Cantidad;
                }
                ctrl.nuevoDetalle.PedidoCantidad = data.Cantidad;
                ctrl.nuevoDetalle.BonificacionCantidad=data.bonificadas;
                ctrl.nuevoDetalle.Total = parseFloat((ctrl.nuevoDetalle.Producto.Precio*ctrl.nuevoDetalle.PedidoCantidad).toFixed(2));
            }
        }

        function cargarPedido(id) {
            ctrl.llamadasPendientes=1;
            pedidoService.getById(id).then(function name(response) {
                var pedido = response.data.Valor;
                ctrl.pedido = {
                    Detalle: pedido.Detalle,
                    Id:  pedido.Id,
                    ClienteId: pedido.ClienteId,
                    VendedorId:pedido.VendedorId,
                    Sucursal: pedido.Sucursal,
                    Numero:pedido.Numero,
                    FechaEntrega: pedido.FechaEntrega,
                    Fecha: pedido.Fecha,
                    LugarEntrega:pedido.LugarEntrega,
                    TransporteId:pedido.TransporteId,
                    Eventos:pedido.Eventos,
                    Leyenda:pedido.Leyenda,
                    Observacion: pedido.Observacion,
                    ObservacionCobranza: pedido.ObservacionCobranza
                };
                ctrl.total =0; 
                for(var i = ctrl.pedido.Detalle.length; i--;) {
                    ctrl.pedido.Detalle[i].Total =ctrl.pedido.Detalle[i].Precio* ctrl.pedido.Detalle[i].PedidoCantidad
                    ctrl.total =ctrl.total + ctrl.pedido.Detalle[i].Total; 
                }  
                ctrl.clienteData = pedido.Cliente;

                ctrl.input.CondicionVenta = pedido.CondicionVentaId;
                ctrl.input.fecha = pedido.FechaEntrega;
                ctrl.input.clienteId=pedido.ClienteId;
                ctrl.cliente.dataSource.read();
                ctrl.input.clienteCB = pedido.ClienteId;
                ctrl.pedido.LugarEntrega=pedido.LugarEntrega;
                ctrl.entregaOpt.dataSource = {data:pedido.Sucursales };
                

                ctrl.input.fleteId = pedido.TransporteId;
                ctrl.controls.flete.dataSource.read();
                ctrl.input.Transporte = pedido.TransporteId;

            }, function (error) {
                sharedService.showError(error);
            });
            
        }
        function isCargando(){
            if(ctrl.llamadasPendientes>0)
                return true;
            return false;
        }
    }
    angular.module('app').component('clientePedidoDetalle', {
        templateUrl: '/App/Cliente/Pedido/detalle.tpl.html',
        controller: detalleController,
        bindings: { $router: '<' },
    });
})(window.angular);