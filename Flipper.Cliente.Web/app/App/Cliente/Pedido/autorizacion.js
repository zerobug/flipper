﻿(function (angular) {
    'use strict';
    function autorizacionController($http,appConfig) {
        var ctrl = this;
        $http.get(appConfig.apiUrl+ 'api/pedido').then(function (response) {
           ctrl.pedidos = response.data; 
        });
        ctrl.autorizar=function (id) {
            console.log(id);
            this.$router.navigate(['clientePedidoDetalle', { id: id }]);
        }
    }
    angular.module('app').component('clientePedidoAutorizacion', {
        templateUrl: '/App/Cliente/Pedido/autorizacion.tpl.html',
        controller: autorizacionController,
        bindings: { $router: '<' },
    });
})(window.angular);