(function (angular) {
    'use strict';
    function sharedService($window) {
        return {
            showError:showError,
            showInfo:showInfo,
            showSuccess:showSuccess,
            showWarning:showWarning,
            isAuthenticated:isAuthenticated,
            getToken:getToken,
            getPermisos:getPermisos
        }
        function showError(message) {
            toastr.error(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function showInfo(message) {
            toastr.info(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function showSuccess(message) {
            toastr.success(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function showWarning(message) {
            toastr.warning(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function isAuthenticated(){
            return $window.sessionStorage.token;
        }
        function getToken(){
            return 'Bearer ' + $window.sessionStorage.token;
        }
        function getPermisos(){
            return JSON.parse($window.sessionStorage.usuario);
        }
    }
    angular.module('app').service('sharedService', sharedService);
    angular.module('app').factory('NotifyingService', function($rootScope) {
        return {
            onLogin: function(scope, callback) {
                var handler = $rootScope.$on('notifying-service-login-event', callback);
                scope.$on('$destroy', handler);
            },

            loginEvent: function() {
                $rootScope.$emit('notifying-service-login-event');
            },
            onLogout: function(scope, callback) {
                var handler = $rootScope.$on('notifying-service-logout-event', callback);
                scope.$on('$destroy', handler);
            },

            logoutEvent: function() {
                $rootScope.$emit('notifying-service-logout-event');
            }
        };
    });
    
})(window.angular);