(function (angular) {
    'use strict';
    function seguridadService($http,appConfig) {
        return {
            search:search,
            getUsuarioById:getUsuarioById,
        }
        function search(valor) {
            
        }
        function getUsuarioById(id) {
            return $http.get(appConfig.apiUrl + "Api/Seguridad/GetByID/"+ id);
        }

    }
    angular.module('app').service('seguridadService', seguridadService);
})(window.angular);