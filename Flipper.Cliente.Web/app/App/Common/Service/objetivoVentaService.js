(function (angular) {
  'use strict';
  function objetivoVentaService($http,appConfig) {
    return {
        grabar: grabar,
        search:search,
        reporte:reporte,
        getById:getById,
    }
    function search(valor) {
        return $http.post(appConfig.apiUrl + "Api/objetivoVenta/Search/", valor);
    }
    function reporte(valor) {
        return $http.post(appConfig.apiUrl + "Api/objetivoVenta/Reporte/", valor);
    }
    function getById(id) {
        return $http.get(appConfig.apiUrl + "Api/objetivoVenta/Get/" + id);
    }
    function grabar(item) {
        return $http.post(appConfig.apiUrl + "Api/objetivoVenta/", item);
    }
  }
  angular.module('app').service('objetivoVentaService', objetivoVentaService);
})(window.angular);