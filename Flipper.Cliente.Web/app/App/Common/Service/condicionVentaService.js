(function (angular) {
  'use strict';
  function condicionVentaService($http,appConfig) {
      var serviceUrl=appConfig.apiUrl +"Api/CondicionVenta/";
      return {
          search:search,
      }
      function search(valor) {
          return $http.get(serviceUrl + "?id=0&query=" );
      }
  }
  angular.module('app').service('condicionVentaService', condicionVentaService);
})(window.angular);