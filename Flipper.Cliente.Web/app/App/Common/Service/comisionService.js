(function (angular) {
    'use strict';
    function comisionService($http,appConfig) {
        var serviceUrl=appConfig.apiUrl +"Api/RRHHComision/";
        return {
            grabar: grabar,
            search:search,
            getById:getById,
            actualizar:actualizar,
            
        }
        function search(valor) {
            return $http.post(serviceUrl + "Search/?query=" + valor);
        }
        function getById(id) {
            return $http.get(serviceUrl + "Get/" + id);
        }
        function grabar(item) {
            return $http.post(serviceUrl + "Grabar/", item);
        }
        function actualizar(item) {
            return $http.post(serviceUrl+ "Actualizar/" , item);
        }
    }
    angular.module('app').service('comisionService', comisionService);
})(window.angular);