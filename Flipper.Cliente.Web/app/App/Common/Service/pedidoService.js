(function (angular) {
    'use strict';
    function pedidoService($http,appConfig) {
        return {
            grabar: grabar,
            search:search,
            getById:getById,
            autorizar:autorizar,
            rechazar:rechazar,
            
        }
        function search(valor) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/Search/", valor);
        }
        function getById(id) {
            return $http.get(appConfig.apiUrl + "Api/Pedido/Get/" + id);
        }
        function grabar(item) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/", item);
        }
        function autorizar(item) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/Autorizar", item);
        }
        function rechazar(item) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/Rechazar", item);
        }
    }
    angular.module('app').service('pedidoService', pedidoService);
})(window.angular);