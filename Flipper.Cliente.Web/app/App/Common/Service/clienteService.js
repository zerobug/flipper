(function (angular) {
    'use strict';
    function clienteService($http,appConfig) {
        return {
            search:search,
            getById:getById,
        }
        function search(valor) {
            
        }
        function getById(id) {
            return $http.get(appConfig.apiUrl + "Api/Cliente/GetByID/"+ id);
        }
    }
    angular.module('app').service('clienteService', clienteService);
})(window.angular);