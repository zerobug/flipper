(function (angular) {
    'use strict';
    function productoService($http,appConfig,sharedService) {
        return {
            search:search,
            getById:getById,
            getByIDAndClient:getByIDAndClient
        }
        function search(params) {
            
        }
        function getById(params) {
            
        }
        function getByIDAndClient(id,cliente) {
            return $http.get(appConfig.apiUrl + "Api/Producto/GetByIDAndClient/?id="+ id + '&cliente=' + cliente );
        }
    }
    angular.module('app').service('productoService', productoService);
})(window.angular);