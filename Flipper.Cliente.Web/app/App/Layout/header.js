﻿(function (angular) {
    'use strict';
    function headerController($scope,sharedService,NotifyingService,seguridadService) {
        var ctrl = this;
        this.title = sharedService.title;
        NotifyingService.onLogin($scope, function(){
            var usuario = sharedService.getPermisos();
            seguridadService.getUsuarioById(usuario.UsuarioId).then(function (response) {
                //console.log(response.data);
                ctrl.nombre=response.data.Valor.Nombre;
                ctrl.codigo= response.data.Valor.Codigo;
            }, function (error) {
            });
        });
    }
    angular.module('app').component('layoutHeader', {
        templateUrl: '/App/Layout/header.tpl.html',
        controller: headerController,
        bindings: {
        },
        $routeConfig: [
            { path: '/Cliente/Pedido/Detalle/:id', name: 'ClientePedidoNuevo', component: 'clientePedidoDetalle' },
            { path: '/Cliente/Pedido/Lista/', name: 'ClientePedidoConsulta', component: 'clientePedidoLista' },
            { path: '/Cliente/Pedido/Autorizacion/', name: 'ClientePedidoAutorizar', component: 'clientePedidoAutorizacion' }
        ]
    });
})(window.angular);