
(function (angular) {
    'use strict';
    function sidebarController(appConfig,NotifyingService) {
        var ctrl = this;
        ctrl.toggle=function(){
            ctrl.onToggle();
        }
        ctrl.logout=function(){
            NotifyingService.logoutEvent();
        }
    }
    angular.module('app').component('sidebar', {
        templateUrl: '/App/Layout/sidebar.tpl.html',
        controller: sidebarController,
        bindings: {
            onToggle:'&',
            title:'=',
            path:'='
        }
    });
})(window.angular);