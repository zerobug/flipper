﻿(function (angular) {
    'use strict';
    function appController($scope,NotifyingService,sharedService) {
        var ctrl = this;
        ctrl.empresa = 'Konig';
        ctrl.title = '';
        ctrl.path = '';
        if(sharedService.isAuthenticated()){
            ctrl.isAuthenticated  =true;
        }else{
            ctrl.isAuthenticated  =false;
        }

        NotifyingService.onLogin($scope, function(){
           ctrl.isAuthenticated=true; 
        });
        NotifyingService.onLogout($scope, function(){
           ctrl.isAuthenticated=false; 
        });
        ctrl.toggleSidebar= function (){
            ctrl.toggle = !ctrl.toggle;
        }
    }
    angular.module('app').component('app', {
        templateUrl: '/App/Layout/app.tpl.html',
        controller: appController,
        bindings: {
        },
        $routeConfig: [
            { path: '/', name: 'Home', component: 'home' },
            { path: '/Cliente/CuentaCorriente/Consulta', name: 'ClienteCuentaCorrienteConsulta', component: 'clienteCuentaCorrienteConsulta' },
            { path: '/Cliente/Pedido/Detalle/', name: 'ClientePedidoNuevo', component: 'clientePedidoDetalle' },
            { path: '/Cliente/Pedido/Detalle/:id', name: 'ClientePedidoEdit', component: 'clientePedidoDetalle' },
            { path: '/Cliente/Pedido/Lista/', name: 'ClientePedidoConsulta', component: 'clientePedidoLista' },
            { path: '/Cliente/ObjetivoVenta/Lista/', name: 'ObjetivoVentaLista', component: 'objetivoVentaLista' },
            { path: '/Cliente/ObjetivoVenta/Detalle/', name: 'ObjetivoVentaNuevo', component: 'objetivoVentaDetalle' },
            { path: '/Cliente/ObjetivoVenta/Detalle/:id', name: 'ObjetivoVentaEditar', component: 'objetivoVentaDetalle' },
            { path: '/Cliente/ObjetivoVenta/Reporte', name: 'ObjetivoVentaReporte', component: 'objetivoVentaReporte' },
            { path: '/Cliente/Reporte/Venta/Diaria/', name: 'ClienteReporteVentaDiaria', component: 'clienteReporteVentaDiaria' },
            { path: '/Cliente/Reporte/Venta/PorProducto/', name: 'ClienteReportePorProducto', component: 'clienteReporteVentaPorProducto' },
            { path: '/Cliente/Reporte/Venta/PorVendedor/', name: 'ClienteReportePorVendedor', component: 'clienteReporteVentaPorVendedor' },
            { path: '/Cliente/Reporte/Venta/PorVendedorCliente/', name: 'ClienteReportePorVendedorCliente', component: 'clienteReporteVentaPorVendedorCliente' },
            { path: '/Cliente/Reporte/Venta/PorVendedorClienteProducto/', name: 'ClienteReportePorVendedorClienteProducto', component: 'clienteReporteVentaPorVendedorClienteProducto' },
            { path: '/Cliente/Pedido/Autorizacion/', name: 'ClientePedidoAutorizacion', component: 'clientePedidoAutorizacion' },
            { path: '/Cliente/Pedido/Autorizacion/Detalle/:id', name: 'ClientePedidoAutorizacionDetalle', component: 'clientePedidoAutorizacionDetalle' },
            { path: '/RRHH/Comision/', name: 'RRHHComision', component: 'rrhhComisionLista' },
            { path: '/RRHH/Comision/Detalle/:id', name: 'RRHHComisionDetalle', component: 'rrhhComisionDetalle' }            
        ]
    });
})(window.angular);
(function() {
'use strict';

    angular
        .module('app')
        .controller('EstilosController', EstilosController);

    EstilosController.$inject = ['$scope','NotifyingService','sharedService'];
    function EstilosController($scope,NotifyingService,sharedService) {
        var vm = this;
        vm.body = 'body-login';
        activate();

        function activate() { 
            if(sharedService.isAuthenticated()){
                vm.body = 'body-app';
            }
        }
        NotifyingService.onLogin($scope, function(){
            vm.body = 'body-app';
        });
    }
})();
