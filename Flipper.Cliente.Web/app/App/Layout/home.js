(function (angular) {
    'use strict';
    function homeController() {
    }
    angular.module('app').component('home', {
        templateUrl: '/App/Layout/home.tpl.html',
        controller: homeController,
        bindings: {
        },
    });
})(window.angular);