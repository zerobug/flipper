﻿(function (angular) {
    'use strict';
    function loginController($http,sharedService,NotifyingService,$sessionStorage,$window,appConfig) {
        var ctrl = this;
        this.$routerOnActivate = function(next, previous) {
        }
        ctrl.ingresar=function () {
            $http.get(appConfig.apiUrl + 'api/seguridad/ingresar?usuario='+ctrl.usuario+'&clave='+ctrl.clave)
            .then(function (response) {
                $window.sessionStorage.usuario = JSON.stringify(response.data);
                $window.sessionStorage.token = response.data.UsuarioId;
                NotifyingService.loginEvent();
            },function (error) {
                sharedService.showError('No se puede conectar con el servidor');
            });
        }
        ctrl.onKeyDown=function($event){
            if($event.keyCode==13){
                ctrl.ingresar();
            }
        }
    }
    angular.module('app').component('usuarioLogin', {
        templateUrl: '/App/Seguridad/Usuario/login.tpl.html',
        controller: loginController,
        bindings: { $router: '<' },
    });
})(window.angular);