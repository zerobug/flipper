// Generated on 2014-06-02 using generator-angular 0.8.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: {
      // configurable paths
      app: require('./bower.json').appPath || 'app',
      dist: 'dist'
    },

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['bowerInstall']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: true
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'autoprefixer']
      },
      less: {
        files: ['<%= yeoman.app %>/assets/less/*.less'],
        tasks: ['less:server']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.js',
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/assets/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      // courtesy of Phubase Tiewthanom
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                 connect.static('./bower_components')
              ),
              connect.static(require('./bower.json').appPath || 'app')
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          base: [
            '.tmp',
            'test',
            '<%= yeoman.app %>'
          ]
        }
      },
      dist: {
        options: {
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/scripts/**/*.js'
      ],
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/assets/css/',
          src: '{,*/}*.css',
          dest: '.tmp/assets/css/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    bowerInstall: {
      app: {
        src: ['<%= yeoman.app %>/index.html'],
        ignorePath: '<%= yeoman.app %>/',
        // we're not going to inject these as they're lazyloaded
        exclude: ['requirejs',
                  'mocha',
                  'jquery.vmap.europe.js',
                  'jquery.vmap.usa.js',
                  'Chart.min.js',
                  'raphael',
                  'morris',
                  'jquery.inputmask',
                  'jquery.validate.js',
                  'jquery.stepy.js',
                  'fullcalendar.js'
                  ]
      }
    },
  concat: {
    options: {
      separator: ';',
    },
    dist: {
      src: [
        'App/App/app.js',
        'App/App/Layout/app.js',
        'App/App/Common/sharedService.js',
        'App/App/Layout/header.js',
        'App/App/Layout/sidebar.js',
        'App/App/Layout/home.js',
        'App/App/Seguridad/Usuario/login.js',
        'App/App/Cliente/Pedido/detalle.js',
        'App/App/Cliente/Pedido/lista.js',
        'App/App/Cliente/Pedido/autorizacion.js',
        'App/App/Cliente/Pedido/autorizacionDetalle.js',
        'App/App/Cliente/Reporte/Venta/diaria.js',
        'App/App/Cliente/Reporte/Venta/porProducto.js',
        'App/App/Cliente/Reporte/Venta/PorVendedor.js',
        'App/App/Cliente/Reporte/Venta/PorVendedorCliente.js',
        'App/App/Cliente/Reporte/Venta/PorVendedorClienteProducto.js',
        'App/App/RRHH/Comision/lista.js',
        'App/App/RRHH/Comision/detalle.js',
        'App/App/common/service/clienteService.js',
        'App/App/common/service/pedidoService.js',
        'App/App/common/service/productoService.js',
        'App/App/common/service/comisionService.js',
        'App/App/common/service/seguridadService.js',
        'App/App/cliente/cuentacorriente/consulta.js'
        ],
      dest: 'dist/scripts/app.js',
      },
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= yeoman.dist %>/scripts/{,*/}*.js',
            '<%= yeoman.dist %>/assets/css/{,*/}*.css',
            '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= yeoman.dist %>/styles/fonts/*'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/css/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= yeoman.dist %>']
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    cssmin: {
      options: {
        // root: '<%= yeoman.app %>',
        relativeTo: '<%= yeoman.app %>',
        processImport: true,
        noAdvanced: true
      }
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true,
          removeOptionalTags: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html', 'views/{,*/}*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    // ngmin tries to make the code safe for minification automatically by
    // using the Angular long form for dependency injection. It doesn't work on
    // things like resolve or inject so those have to be done manually.
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            '*.html',
            'app/{,*/}*.html',
            'app/{,*/}{,*/}*.html',
            'images/{,*/}*.{webp}',
            'assets/**',
            // these files are lazyloaded
            'bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js',
            'bower_components/jquery-validation/dist/jquery.validate.js',
            'bower_components/jqvmap/jqvmap/maps/jquery.vmap.europe.js',
            'bower_components/jqvmap/jqvmap/maps/jquery.vmap.usa.js',
            'bower_components/stepy/lib/jquery.stepy.js',
            'bower_components/Chart.js/Chart.min.js',
            'bower_components/raphael/raphael.js',
            'bower_components/morris.js/morris.js',
            'bower_components/fullcalendar/fullcalendar.js'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          flatten: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>/assets/fonts',
          src: ['bower_components/font-awesome/fonts/*']
        }]
      },
      fonts: {
        expand: true,
        flatten: true,
        cwd: '<%= yeoman.app %>',
        dest: '<%= yeoman.dist %>/assets/fonts',
        src: 'bower_components/font-awesome/fonts/*'
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/assets/css',
        dest: '.tmp/assets/css',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'copy:styles',
        'copy:dist',
      ]
    },
    less: {
      server: {
        options: {
          dumpLineNumbers: true,
          sourceMap: true,
          sourceMapRootpath: '',
          outputSourceFiles: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/assets/less',
            src: 'styles.less',
            dest: '.tmp/assets/css',
            ext: '.css'
          }
        ]
      },
      dist: {
        options: {
          cleancss: true,
          report: 'min'
        },
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/assets/less',
            src: 'styles.less',
            dest: '.tmp/assets/css',
            ext: '.css'
          }
        ]
      }
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },
    processhtml: {
      options: {
        commentMarker: 'prochtml',
        process: true
      },
      dist: {
        files: {
          '<%= yeoman.dist %>/index.html': ['<%= yeoman.dist %>/index.html']
        }
      }
    },
    uglify: {
      options: {
        mangle: false
      }
    }
  });


  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'bowerInstall',
      'concurrent:server',
      'autoprefixer',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'bowerInstall',
    'useminPrepare',
    'concurrent:dist',
    'less:dist',
    'autoprefixer',
    'concat',
    'copy:dist',
    'cssmin',
    'uglify',
    'rev',
    'usemin',
    'processhtml:dist',
  ]);

  grunt.registerTask('default', [
    'build'
  ]);
};
