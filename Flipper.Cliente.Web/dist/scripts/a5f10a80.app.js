(function () {
    'use strict';

    var app = angular.module('app', ['ngComponentRouter','kendo.directives','ejangular','ngStorage','nvd3'])
    .constant('appConfig', {'apiUrl': 'http://localhost:9001/',})
    //.constant('appConfig', {'apiUrl': '/',})
    //.constant('appConfig', {'apiUrl': 'http://koniglab.sytes.net/',})
    .factory('authInterceptor', function ($rootScope, $q, $window) {
    return {
        request: function (config) {
        config.headers = config.headers || {};
        if ($window.sessionStorage.token) {
            config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
        }
        return config;
        },
        response: function (response) {
        if (response.status === 401) {
            // handle the case where the user is not authenticated
        }
        return response || $q.when(response);
        }
    };
    })
    .config(function ($locationProvider,$httpProvider) {
        $locationProvider.html5Mode(false);
        $httpProvider.interceptors.push('authInterceptor');
    })
    .value('$routerRootComponent', 'app');
})();
;(function (angular) {
    'use strict';
    function appController($scope,NotifyingService,sharedService) {
        var ctrl = this;
        ctrl.empresa = 'Konig';
        ctrl.title = '';
        ctrl.path = '';
        if(sharedService.isAuthenticated()){
            ctrl.isAuthenticated  =true;
        }else{
            ctrl.isAuthenticated  =false;
        }

        NotifyingService.onLogin($scope, function(){
           ctrl.isAuthenticated=true; 
        });
        NotifyingService.onLogout($scope, function(){
           ctrl.isAuthenticated=false; 
        });
        ctrl.toggleSidebar= function (){
            ctrl.toggle = !ctrl.toggle;
        }
    }
    angular.module('app').component('app', {
        templateUrl: '/App/Layout/app.tpl.html',
        controller: appController,
        bindings: {
        },
        $routeConfig: [
            { path: '/', name: 'Home', component: 'home' },
            { path: '/Cliente/CuentaCorriente/Consulta', name: 'ClienteCuentaCorrienteConsulta', component: 'clienteCuentaCorrienteConsulta' },
            { path: '/Cliente/Pedido/Detalle/', name: 'ClientePedidoNuevo', component: 'clientePedidoDetalle' },
            { path: '/Cliente/Pedido/Detalle/:id', name: 'ClientePedidoEdit', component: 'clientePedidoDetalle' },
            { path: '/Cliente/Pedido/Lista/', name: 'ClientePedidoConsulta', component: 'clientePedidoLista' },
            { path: '/Cliente/ObjetivoVenta/Lista/', name: 'ObjetivoVentaLista', component: 'objetivoVentaLista' },
            { path: '/Cliente/ObjetivoVenta/Detalle/', name: 'ObjetivoVentaNuevo', component: 'objetivoVentaDetalle' },
            { path: '/Cliente/ObjetivoVenta/Detalle/:id', name: 'ObjetivoVentaEditar', component: 'objetivoVentaDetalle' },
            { path: '/Cliente/ObjetivoVenta/Reporte', name: 'ObjetivoVentaReporte', component: 'objetivoVentaReporte' },
            { path: '/Cliente/Reporte/Venta/Diaria/', name: 'ClienteReporteVentaDiaria', component: 'clienteReporteVentaDiaria' },
            { path: '/Cliente/Reporte/Venta/PorProducto/', name: 'ClienteReportePorProducto', component: 'clienteReporteVentaPorProducto' },
            { path: '/Cliente/Reporte/Venta/PorVendedor/', name: 'ClienteReportePorVendedor', component: 'clienteReporteVentaPorVendedor' },
            { path: '/Cliente/Reporte/Venta/PorVendedorCliente/', name: 'ClienteReportePorVendedorCliente', component: 'clienteReporteVentaPorVendedorCliente' },
            { path: '/Cliente/Reporte/Venta/PorVendedorClienteProducto/', name: 'ClienteReportePorVendedorClienteProducto', component: 'clienteReporteVentaPorVendedorClienteProducto' },
            { path: '/Cliente/Pedido/Autorizacion/', name: 'ClientePedidoAutorizacion', component: 'clientePedidoAutorizacion' },
            { path: '/Cliente/Pedido/Autorizacion/Detalle/:id', name: 'ClientePedidoAutorizacionDetalle', component: 'clientePedidoAutorizacionDetalle' },
            { path: '/RRHH/Comision/', name: 'RRHHComision', component: 'rrhhComisionLista' },
            { path: '/RRHH/Comision/Detalle/:id', name: 'RRHHComisionDetalle', component: 'rrhhComisionDetalle' }            
        ]
    });
})(window.angular);
(function() {
'use strict';

    angular
        .module('app')
        .controller('EstilosController', EstilosController);

    EstilosController.$inject = ['$scope','NotifyingService','sharedService'];
    function EstilosController($scope,NotifyingService,sharedService) {
        var vm = this;
        vm.body = 'body-login';
        activate();

        function activate() { 
            if(sharedService.isAuthenticated()){
                vm.body = 'body-app';
            }
        }
        NotifyingService.onLogin($scope, function(){
            vm.body = 'body-app';
        });
    }
})();
;(function (angular) {
    'use strict';
    function sharedService($window) {
        return {
            showError:showError,
            showInfo:showInfo,
            showSuccess:showSuccess,
            showWarning:showWarning,
            isAuthenticated:isAuthenticated,
            getToken:getToken,
            getPermisos:getPermisos
        }
        function showError(message) {
            toastr.error(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function showInfo(message) {
            toastr.info(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function showSuccess(message) {
            toastr.success(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function showWarning(message) {
            toastr.warning(message,'', { timeOut: 5000, preventDuplicates: true,positionClass: "toast-top-center", closeButton: true });
        }
        function isAuthenticated(){
            return $window.sessionStorage.token;
        }
        function getToken(){
            return 'Bearer ' + $window.sessionStorage.token;
        }
        function getPermisos(){
            return JSON.parse($window.sessionStorage.usuario);
        }
    }
    angular.module('app').service('sharedService', sharedService);
    angular.module('app').factory('NotifyingService', function($rootScope) {
        return {
            onLogin: function(scope, callback) {
                var handler = $rootScope.$on('notifying-service-login-event', callback);
                scope.$on('$destroy', handler);
            },

            loginEvent: function() {
                $rootScope.$emit('notifying-service-login-event');
            },
            onLogout: function(scope, callback) {
                var handler = $rootScope.$on('notifying-service-logout-event', callback);
                scope.$on('$destroy', handler);
            },

            logoutEvent: function() {
                $rootScope.$emit('notifying-service-logout-event');
            }
        };
    });
    
})(window.angular);;(function (angular) {
    'use strict';
    function headerController($scope,sharedService,NotifyingService,seguridadService) {
        var ctrl = this;
        this.title = sharedService.title;
        NotifyingService.onLogin($scope, function(){
            var usuario = sharedService.getPermisos();
            seguridadService.getUsuarioById(usuario.UsuarioId).then(function (response) {
                //console.log(response.data);
                ctrl.nombre=response.data.Valor.Nombre;
                ctrl.codigo= response.data.Valor.Codigo;
            }, function (error) {
            });
        });
    }
    angular.module('app').component('layoutHeader', {
        templateUrl: '/App/Layout/header.tpl.html',
        controller: headerController,
        bindings: {
        },
        $routeConfig: [
            { path: '/Cliente/Pedido/Detalle/:id', name: 'ClientePedidoNuevo', component: 'clientePedidoDetalle' },
            { path: '/Cliente/Pedido/Lista/', name: 'ClientePedidoConsulta', component: 'clientePedidoLista' },
            { path: '/Cliente/Pedido/Autorizacion/', name: 'ClientePedidoAutorizar', component: 'clientePedidoAutorizacion' }
        ]
    });
})(window.angular);;
(function (angular) {
    'use strict';
    function sidebarController(appConfig,NotifyingService) {
        var ctrl = this;
        ctrl.toggle=function(){
            ctrl.onToggle();
        }
        ctrl.logout=function(){
            NotifyingService.logoutEvent();
        }
    }
    angular.module('app').component('sidebar', {
        templateUrl: '/App/Layout/sidebar.tpl.html',
        controller: sidebarController,
        bindings: {
            onToggle:'&',
            title:'=',
            path:'='
        }
    });
})(window.angular);;(function (angular) {
    'use strict';
    function homeController() {
    }
    angular.module('app').component('home', {
        templateUrl: '/App/Layout/home.tpl.html',
        controller: homeController,
        bindings: {
        },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function loginController($http,sharedService,NotifyingService,$sessionStorage,$window,appConfig) {
        var ctrl = this;
        this.$routerOnActivate = function(next, previous) {
        }
        ctrl.ingresar=function () {
            $http.get(appConfig.apiUrl + 'api/seguridad/ingresar?usuario='+ctrl.usuario+'&clave='+ctrl.clave)
            .then(function (response) {
                $window.sessionStorage.usuario = JSON.stringify(response.data);
                $window.sessionStorage.token = response.data.UsuarioId;
                NotifyingService.loginEvent();
            },function (error) {
                sharedService.showError('No se puede conectar con el servidor');
            });
        }
        ctrl.onKeyDown=function($event){
            if($event.keyCode==13){
                ctrl.ingresar();
            }
        }
    }
    angular.module('app').component('usuarioLogin', {
        templateUrl: '/App/Seguridad/Usuario/login.tpl.html',
        controller: loginController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function detalleController(appConfig,sharedService,$http,pedidoService,productoService,clienteService,condicionVentaService) {
        var ctrl = this;
        kendo.culture("es-AR");
        ctrl.title = '';
        ctrl.controls={};
        ctrl.editable = true;
        ctrl.llamadasPendientes=0;
        ctrl.puedeAutorizar =false;

        ctrl.input = {
            fleteId:0,
            clienteId:0,
            cliente:function(valor){
                if(arguments.length){
                    if(isCargando()){
                        ctrl.llamadasPendientes--;
                    }else{
                        if(valor!=null){
                            clienteService.getById(valor)
                            .then(function (response) {
                                ctrl.clienteData = response.data.Valor;
                                ctrl.input.fecha = moment().add(ctrl.clienteData.Entrega,'H').toDate();
                                ctrl.pedido.ClienteId =valor;
                                ctrl.entregaOpt.dataSource = {data:ctrl.clienteData.Sucursales };
                                ctrl.input.fleteId= ctrl.clienteData.Flete.Id;
                                ctrl.controls.flete.dataSource.read();
                                ctrl.input.Transporte = ctrl.clienteData.Flete.Id;
                                ctrl.input.ListaCodigo = ctrl.clienteData.ListaCodigo;
                                ctrl.input.CategoriaIVA = ctrl.clienteData.CategoriaIVA;
                                ctrl.input.CondicionVenta = ctrl.clienteData.CondicionVenta;
                                ctrl.pedido.TransporteId = ctrl.clienteData.Flete.Id;
                            }, function (error) {
                                sharedService.showError(error);
                            });
                        }
                    }
                    return ctrl.input.clienteId = valor;
                }else{
                    return ctrl.input.clienteId;
                }
            },
            productoId:0,
            producto:function (valor) {
                if(arguments.length){
                    if(valor!=null&&valor!=0){
                        productoService.getByIDAndClient(valor, ctrl.clienteData.Id)
                        .then(function (response) {
                            if(response.data.Exito){
                                if(!validarTrazable(response.data.Valor)){
                                    sharedService.showError('No se pueden mezclar productos trazables y no trazables');
                                    return;
                                }
                                ctrl.nuevoDetalle={};
                                ctrl.nuevoDetalle.Producto = response.data.Valor;
                                ctrl.nuevoDetalle.ProductoId=ctrl.nuevoDetalle.Producto.Id;
                                ctrl.nuevoDetalle.Bonificacion = ctrl.nuevoDetalle.Producto.Bonificacion;
                                ctrl.nuevoDetalle.BonificacionAdicional = 0;
                                ctrl.nuevoDetalle.Cantidad=0;
                                ctrl.nuevoDetalle.Precio=ctrl.nuevoDetalle.Producto.Precio;
                            }else{
                                sharedService.showError(response.data.ResumenError);
                            }
                        }, function (error) {
                            sharedService.showError(error);
                        });
                    }
                    return ctrl.input.productoId = valor;
                }else{
                    return ctrl.input.productoId;
                }
            },
            fecha: moment().toDate()
        };

        this.$routerOnActivate = function(next, previous) {
            ctrl.nuevo();
            var permisos = sharedService.getPermisos();
            condicionVentaService.search().then(function name(response) {
                ctrl.condicionVentaLista = response.data;
            }, function (error) {
                sharedService.showError(error);
            });

            if(next.params.id=='null' || next.params.id===0){
                ctrl.title ='Nuevo pedido'; 
            }else{
                ctrl.title ='ver pedido';
                ctrl.puedeAutorizar = false;
                ctrl.editable = false;
                if(next.params.operacion=='autorizar'){
                    ctrl.title ='Autorizar pedido';
                    if(!angular.isUndefined(permisos)){
                        ctrl.puedeAutorizar = permisos.Autorizar;
                        ctrl.editable = permisos.Autorizar;
                    }
                }
                if(next.params.operacion=='editar'){
                    ctrl.title ='editar pedido';
                    ctrl.editable = true;
                }
                cargarPedido(next.params.id);
            }
        };

        ctrl.Pedido = function (valor){
            if(arguments.length){
                if(valor<0){
                    return false;
                }
                ctrl.nuevoDetalle.Cantidad = valor;
                calcularBonificacion();
                return true;
            }else{
                if(angular.isUndefined(ctrl.nuevoDetalle)||ctrl.nuevoDetalle===null)
                    return 0;
                return ctrl.nuevoDetalle.Cantidad;
            }
        };

        ctrl.Bonificacion = function (valor) {
            if(arguments.length){
                if(valor<0){
                    return false;
                }
                ctrl.nuevoDetalle.BonificacionAdicional = valor;
                calcularBonificacion();
                return true;
            }else{
                if(angular.isUndefined(ctrl.nuevoDetalle)||ctrl.nuevoDetalle===null)
                    return 0;
                return ctrl.nuevoDetalle.BonificacionAdicional;
            }
        };

        ctrl.nuevo = function () {
            ctrl.pedido = {
                Detalle:[],
            };
            ctrl.nuevoDetalle={};
            ctrl.input.productoId=0;
            ctrl.input.productoCB=null;
            ctrl.input.fleteId=0;
            ctrl.input.clienteId=0;
            ctrl.input.clienteCB=null;
            ctrl.input.Transporte=null;
            ctrl.input.fecha=moment().toDate();
            ctrl.cliente.focus();
            ctrl.total=0;
        };

        function getFechaEntrega() {
            if(ctrl.input.fecha instanceof Date && !isNaN(ctrl.input.fecha.valueOf())){
                return moment(ctrl.input.fecha).format("YYYY-MM-DD");
            }else{
                return moment(ctrl.input.fecha,'DD/MM/YYYY').format("YYYY-MM-DD");
            }
        };

        function validarTrazable(producto){
            for(var i=0;i<ctrl.pedido.Detalle.length;i++){
                if(producto.RemiteSeparado != ctrl.pedido.Detalle[i].Producto.RemiteSeparado)
                    return false;
            }
            return true;
        };

        function validarTieneItems() {
            if(ctrl.pedido.Detalle.length>0)
                return true;
            sharedService.showError('El pedido debe tener al menos 1 producto');
            return false;
        };

        function validarMoneda(item) {            
            if(!ctrl.pedido.Detalle.length>0){
                return true;
            }else{
                if(ctrl.pedido.Detalle[0].Producto.Moneda == item.Producto.Moneda)
                    return true;
            }
            sharedService.showError('Todos los productos deben tener precio en la misma moneda.');
            return false;
        }

        ctrl.enviar = function () {
            ctrl.pedido.FechaEntrega=getFechaEntrega();
            if(!validarTieneItems())
                return;
            ctrl.pedido.CondicionVentaId=ctrl.input.CondicionVenta;
            console.log('grabar: ',ctrl.clienteData.Sucursales);


            if(!ctrl.pedido.LugarEntrega && ctrl.clienteData.Sucursales.length>0){
                sharedService.showError('Debe completar la sucursal.');
                return;
            }
            if(!ctrl.pedido.TransporteId){
                sharedService.showError('Debe completar el flete.');
                return;
            }
            pedidoService.grabar(ctrl.pedido).then(function (response) {
                sharedService.showSuccess('Pedido enviado');
                ctrl.nuevo();
            }, function (error) {
                sharedService.showError(error.data.ExceptionMessage);
            });
        };

        ctrl.cancelar = function () {
            ctrl.$router.navigate(['Home', {  }]);
        };

        ctrl.bonificar = function () {
            ctrl.nuevoDetalle.Total=0;
            ctrl.nuevoDetalle.Precio=0;
            ctrl.nuevoDetalle.Bonificacion=0;
            ctrl.nuevoDetalle.BonificacionCantidad=0;
            ctrl.nuevoDetalle.Producto.PrecioBruto=0;
            
            ctrl.agregar();
        };

        ctrl.agregar = function () {
            if(validarMoneda(ctrl.nuevoDetalle)){
                ctrl.total = ctrl.total + ctrl.nuevoDetalle.Total; 
                ctrl.pedido.Detalle.push(ctrl.nuevoDetalle);
                ctrl.nuevoDetalle={};
                ctrl.productoCB.search('');
                // ctrl.input.producto=null;
                ctrl.input.productoCB=null;
                // console.log(ctrl.productoCB.items())
            }            
        }

        ctrl.eliminar = function (item) {
            for(var i = ctrl.pedido.Detalle.length; i--;) {
                if(ctrl.pedido.Detalle[i].ProductoId == item.ProductoId) {
                    ctrl.total =ctrl.total - item.Total; 
                    ctrl.pedido.Detalle.splice(i, 1);
                }
            }
        }

        ctrl.autorizar = function () {
            ctrl.pedido.FechaEntrega=getFechaEntrega();
            if(!validarTieneItems())
                return;
            ctrl.pedido.CondicionVentaId=ctrl.input.CondicionVenta;
            pedidoService.autorizar(ctrl.pedido).then(function (response) {
                    sharedService.showSuccess('Pedido enviado');
                    ctrl.$router.navigate(['ClientePedidoConsulta', {  }]);
                }, function (error) {
                    sharedService.showError(error);
                });
        };

        ctrl.rechazar = function () {
            ctrl.pedido.FechaEntrega=getFechaEntrega();
            if(!validarTieneItems())
                return;
            pedidoService.rechazar(ctrl.pedido).then(function (response) {
                    sharedService.showSuccess('Pedido enviado');
                    ctrl.$router.navigate(['ClientePedidoConsulta', { }]);
                }, function (error) {
                    sharedService.showError(error);
                });
        };

        ctrl.clienteOpt = {
            placeholder: "Elija el cliente...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url:appConfig.apiUrl + "Api/Cliente/ComboSource",
                        beforeSend: function(req) {
                            req.setRequestHeader('Authorization', sharedService.getToken());
                        },                            
                        data: function() {
                            return {
                            query: ctrl.cliente.text(),
                            id: ctrl.input.clienteId
                            };
                        }
                    }
                },
            },
        };

        ctrl.fleteOpt = {
            placeholder: "Elija el flete...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url:appConfig.apiUrl + "Api/Flete/",
                        beforeSend: function(req) {
                            req.setRequestHeader('Authorization', sharedService.getToken());
                        },                            
                        data: function() {
                            return {
                            query: ctrl.controls.flete.text(),
                            id: ctrl.input.fleteId
                            };
                        }
                    }
                },
            },
        };

        ctrl.entregaOpt = {
            placeholder: "Elija la direccion...",
            dataTextField: "Nombre",
            dataValueField: "Id",
            delay: 500,
            filter: "contains",
            autoBind: false,
            suggest: true,
            valuePrimitive: true,
        };
            
        ctrl.entregaChange = function(){
            var flete={};

            for(var i=0;i<ctrl.entregaOpt.dataSource.data.length;i++){
                if(ctrl.entregaOpt.dataSource.data[i].Id==ctrl.pedido.LugarEntrega){
                    flete=ctrl.entregaOpt.dataSource.data[i].Flete;
                }
            }
            if(flete){
                ctrl.input.fleteId = flete.Id;
                ctrl.controls.flete.dataSource.read().then(function() {
                    ctrl.controls.flete.select(function(dataItem) {    
                        return dataItem.Value == flete.Id;
                    });
                    ctrl.controls.flete.trigger("change");
                });
            }
        }

        ctrl.productoPruebaSeleccion =  function() {
            console.log("Se ejecutó ng-change");
        }

        ctrl.productoOpt = {
            placeholder: "Elija el producto...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url:appConfig.apiUrl + "Api/Producto/",
                        beforeSend: function(req) {
                            req.setRequestHeader('Authorization', sharedService.getToken());
                        },                            
                        data: function() {
                            return {
                            query: ctrl.productoCB.text()
                            };
                        }
                    }
                },
            },
            change: function (e) {
                console.log("change");
                console.log(e);
            },
            select: function(e) {
                console.log("select item: ",e.item)
                console.log("select productoOpt:",ctrl.productoOpt)
                var item = e.item;
                var text = item.text();
            }
        };

        function calcularBonificacion() {
            var data = {};
            if(ctrl.nuevoDetalle.Producto){
                var bonificacion = (ctrl.nuevoDetalle.Producto.Bonificacion+ctrl.nuevoDetalle.BonificacionAdicional)/100;
                data.bonificadas = Math.floor(parseInt(ctrl.nuevoDetalle.Cantidad)*bonificacion);
                data.total = parseInt(ctrl.nuevoDetalle.Cantidad)+data.bonificadas;
                if(ctrl.clienteData.VentaPorPack){
                    data.totalCorregido = Math.ceil(data.total/ctrl.nuevoDetalle.Producto.ContenedorCantidad)*ctrl.nuevoDetalle.Producto.ContenedorCantidad
                    data.Cantidad = Math.ceil((data.totalCorregido/(bonificacion+1)).toFixed(2));
                    data.bonificadas = data.totalCorregido-data.Cantidad;
                }else{
                    data.Cantidad = ctrl.nuevoDetalle.Cantidad;
                }
                ctrl.nuevoDetalle.PedidoCantidad = data.Cantidad;
                ctrl.nuevoDetalle.BonificacionCantidad=data.bonificadas;
                ctrl.nuevoDetalle.Total = parseFloat((ctrl.nuevoDetalle.Producto.Precio*ctrl.nuevoDetalle.PedidoCantidad).toFixed(2));
            }
        }

        function cargarPedido(id) {
            ctrl.llamadasPendientes=1;
            pedidoService.getById(id).then(function name(response) {
                var pedido = response.data.Valor;
                ctrl.pedido = {
                    Detalle: pedido.Detalle,
                    Id:  pedido.Id,
                    ClienteId: pedido.ClienteId,
                    VendedorId:pedido.VendedorId,
                    Sucursal: pedido.Sucursal,
                    Numero:pedido.Numero,
                    FechaEntrega: pedido.FechaEntrega,
                    Fecha: pedido.Fecha,
                    LugarEntrega:pedido.LugarEntrega,
                    TransporteId:pedido.TransporteId,
                    Eventos:pedido.Eventos,
                    Leyenda:pedido.Leyenda,
                    Observacion: pedido.Observacion,
                    ObservacionCobranza: pedido.ObservacionCobranza
                };
                ctrl.total =0; 
                for(var i = ctrl.pedido.Detalle.length; i--;) {
                    ctrl.pedido.Detalle[i].Total =ctrl.pedido.Detalle[i].Precio* ctrl.pedido.Detalle[i].PedidoCantidad
                    ctrl.total =ctrl.total + ctrl.pedido.Detalle[i].Total; 
                }  
                ctrl.clienteData = pedido.Cliente;

                ctrl.input.CondicionVenta = pedido.CondicionVentaId;
                ctrl.input.fecha = pedido.FechaEntrega;
                ctrl.input.clienteId=pedido.ClienteId;
                ctrl.cliente.dataSource.read();
                ctrl.input.clienteCB = pedido.ClienteId;
                ctrl.pedido.LugarEntrega=pedido.LugarEntrega;
                ctrl.entregaOpt.dataSource = {data:pedido.Sucursales };
                

                ctrl.input.fleteId = pedido.TransporteId;
                ctrl.controls.flete.dataSource.read();
                ctrl.input.Transporte = pedido.TransporteId;

            }, function (error) {
                sharedService.showError(error);
            });
            
        }
        function isCargando(){
            if(ctrl.llamadasPendientes>0)
                return true;
            return false;
        }
    }
    angular.module('app').component('clientePedidoDetalle', {
        templateUrl: '/App/Cliente/Pedido/detalle.tpl.html',
        controller: detalleController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function listaController(appConfig,pedidoService,sharedService,$window) {
        var ctrl = this;
        ctrl.puedeAutorizar =false;
        ctrl.fechaDesde =moment().startOf('month').format('DD/MM/YYYY');
        ctrl.fechaHasta =moment().endOf('month').format('DD/MM/YYYY');
        ctrl.filtro={};
        //ctrl.fechaDesde ='';
        ctrl.vendedorOpt = {
            placeholder: "Elija el vendedor...",
            dataTextField: "Text",
            dataValueField: "Value",
            delay: 500,
            filter: "contains",
            autoBind: false,
            valuePrimitive: true,
            dataSource: {
              type: "json",
              serverFiltering: true,
              transport: {
                read: {
                  url:appConfig.apiUrl + "Api/Vendedor/ComboSource",
                  beforeSend: function(req) {
                      req.setRequestHeader('Authorization', sharedService.getToken());
                  },                            
                  data: function() {
                    return {
                      query: ctrl.vendedor.text(),
                      id: ctrl.filtro.vendedorId
                    };
                  }
                }
              },
            },
            };

        this.$routerOnActivate = function() {
            ctrl.puedeAutorizar = sharedService.getPermisos().Autorizar;
            if(sharedService.filtroPedido){
                //ctrl.cliente.dataSource.read();
                //ctrl.vendedor.dataSource.read();
                console.log(sharedService.filtroPedido);
                ctrl.fechaDesde =sharedService.filtroPedido.fechaDesde;
                ctrl.fechaHasta =sharedService.filtroPedido.fechaHasta;
                ctrl.clienteId=sharedService.filtroPedido.clienteId;
                ctrl.vendedorId=sharedService.filtroPedido.vendedor;
                ctrl.vendedor.value(sharedService.filtroPedido.vendedor);
                ctrl.cliente.value(sharedService.filtroPedido.clienteId);
            }else{
                ctrl.fechaDesde =moment().toDate();//.format('DD/MM/YYYY');
                ctrl.fechaHasta =moment().toDate();//.format('DD/MM/YYYY');
            }
            //refrescar();
        }
        ctrl.autorizar=function (id) {
            this.$router.navigate(['ClientePedidoNuevo', { id: id, operacion:'autorizar' }]);
        }
        ctrl.ver=function (id) {
            this.$router.navigate(['ClientePedidoNuevo', { id: id, operacion:'ver' }]);
        }
        ctrl.editar=function (id) {
            this.$router.navigate(['ClientePedidoNuevo', { id: id, operacion:'editar' }]);
        }
        function refrescar(){
            //$window.sessionStorage.filtroPedido = JSON.stringify({fechaDesde:ctrl.fechaDesde, fechaHasta: ctrl.fechaHasta});
            sharedService.filtroPedido = {cliente:ctrl.clienteId, vendedor: ctrl.vendedorId, fechaDesde:ctrl.fechaDesde, fechaHasta: ctrl.fechaHasta};
            var desde=moment(ctrl.fechaDesde).format('DD/MM/YYYY');
            var hasta=moment(ctrl.fechaHasta).format('DD/MM/YYYY');
            pedidoService.search({cliente:ctrl.clienteId, vendedor: ctrl.vendedorId, estado:ctrl.estado, fechaDesde:desde, fechaHasta: hasta })
            .then(function (response) {
                ctrl.pedidos = response.data.Valor;
            }, function (error) {
                    sharedService.showError(error|| 'Ha ocurrido un error contactese con sistemas');
            });
        }
        ctrl.buscar = function(){
            refrescar();
        }
      
        ctrl.clienteOpt = {
				placeholder: "Elija el cliente...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: {
					type: "json",
					serverFiltering: true,
					transport: {
						read: {
                            url:appConfig.apiUrl + "Api/Cliente/ComboSource",
                            beforeSend: function(req) {
                                req.setRequestHeader('Authorization', sharedService.getToken());
                            },                            
                            data: function() {
                                return {
                                query: ctrl.cliente.text(),
                                id: ctrl.clienteId
                                };
                            }
                        }
					},
				},
			};
        ctrl.estadoOpt = {
				placeholder: "Elija el estado...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: [{Value:1, Text:'Pendiente'},{Value:2, Text:'Rechazado'},{Value:3, Text:'Autorizado'},{Value:4, Text:'Pendiente Administracion'},{Value:4, Text:'Pendiente Confirmacion'}]
            };
            //
    }
    angular.module('app').component('clientePedidoLista', {
        templateUrl: '/App/Cliente/Pedido/lista.tpl.html',
        controller: listaController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function autorizacionController($http,appConfig) {
        var ctrl = this;
        $http.get(appConfig.apiUrl+ 'api/pedido').then(function (response) {
           ctrl.pedidos = response.data; 
        });
        ctrl.autorizar=function (id) {
            console.log(id);
            this.$router.navigate(['clientePedidoDetalle', { id: id }]);
        }
    }
    angular.module('app').component('clientePedidoAutorizacion', {
        templateUrl: '/App/Cliente/Pedido/autorizacion.tpl.html',
        controller: autorizacionController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function autorizacionDetalleController($http, appConfig) {
        var ctrl = this;
        this.$routerOnActivate = function(next) {
            $http.get(appConfig.apiUrl+ 'api/pedido/'+next.params.id).then(function (response) {
            ctrl.pedido = response.data; 
            });
        };
    }
    angular.module('app').component('clientePedidoAutorizacionDetalle', {
        templateUrl: '/App/Cliente/Pedido/autorizacionDetalle.tpl.html',
        controller: autorizacionDetalleController,
        bindings: {
        }
    });
})(window.angular);;(function (angular) {
    'use strict';
    function ventaDiariaController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().subtract(1, 'months').startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.model = {ventas:[]};
        console.log('diaria');
        //ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        //ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        //ctrl.reportPath= 'VentaDiaria.rdl';
        ctrl.consultar = function () {
            $http.get(appConfig.apiUrl + "Api/ReporteVentaDiaria/Get/?desde="+moment(ctrl.desde).format('YYYYMMDD')+"&hasta="+moment(ctrl.hasta).format('YYYYMMDD')+"&tipo=1").then(function (response) {
                ctrl.model.ventas=response.data;
                var data = [];
                ctrl.data2 = [{
                    key: "Venta",
                    values: []
                    }]
                ctrl.model.ventas.forEach(function(el) {
                    ctrl.data2[0].values.push({ x: el.Dia , y : el.Importe });
                });
                console.log(ctrl.data2);
                console.log(data);

            }, function (error) {
                sharedService.showError(error.data.ExceptionMessage);
            });
            // var proxy = $('#container').data('ejReportViewer');
            // proxy._refresh = true;
            // $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            // _params = [];
            // _params.push({ Name: 'tipo', Values: ['porProducto'] });
            // _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            // _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            // proxy._refreshReport();
        }

        // ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
        //     var proxy = $('#container').data('ejReportViewer');
        //     var inVokemethod = onSuccess;

        //     $.ajax({
        //         type: type,
        //         url: url,
        //         crossDomain: true,
        //         contentType: 'application/json; charset=utf-8',
        //         dataType: 'json',
        //         data: jsondata,
        //         beforeSend: function (req) {
        //             if (inVokemethod == "_getDataSourceCredential") {
        //                 var _json = jQuery.parseJSON(this.data);
        //                 if (_params != null) {
        //                     _json["params"] = _params;
        //                 }
        //                 this.data = JSON.stringify(_json);
        //             }

        //             if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
        //                 if (!proxy._isToolbarClick) {
        //                     proxy._showloadingIndicator(true);
        //                     proxy._updateDatasource = true;
        //                 } else {
        //                     proxy._showNavigationIndicator(true);
        //                 }
        //             }
        //             req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
        //             req.setRequestHeader('Authorization', sharedService.getToken());
        //         },
        //         success: function (data) {
        //             if (data && typeof (data.Data) != "undefined") {
        //                 data = data.Data;
        //             }
        //             if (typeof (data) == "string") {
        //                 if (data.indexOf("Sf_Exception") != -1) {
        //                     proxy._renderExcpetion(inVokemethod + ":" + data);
        //                     return;
        //                 }
        //             }
        //             proxy[inVokemethod](data);
        //         },
        //     });
        //}
        ctrl.options2 = {
            chart: {
                type: 'discreteBarChart',
                height: 350,
                margin : {
                    top: 10,
                    right: 20,
                    bottom: 40,
                    left: 70
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                useInteractiveGuideline: true,
                dispatch: {
                    stateChange: function(e){ console.log("stateChange"); },
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Dias'
                },
                yAxis: {
                    axisLabel: 'Ventas',
                    tickFormat: function(d){
                        return d3.format('$08.02g')(d);
                    },
                    axisLabelDistance: -10
                },
            },
            title: {
                enable: true,
                text: 'Venta por dia'
            },
        };

        ctrl.options = {
            chart: {
                type: 'lineChart',
                height: 350,
                margin : {
                    top: 10,
                    right: 20,
                    bottom: 40,
                    left: 55
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                useInteractiveGuideline: true,
                dispatch: {
                    stateChange: function(e){ console.log("stateChange"); },
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Dias'
                },
                yAxis: {
                    axisLabel: 'Ventas',
                    tickFormat: function(d){
                        return d3.format('$.02g')(d);
                    },
                    axisLabelDistance: -10
                },
            },
            title: {
                enable: true,
                text: 'Venta Acumulada vs objetivo'
            },
        };
                
        ctrl.data2 = [];

        ctrl.data = [{
            key: "Venta",
            values: [
                { x: "1" , y : 0 },
                { x : "2" , y : 0 },
                { x : "3" , y : 12 },
                { x : "4" , y : 19 },
                { x: "5" , y : 30 },
                { x : "6" ,y : 33 },
                { x : "7" ,y : 35 },
                { x : "8" , y: 50 }
                ]
            },{
            key: "Proyeccion",
            values: [
                { x: "1" , y : 0 },
                { x : "2" , y : 10 },
                { x : "3" , y : 20 },
                { x : "4" , y : 30 },
                { x: "5" , y : 40 },
                { x : "6" ,y : 50 },
                { x : "7" ,y : 60 },
                { x : "8" , y: 70 }
                ]
            }]
        }
    angular.module('app').component('clienteReporteVentaDiaria', {
        templateUrl: '/App/Cliente/Reporte/Venta/diaria.tpl.html',
        controller: ventaDiariaController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function ventaPorProductoController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        ctrl.reportPath= 'Ventas.rdl';
        ctrl.consultar = function () {
            var proxy = $('#container').data('ejReportViewer');
            proxy._refresh = true;
            $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            _params = [];
            _params.push({ Name: 'tipo', Values: ['porProducto'] });
            _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            proxy._refreshReport();
        }

            ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
                var proxy = $('#container').data('ejReportViewer');
                var inVokemethod = onSuccess;

                $.ajax({
                    type: type,
                    url: url,
                    crossDomain: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: jsondata,
                    beforeSend: function (req) {
                        if (inVokemethod == "_getDataSourceCredential") {
                            var _json = jQuery.parseJSON(this.data);
                            if (_params != null) {
                                _json["params"] = _params;
                            }
                            this.data = JSON.stringify(_json);
                        }

                        if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
                            if (!proxy._isToolbarClick) {
                                proxy._showloadingIndicator(true);
                                proxy._updateDatasource = true;
                            } else {
                                proxy._showNavigationIndicator(true);
                            }
                        }
                        req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
                        req.setRequestHeader('Authorization', sharedService.getToken());
                    },
                    success: function (data) {
                        if (data && typeof (data.Data) != "undefined") {
                            data = data.Data;
                        }
                        if (typeof (data) == "string") {
                            if (data.indexOf("Sf_Exception") != -1) {
                                proxy._renderExcpetion(inVokemethod + ":" + data);
                                return;
                            }
                        }
                        proxy[inVokemethod](data);
                    },
                });
            }
    }
    angular.module('app').component('clienteReporteVentaPorProducto', {
        templateUrl: '/App/Cliente/Reporte/Venta/porProducto.tpl.html',
        controller: ventaPorProductoController,
        bindings: { $router: '<' },
    });
})(window.angular);;
(function (angular) {
    'use strict';
    function ventaPorVendedorController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        ctrl.reportPath= 'Ventas.rdl';
        ctrl.consultar = function () {
            var proxy = $('#container').data('ejReportViewer');
            proxy._refresh = true;
            $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            _params = [];
            _params.push({ Name: 'tipo', Values: ['porVendedor'] });
            _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            proxy._refreshReport();
        }

            ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
                var proxy = $('#container').data('ejReportViewer');
                var inVokemethod = onSuccess;

                $.ajax({
                    type: type,
                    url: url,
                    crossDomain: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: jsondata,
                    beforeSend: function (req) {
                        if (inVokemethod == "_getDataSourceCredential") {
                            var _json = jQuery.parseJSON(this.data);
                            if (_params != null) {
                                _json["params"] = _params;
                            }
                            this.data = JSON.stringify(_json);
                        }

                        if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
                            if (!proxy._isToolbarClick) {
                                proxy._showloadingIndicator(true);
                                proxy._updateDatasource = true;
                            } else {
                                proxy._showNavigationIndicator(true);
                            }
                        }
                        req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
                        req.setRequestHeader('Authorization', sharedService.getToken());
                    },
                    success: function (data) {
                        if (data && typeof (data.Data) != "undefined") {
                            data = data.Data;
                        }
                        if (typeof (data) == "string") {
                            if (data.indexOf("Sf_Exception") != -1) {
                                proxy._renderExcpetion(inVokemethod + ":" + data);
                                return;
                            }
                        }
                        proxy[inVokemethod](data);
                    },
                });
            }
    }
    angular.module('app').component('clienteReporteVentaPorVendedor', {
        templateUrl: '/App/Cliente/Reporte/Venta/porVendedor.tpl.html',
        controller: ventaPorVendedorController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function ventaPorVendedorClienteController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        ctrl.reportPath= 'Ventas.rdl';
        ctrl.consultar = function () {
            var proxy = $('#container').data('ejReportViewer');
            proxy._refresh = true;
            $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            _params = [];
            _params.push({ Name: 'tipo', Values: ['porVendedorCliente'] });
            _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            proxy._refreshReport();
        }

            ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
                var proxy = $('#container').data('ejReportViewer');
                var inVokemethod = onSuccess;

                $.ajax({
                    type: type,
                    url: url,
                    crossDomain: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: jsondata,
                    beforeSend: function (req) {
                        if (inVokemethod == "_getDataSourceCredential") {
                            var _json = jQuery.parseJSON(this.data);
                            if (_params != null) {
                                _json["params"] = _params;
                            }
                            this.data = JSON.stringify(_json);
                        }

                        if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
                            if (!proxy._isToolbarClick) {
                                proxy._showloadingIndicator(true);
                                proxy._updateDatasource = true;
                            } else {
                                proxy._showNavigationIndicator(true);
                            }
                        }
                        req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
                        req.setRequestHeader('Authorization', sharedService.getToken());
                    },
                    success: function (data) {
                        if (data && typeof (data.Data) != "undefined") {
                            data = data.Data;
                        }
                        if (typeof (data) == "string") {
                            if (data.indexOf("Sf_Exception") != -1) {
                                proxy._renderExcpetion(inVokemethod + ":" + data);
                                return;
                            }
                        }
                        proxy[inVokemethod](data);
                    },
                });
            }
    }
    angular.module('app').component('clienteReporteVentaPorVendedorCliente', {
        templateUrl: '/App/Cliente/Reporte/Venta/porVendedorCliente.tpl.html',
        controller: ventaPorVendedorClienteController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function ventaPorVendedorClienteProductoController($http,appConfig,sharedService) {
        var ctrl = this;
        var _params = null;
        ctrl.desde = moment().startOf('month').toDate();
        ctrl.hasta = moment().endOf('month').toDate();
        ctrl.reportServiceUrl=appConfig.apiUrl +"api/Reports";
        ctrl.processingMode= ej.ReportViewer.ProcessingMode.Local;
        ctrl.reportPath= 'Ventas.rdl';
        ctrl.consultar = function () {
            var proxy = $('#container').data('ejReportViewer');
            proxy._refresh = true;
            $('#' + proxy._id + '_viewBlockContainer .e-reportviewer-viewerblockcontent table:first').attr('isviewclick', 'true');
            _params = [];
            _params.push({ Name: 'tipo', Values: ['porVendedorClienteProducto'] });
            _params.push({ Name: 'desde', Values: [moment(ctrl.desde).format('YYYYMMDD')] });
            _params.push({ Name: 'hasta', Values: [moment(ctrl.hasta).format('YYYYMMDD')] });
            proxy._refreshReport();
        }

            ej.ReportViewer.prototype.doAjaxPost = function (type, url, jsondata, onSuccess) {
                var proxy = $('#container').data('ejReportViewer');
                var inVokemethod = onSuccess;

                $.ajax({
                    type: type,
                    url: url,
                    crossDomain: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: jsondata,
                    beforeSend: function (req) {
                        if (inVokemethod == "_getDataSourceCredential") {
                            var _json = jQuery.parseJSON(this.data);
                            if (_params != null) {
                                _json["params"] = _params;
                            }
                            this.data = JSON.stringify(_json);
                        }

                        if (inVokemethod == "_getPageModel" || inVokemethod == "_getPreviewModel") {
                            if (!proxy._isToolbarClick) {
                                proxy._showloadingIndicator(true);
                                proxy._updateDatasource = true;
                            } else {
                                proxy._showNavigationIndicator(true);
                            }
                        }
                        req.setRequestHeader('ejAuthenticationToken', proxy._authenticationToken);
                        req.setRequestHeader('Authorization', sharedService.getToken());
                    },
                    success: function (data) {
                        if (data && typeof (data.Data) != "undefined") {
                            data = data.Data;
                        }
                        if (typeof (data) == "string") {
                            if (data.indexOf("Sf_Exception") != -1) {
                                proxy._renderExcpetion(inVokemethod + ":" + data);
                                return;
                            }
                        }
                        proxy[inVokemethod](data);
                    },
                });
            }
    }
    angular.module('app').component('clienteReporteVentaPorVendedorClienteProducto', {
        templateUrl: '/App/Cliente/Reporte/Venta/porVendedorClienteProducto.tpl.html',
        controller: ventaPorVendedorClienteProductoController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function listaController(appConfig,sharedService,$http,comisionService) {
        var ctrl = this;

        this.$routerOnActivate = function(next, previous) {
            comisionService.search('')
            .then(function (response) {
                ctrl.pedidos = response.data;
            },function (error) {
                sharedService.showError(error);
            });
        }

        ctrl.ver = function (id) {
            this.$router.navigate(['RRHHComisionDetalle', { id: id }]);
        }

        ctrl.nuevo = function () {
            this.$router.navigate(['RRHHComisionDetalle', { id: 0 }]);
        }
    }
    angular.module('app').component('rrhhComisionLista', {
        templateUrl: '/App/RRHH/Comision/lista.tpl.html',
        controller: listaController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function detalleController(appConfig,sharedService,$http,comisionService) {
        var ctrl = this;
        kendo.culture("es-AR");
        ctrl.controls={};
        ctrl.controls.comisionOptions={
            format:'#.##',
            culture: 'en-US',
            decimals:2
        }
        this.$routerOnActivate = function(next, previous) {
            if(next.params.id=='null' || next.params.id=='0'){
                ctrl.item={};
                ctrl.item.Desde=moment().startOf('month').format('DD/MM/YYYY');
                ctrl.item.Hasta=moment().endOf('month').format('DD/MM/YYYY');
                console.log(ctrl.item);
            }else{
                cargarLiquidacion(next.params.id);
            }
        }
        ctrl.actualizar=function () {
            comisionService.actualizar(ctrl.item)
            .then(function (response) {
                ctrl.totales = response.data.Totales;
                ctrl.vendedores = response.data.Vendedores;
                ctrl.exclusivos = response.data.Exclusivos;
                ctrl.supermercados = response.data.Supermercados;
                ctrl.gerentes = response.data.Gerentes;
            },function (error) {
                sharedService.showError(error);
            });
        }
        ctrl.grabar=function () {
            comisionService.grabar(ctrl.item)
            .then(function (response) {
                ctrl.item =response.data; 
            },function (error) {
                sharedService.showError(error);
            });
        }
        function cargarLiquidacion(id){
            comisionService.getById(id)
            .then(function (response) {
                ctrl.item =response.data; 
            },function (error) {
                sharedService.showError(error);
            });
        }
    }
    angular.module('app').component('rrhhComisionDetalle', {
        templateUrl: '/App/RRHH/Comision/detalle.tpl.html',
        controller: detalleController,
        bindings: { $router: '<' },
    });
})(window.angular);;(function (angular) {
    'use strict';
    function clienteService($http,appConfig) {
        return {
            search:search,
            getById:getById,
        }
        function search(valor) {
            
        }
        function getById(id) {
            return $http.get(appConfig.apiUrl + "Api/Cliente/GetByID/"+ id);
        }
    }
    angular.module('app').service('clienteService', clienteService);
})(window.angular);;(function (angular) {
    'use strict';
    function pedidoService($http,appConfig) {
        return {
            grabar: grabar,
            search:search,
            getById:getById,
            autorizar:autorizar,
            rechazar:rechazar,
            
        }
        function search(valor) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/Search/", valor);
        }
        function getById(id) {
            return $http.get(appConfig.apiUrl + "Api/Pedido/Get/" + id);
        }
        function grabar(item) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/", item);
        }
        function autorizar(item) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/Autorizar", item);
        }
        function rechazar(item) {
            return $http.post(appConfig.apiUrl + "Api/Pedido/Rechazar", item);
        }
    }
    angular.module('app').service('pedidoService', pedidoService);
})(window.angular);;(function (angular) {
    'use strict';
    function productoService($http,appConfig,sharedService) {
        return {
            search:search,
            getById:getById,
            getByIDAndClient:getByIDAndClient
        }
        function search(params) {
            
        }
        function getById(params) {
            
        }
        function getByIDAndClient(id,cliente) {
            return $http.get(appConfig.apiUrl + "Api/Producto/GetByIDAndClient/?id="+ id + '&cliente=' + cliente );
        }
    }
    angular.module('app').service('productoService', productoService);
})(window.angular);;(function (angular) {
    'use strict';
    function comisionService($http,appConfig) {
        var serviceUrl=appConfig.apiUrl +"Api/RRHHComision/";
        return {
            grabar: grabar,
            search:search,
            getById:getById,
            actualizar:actualizar,
            
        }
        function search(valor) {
            return $http.post(serviceUrl + "Search/?query=" + valor);
        }
        function getById(id) {
            return $http.get(serviceUrl + "Get/" + id);
        }
        function grabar(item) {
            return $http.post(serviceUrl + "Grabar/", item);
        }
        function actualizar(item) {
            return $http.post(serviceUrl+ "Actualizar/" , item);
        }
    }
    angular.module('app').service('comisionService', comisionService);
})(window.angular);;(function (angular) {
    'use strict';
    function seguridadService($http,appConfig) {
        return {
            search:search,
            getUsuarioById:getUsuarioById,
        }
        function search(valor) {
            
        }
        function getUsuarioById(id) {
            return $http.get(appConfig.apiUrl + "Api/Seguridad/GetByID/"+ id);
        }

    }
    angular.module('app').service('seguridadService', seguridadService);
})(window.angular);;(function (angular) {
    'use strict';
    function consultaController(appConfig,sharedService,$http,pedidoService,productoService,clienteService) {
        var ctrl = this;
        ctrl.title = 'Cuenta corriente de clientes';
        ctrl.clienteOpt = {
				placeholder: "Elija el cliente...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: {
					type: "json",
					serverFiltering: true,
					transport: {
						read: {
                            url:appConfig.apiUrl + "Api/Cliente/ComboSource",
                            beforeSend: function(req) {
                                req.setRequestHeader('Authorization', sharedService.getToken());
                            },                            
                            data: function() {
                                return {
                                query: ctrl.cliente.text(),
                                id: ctrl.clienteId
                                };
                            }
                        }
					},
				},
			};
        ctrl.estadoOpt = {
				placeholder: "Elija el estado...",
				dataTextField: "Text",
				dataValueField: "Value",
				delay: 500,
				filter: "contains",
				autoBind: false,
				valuePrimitive: true,
				dataSource: [{Value:1, Text:'Pendiente'},{Value:2, Text:'Todos'}]
			};
    }
    angular.module('app').component('clienteCuentaCorrienteConsulta', {
        templateUrl: '/App/Cliente/CuentaCorriente/consulta.tpl.html',
        controller: consultaController,
        bindings: { $router: '<' },
    });
})(window.angular);